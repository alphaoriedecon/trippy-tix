<?php

namespace App\Laravel\Models;

use Carbon, Helper;
use App\Laravel\Models\Views\UserGroup;
use Illuminate\Notifications\Notifiable;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Inquiry extends Authenticatable
{
   use Notifiable, SoftDeletes;

    protected $table = "inquiry";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'contact_number',
        'nationality',
        'marital_status',
        'message',
        'filename',
        'address',
        'country',
        'departure_date',
        'arrival_date',
        'departure_location',
        'arrival_location',
        'tourist_visa',
        'appointment_date',
        'user_id',
        'type',
        'agent','passenger','infant','child','days',
        'status'
    ];
    public $timestamps = true;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

   
     public function author() {
        return $this->BelongsTo("App\Laravel\Models\User", "agent", "id");
    }


}
