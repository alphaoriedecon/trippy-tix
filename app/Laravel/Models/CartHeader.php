<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;

class CartHeader extends Model
{
    protected $table = "cart_header";  

    protected $fillable = ["trans_id","user_id","qty","total_price","status"];

    public function details()
    {
      return $this->hasMany("App\Laravel\Models\CartDetail","transaction_id","trans_id");
    }
    
     

    
}
