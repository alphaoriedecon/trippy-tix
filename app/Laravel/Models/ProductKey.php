<?php

namespace App\laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ProductKey extends Model
{
	use SoftDeletes;
    protected $fillable =["product_id","product_key","expiration","price","qty"];



    public function products()
    {
    	// return $this->belongsTo("App\Laravel\Models\Product");
    	return $this->hasOne("App\Laravel\Models\Product","id","product_id");
    }

    
}
