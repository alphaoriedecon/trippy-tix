<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionHeader extends Model
{
    protected $table = "transaction_header";	

    protected $fillable = ["trans_id","user_id","qty","total_price","status"];

    public function details()
    {
    	return $this->hasMany("App\Laravel\Models\TransactionDetail","transaction_id","trans_id");
    }
    
     

    
}
