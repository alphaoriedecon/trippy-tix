<?php

namespace App\laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
	use SoftDeletes;
    protected $fillable = ["name","remarks","sub_category_id"];
    
     public function sub_category()
    {
        return $this->hasOne("App\Laravel\Models\SubCategory","id","sub_category_id");
    }    
}
