<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    protected $table = "transaction_details";	

    protected $fillable = ["transaction_id","user_id","product_id","product_name","qty","item_price","sub_total","discount"];
  

  	public function header()
  	{
  		return $this->belongsTo("App\Laravel\Models\TransactionHeader","trans_id","transction_id")->where("status","pending");
  	}

  	public function product()
  	{
  		return $this->hasOne("App\Laravel\Models\ProductKey","id","product_id");
  	}
}
