<?php

namespace App\laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategory extends Model
{
	use SoftDeletes;
    protected $fillable = ["name","remarks","path","directory","filename","main_category"];
    
}
