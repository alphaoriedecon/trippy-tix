<?php

namespace App\Laravel\Models;

use Carbon, Helper;
use App\Laravel\Models\Views\UserGroup;
use Illuminate\Notifications\Notifiable;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Blogs extends Authenticatable
{
   use Notifiable, SoftDeletes;

    protected $table = "blogs";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'content',
        'category',
        'url',
        'path',
        'directory',
        'filename'
    ];
    public $timestamps = true;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    public function categories()
    {
        return $this->hasOne("App\Laravel\Models\BlogCategory","id","category");
    }

     public function author() {
        return $this->BelongsTo("App\Laravel\Models\User", "user_id", "id");
    }

}
