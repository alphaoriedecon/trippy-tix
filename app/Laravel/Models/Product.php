<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Product extends Model
{
	use SoftDeletes;
    protected $fillable = ["title","main_category","sub_category","category","type","price","description","status","qty"];

     public function author(){
        return $this->belongsTo('App\Laravel\Models\User','user_id','id');
    }

     public function images(){
        return $this->hasMany('App\Laravel\Models\ProductImages','product_id','id');
    }

// public function price()
// {
//       return $this->hasOne("App\Laravel\Models\ProductKey","product_id","id");
// }


    public function scopeFilter($query, $title= "", $status= "")    {

    	return $query->where('title', 'like', '%'.$title.'%')
    				  ->where("status",$status);
    				
    }
}
