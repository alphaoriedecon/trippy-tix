<?php

namespace App\Laravel\Models;

use Carbon, Helper;
use App\Laravel\Models\Views\UserGroup;
use Illuminate\Notifications\Notifiable;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Packages extends Authenticatable
{
   use Notifiable, SoftDeletes;

    protected $table = "packages";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'content',
        'path',
        'directory',
        'filename'
    ];
    public $timestamps = true;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

     public function author() {
        return $this->BelongsTo("App\Laravel\Models\User", "user_id", "id");
    }

}
