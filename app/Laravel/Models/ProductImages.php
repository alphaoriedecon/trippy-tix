<?php

namespace App\Laravel\Models;
use Helper;

use Illuminate\Database\Eloquent\Model;

class ProductImages extends Model
{
    protected $table = "product_images";

    protected $fillable =['directory','path','filename'];

   
}
