<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;

class CartDetail extends Model
{
    protected $table = "cart_details";	

    protected $fillable = ["transaction_id","user_id","product_id","product_name","qty","item_price","sub_total","discount"];
  

  	public function header()
  	{
  		return $this->belongsTo("App\Laravel\Models\CartHeader","trans_id","transction_id");
  	}

  	public function product()
  	{
  		return $this->hasOne("App\Laravel\Models\ProductKey","id","product_id");
  	}
}
