<?php

namespace App\Laravel\Models;

use Carbon, Helper;
use App\Laravel\Models\Views\UserGroup;
use Illuminate\Notifications\Notifiable;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;


class BlogCategory extends Authenticatable
{
    use Notifiable, SoftDeletes;

    protected $table = "blog_categories";

    protected $fillable = [
        'category_name',
    ];



}
