<?php 

namespace App\Laravel\Controllers\System;

/*
*Models
*/
use App\Laravel\Models\User;
use App\Laravel\Models\SubCategory;
use App\Laravel\Models\MainCategory;
use Helper, Session, Str,ImageUploader,Carbon,Auth,DB;

/*
	Request
*/ 

use App\Laravel\Requests\System\SubCategoryRequest;	


class SubCategoryController extends Controller{

function __construct()
{
	$this->data = [];
}
	public function index(){

		// return SubCategory::all();
		$this->data['categories'] = SubCategory::orderBy('updated_at',"DESC")->get();
		return view('system.sub-category.index',$this->data);
	}
	
	public function create(){
        $this->data['main_category'] = MainCategory::all();
		return view('system.sub-category.create',$this->data);
	}

	public function store(SubCategoryRequest $request)
	{
		// return $request->all();
		try {
			DB::beginTransaction();
			$sub_category = new SubCategory;
			$sub_category->name = $request->name;
            $sub_category->main_category = $request->main_category;
            if($request->hasFile('file')) {
                $image = ImageUploader::upload($request->file('file'), "uploads/images/subcategory");
                $sub_category->path = $image['path'];
                $sub_category->directory = $image['directory'];
                $sub_category->filename = $image['filename'];
            }
            
			$sub_category->save();
			DB::commit();
			session()->flash('notification-status','success');
			session()->flash('notification-msg',"New Sub category has been added.");
			return redirect()->route("system.sub_category.index");

		} catch (Exception $e) {
			DB::rollback();
			session()->flash('notification-status','warning');
			session()->flash('notification-msg',"Went something wrong".$e->message());
		}
	}

	public function edit($id = NULL)
	{
        $this->data['main_category'] = MainCategory::all();
		$this->data['category'] = SubCategory::find($id);
		return  view("system.sub-category.update",$this->data);
	}


	public function update(SubCategoryRequest $request, $id=NULL)
	{
		// return $request->name;
		try {
			DB::beginTransaction();
			$sub_category = SubCategory::find($id);
			$sub_category->name = $request->name;
            $sub_category->remarks = $request->remarks;
            $sub_category->main_category = $request->main_category;
            if($request->hasFile('file')) {
                $image = ImageUploader::upload($request->file('file'), "uploads/images/subcategory");
                $sub_category->path = $image['path'];
                $sub_category->directory = $image['directory'];
                $sub_category->filename = $image['filename'];
            }
			$sub_category->save();

			DB::commit();
			session()->flash('notification-status','success');
			session()->flash('notification-msg',"Sub category has been modified.");
			return redirect()->route("system.sub_category.index");
		} catch (Exception $e) {
			DB::rollback();
			session()->flash('notification-status','warning');
			session()->flash('notification-msg',"Went something wrong".$e->message());
			return redirect()->back();
		}
	}

	public function destroy($id=NULL)
	{
		try {
			DB::beginTransaction();
			SubCategory::destroy($id);
			DB::commit();
			session()->flash('notification-status','success');
			session()->flash('notification-msg',"Sub category has been deleted.");
			return redirect()->route("system.sub_category.index");
		} catch (Exception $e) {
			DB::rollback();
			session()->flash('notification-status','warning');
			session()->flash('notification-msg',"Went something wrong".$e->message());
			return redirect()->back();
		}
	}


}