<?php 

namespace App\Laravel\Controllers\System;

use App\Laravel\Models\Blogs;
use Helper, Session, Str,ImageUploader,Carbon,Auth;
use App\Laravel\Requests\System\BlogsRequest;
use App\Laravel\Requests\System\BlogsRequest1;
use App\Laravel\Models\BlogCategory;

class BlogsController extends Controller{
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = [ 'active' => "Active","inactive" => "Inactive"];
		//$this->data['categories'] = ['--Choose Category--'] + Category::pluck('name','id')->toArray();
	}

	public function index(){
		$this->data['blogs'] = Blogs::orderBy('updated_at',"DESC")->get();
		return view('system.content-management.blogs.index',$this->data);
	}

	public function store (BlogsRequest $request) {
		try {
			$new_blogs = new Blogs;
			$user = $request->user();
      $new_blogs->fill($request->only('title','url','content'));
			if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/images/users/{$user->id}/blogs");
			    $new_blogs->path = $image['path'];
			    $new_blogs->directory = $image['directory'];
			    $new_blogs->filename = $image['filename'];
			}
			// $new_blogs->status = $request->get('status');
			$new_blogs->user_id = $request->user()->id;
			if($new_blogs->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.content_management.blogs.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function create(){
		$this->data['categories'] = BlogCategory::all();
		return view('system.content-management.blogs.create',$this->data);
	}
	public function edit ($id = NULL) {
		$blogs = Blogs::find($id);
		$this->data['categories'] = BlogCategory::all();
		if (!$blogs) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.content_management.blogs.index');
		}

		if($id < 0){
			session()->flash('notification-status',"warning");
			session()->flash('notification-msg',"Unable to update special record.");
			return redirect()->route('system.content_management.blogs.index');	
		}

		$this->data['blogs'] = $blogs;
		return view('system.content-management.blogs.edit',$this->data);
	}
	
	public function update (BlogsRequest1 $request, $id = NULL) {
		try {
			$blogs = Blogs::find($id);

			if (!$blogs) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.content_management.blogs.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to update special record.");
				return redirect()->route('system.content_management.blogs.index');	
			}
			$user = $request->user();
        	$blogs->fill($request->only('title','url','content','category'));
        	if($request->hasFile('file')) {
        	    $image = ImageUploader::upload($request->file('file'), "uploads/images/users/{$user->id}/blogs");
        	    $blogs->path = $image['path'];
        	    $blogs->directory = $image['directory'];
        	    $blogs->filename = $image['filename'];
        	}

			if($blogs->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.content_management.blogs.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$blogs = Blogs::find($id);

			if (!$blogs) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.content_management.blogs.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to remove special record.");
				return redirect()->route('system.content_management.blogs.index');	
			}

			if($blogs->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.content_management.blogs.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}