<?php 

namespace App\Laravel\Controllers\System;


use App\Laravel\Models\TransactionHeader;
use App\Laravel\Models\TransactionDetail;


use Carbon,Auth;

class TransactionController extends Controller{

	public function show(){
		return view('system.transaction.show');
	}
	public function new(){

		$this->data["transactions"] = TransactionHeader::all();
		return view('system.transaction.new',$this->data);
	}


	public function store(PageRequest $request)
	{
		// $check = TransactionHeader::find($request->id);

		$transaction = new TransactionHeader;
		$transaction->trans_id = $transaction->id;
		$transaction->user_id = 1;
		$transaction->qty = 1;
		$transaction->total_price = 1000;
		$transaction->save();
	}



	public function pending(){
		return view('system.transaction.pending');
	}
	public function completed(){
		return view('system.transaction.completed');
	}

}