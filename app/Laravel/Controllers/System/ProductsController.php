<?php 

namespace App\Laravel\Controllers\System;

/*
	Models
*/ 
use App\Laravel\Models\ProductCategory;
use App\Laravel\Models\Product;


/*
	Requests
*/ 
use App\Http\Requests\PageRequest;
use App\Laravel\Requests\System\ProductRequest;	

use Carbon,Auth,DB,ImageUploader;

class ProductsController extends Controller{


function __construct()
{
	$this->data = [];
}
	public function index(PageRequest $request){

	switch (true) {
			case $request->title:
				$this->data['products'] = Product::filter($request->title,$request->status)->get();
				break;
			
			default:
				$this->data['products'] = Product::all();
				break;
		}
		return view('system.products.index',$this->data);
	}

	public function create(){
		$this->data['categories'] = ProductCategory::all();
		return view('system.products.create',$this->data);
	}
	public function store(ProductRequest $request)
	{
		// return $request->all();
		try {
			DB::beginTransaction();
			$product = new Product;

			if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/products");
			    $product->path = $image['path'];
			    $product->directory = $image['directory'];
			   	$product->filename = $image['filename'];
			}
			$product->title = strtolower($request->title);
			$product->description = strtolower($request->description);
			$product->category = strtolower($request->category);
			// $product->qty = $request->qty;
			$product->save();
			DB::commit();
			session()->flash('notification-status','success');
			session()->flash('notification-msg',"New Prodcuct category has been added.");
			return redirect()->route("system.products.index");

		} catch (Exception $e) {
			DB::rollback();
			session()->flash('notification-status','warning');
			session()->flash('notification-msg',"Went something wrong".$e->message());
		}
	}

	public function edit($id = NULL)
	{
		$this->data['categories'] = ProductCategory::all();
		$this->data['product'] = Product::find($id);
		return  view("system.products.update",$this->data);
	}


	public function update(ProductRequest $request, $id=NULL)
	{
		// return $request->file;
		try {
			DB::beginTransaction();

			    $product = Product::find($id);
				$product->title = strtolower($request->title);
				$product->description = strtolower($request->description);
				$product->category = strtolower($request->category);
				$product->qty = $request->qty;
			
			switch (true) {
				case $request->hasFile('file'):
				$image = ImageUploader::upload($request->file('file'), "uploads/products");
			    $product->path = $image['path'];
			    $product->directory = $image['directory'];
			 	$product->filename = $image['filename'];
					break;
				
				default:
				$product->path = $product->path;
			    $product->directory = $product->directory;
			 	$product->filename = $product->filename ;
					break;
			}
			
			 
			$product->save();
			DB::commit();
			session()->flash('notification-status','success');
			session()->flash('notification-msg',"Prodcuct category has been modified.");
			return redirect()->route("system.products.index");
		} catch (Exception $e) {
			DB::rollback();
			session()->flash('notification-status','warning');
			session()->flash('notification-msg',"Went something wrong".$e->message());
			return redirect()->back();
		}
	}

	public function destroy($id=NULL)
	{
		try {
			DB::beginTransaction();
			Product::destroy($id);
			DB::commit();
			session()->flash('notification-status','success');
			session()->flash('notification-msg',"Prodcuct has been deleted.");
			return redirect()->route("system.products.index");
		} catch (Exception $e) {
			DB::rollback();
			session()->flash('notification-status','warning');
			session()->flash('notification-msg',"Went something wrong".$e->message());
			return redirect()->back();
		}
	}

}