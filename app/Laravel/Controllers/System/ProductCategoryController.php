<?php 

namespace App\Laravel\Controllers\System;

/*
*Models
*/
use App\Laravel\Models\User;
use App\Laravel\Models\ProductCategory;

/*
	Request
*/ 

use App\Laravel\Requests\System\ProductCategoryRequest;	

use Carbon,Auth,DB;

class ProductCategoryController extends Controller{

function __construct()
{
	$this->data = [];
}
	public function index(){

		// return ProductCategory::all();
		$this->data['categories'] = ProductCategory::all();
		return view('system.product-category.index',$this->data);
	}
	
	public function create(){

		return view('system.product-category.create');
	}

	public function store(ProductCategoryRequest $request)
	{
		// return $request->all();
		try {
			DB::beginTransaction();
			$product_category = new ProductCategory;
			$product_category->category_name = $request->category_name;
			$product_category->save();
			DB::commit();
			session()->flash('notification-status','success');
			session()->flash('notification-msg',"New Prodcuct category has been added.");
			return redirect()->route("system.product_category.index");

		} catch (Exception $e) {
			DB::rollback();
			session()->flash('notification-status','warning');
			session()->flash('notification-msg',"Went something wrong".$e->message());
		}
	}

	public function edit($id = NULL)
	{
		$this->data['category'] = ProductCategory::find($id);
		return  view("system.product-category.update",$this->data);
	}


	public function update(ProductCategoryRequest $request, $id=NULL)
	{
		// return $request->category_name;
		try {
			DB::beginTransaction();
			$category = ProductCategory::find($id);
			$category->category_name = $request->category_name;
			$category->save();

			DB::commit();
			session()->flash('notification-status','success');
			session()->flash('notification-msg',"Prodcuct category has been modified.");
			return redirect()->route("system.product_category.index");
		} catch (Exception $e) {
			DB::rollback();
			session()->flash('notification-status','warning');
			session()->flash('notification-msg',"Went something wrong".$e->message());
			return redirect()->back();
		}
	}

	public function destroy($id=NULL)
	{
		try {
			DB::beginTransaction();
			ProductCategory::destroy($id);
			DB::commit();
			session()->flash('notification-status','success');
			session()->flash('notification-msg',"Prodcuct category has been deleted.");
			return redirect()->route("system.product_category.index");
		} catch (Exception $e) {
			DB::rollback();
			session()->flash('notification-status','warning');
			session()->flash('notification-msg',"Went something wrong".$e->message());
			return redirect()->back();
		}
	}


}