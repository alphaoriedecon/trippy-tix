<?php 

namespace App\Laravel\Controllers\System;


use App\Laravel\Models\BlogCategory;
use App\Laravel\Requests\System\BlogCategoryRequest;

use Carbon,Auth;

class BlogCategoriesController extends Controller{

	public function index(){
		$this->data['blog_categories'] = BlogCategory::orderBy('updated_at',"DESC")->paginate(15);
		return view('system.content-management.blog-categories.index',$this->data);
	}

	public function create(){
		return view('system.content-management.blog-categories.create');
	}
	public function store (BlogCategoryRequest $request) {
		try {
			$new_blog_category = new BlogCategory;
      $new_blog_category->fill($request->only('category_name'));
			if($new_blog_category->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.content_management.blog_categories.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit($id = Null){
		$blog_categories = BlogCategory::find($id);
		if (!$blog_categories) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.content_management.blog_categories.index');
		}

		if($id < 0){
			session()->flash('notification-status',"warning");
			session()->flash('notification-msg',"Unable to update special record.");
			return redirect()->route('system.content_management.blog_categories.index');	
		}

		$this->data['blog_categories'] = $blog_categories;
		return view('system.content-management.blog-categories.edit',$this->data);
	}

	public function update (BlogCategoryRequest $request, $id = NULL) {
		try {
			$blog_categories = BlogCategory::find($id);

			if (!$blog_categories) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.content_management.blog_categories.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to update special record.");
				return redirect()->route('system.content_management.blog_categories.index');	
			}
			
      $blog_categories->fill($request->only('category_name'));
			if($blog_categories->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.content_management.blog_categories.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$blog_categories = BlogCategory::find($id);

			if (!$blog_categories) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.content_management.blog_categories.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to remove special record.");
				return redirect()->route('system.content_management.blog_categories.index');	
			}

			if($blog_categories->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.content_management.blog_categories.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}	

}