<?php 

namespace App\Laravel\Controllers\System;

use App\Laravel\Models\Inquiry;
use Helper, Session, Str,ImageUploader,Carbon,Auth;
use App\Laravel\Requests\System\BlogsRequest;
use App\Laravel\Models\BlogCategory;

class InquiryController extends Controller{
	protected $data;

	public function request_travel_index(){
		$fullname = \Auth::user()->first_name. " " .\Auth::user()->last_name;
		if(\Auth::user()->type == 'super_user'){
				$this->data['request_travel'] = Inquiry::orderBy('updated_at',"DESC")->where('type','request_travel')->get();
		}
		else {
				$this->data['request_travel'] = Inquiry::orderBy('updated_at',"DESC")->where('type','request_travel')->where('agent',\Auth::user()->id)->get();
		}
		
		return view('system.inquiry.request-travel',$this->data); 
	}
	public function request_travel_land_index(){
		$fullname = \Auth::user()->first_name. " " .\Auth::user()->last_name;
		if(\Auth::user()->type == 'super_user'){
				$this->data['request_travel_land'] = Inquiry::orderBy('updated_at',"DESC")->where('type','request_travel_land')->get();
		}
		else {
				$this->data['request_travel_land'] = Inquiry::orderBy('updated_at',"DESC")->where('type','request_travel_land')->where('agent',\Auth::user()->id)->get();
		}
		
		return view('system.inquiry.request-travel-land',$this->data); 
	}
	public function insured_index(){
		$fullname = \Auth::user()->first_name. " " .\Auth::user()->last_name;
			if(\Auth::user()->type == 'super_user'){
				$this->data['insured'] = Inquiry::orderBy('updated_at',"DESC")->where('type','insured')->get();
			}
			else {
				$this->data['insured'] = Inquiry::orderBy('updated_at',"DESC")->where('type','insured')->where('agent',\Auth::user()->id)->get();
			}
		return view('system.inquiry.insured',$this->data);
	}
	public function airline_index(){
			$fullname = \Auth::user()->first_name. " " .\Auth::user()->last_name;
			if(\Auth::user()->type == 'super_user'){
				$this->data['airline'] = Inquiry::orderBy('updated_at',"DESC")->where('type','airline')->get();
			}
			else {
				$this->data['airline'] = Inquiry::orderBy('updated_at',"DESC")->where('type','airline')->where('agent',\Auth::user()->id)->get();
			}
		return view('system.inquiry.airline',$this->data);
	}
	public function appointment_index(){

		$fullname = \Auth::user()->first_name. " " .\Auth::user()->last_name;
			if(\Auth::user()->type == 'super_user'){
				$this->data['appointment'] = Inquiry::orderBy('updated_at',"DESC")->where('type','appointment')->get();
			}
			else {
				$this->data['appointment'] = Inquiry::orderBy('updated_at',"DESC")->where('type','appointment')->where('agent',\Auth::user()->id)->get();
			}
		return view('system.inquiry.appointment',$this->data);
	}
	public function permanent_visa_index(){
		$fullname = \Auth::user()->first_name. " " .\Auth::user()->last_name;
			if(\Auth::user()->type == 'super_user'){
				$this->data['permanent_visa'] = Inquiry::orderBy('updated_at',"DESC")->where('type','permanent_visa')->get();
			}
			else {
				$this->data['permanent_visa'] = Inquiry::orderBy('updated_at',"DESC")->where('type','permanent_visa')->where('agent',\Auth::user()->id)->get();
			}
		return view('system.inquiry.permanent-visa',$this->data);
	}
	public function tourist_visa_index(){
		$fullname = \Auth::user()->first_name. " " .\Auth::user()->last_name;
			if(\Auth::user()->type == 'super_user'){
				$this->data['tourist_visa'] = Inquiry::orderBy('updated_at',"DESC")->where('type','tourist_visa')->get();
			}
			else {
				$this->data['tourist_visa'] = Inquiry::orderBy('updated_at',"DESC")->where('type','tourist_visa')->where('agent',\Auth::user()->id)->get();
			}
		
		return view('system.inquiry.tourist-visa',$this->data);
	}

	public function details ($id = NULL) {
		$details = Inquiry::find($id);
		if (!$details) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->back();
		}

		if($id < 0){
			session()->flash('notification-status',"warning");
			session()->flash('notification-msg',"Unable to update special record.");
		return redirect()->back();
		}

		$this->data['details'] = $details;
		return view('system.inquiry.details',$this->data);
	}


	public function complete ($id = NULL) {
		try {
			$inquiry = Inquiry::find($id);

			if (!$inquiry) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.dashboard');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to remove special record.");
				return redirect()->route('system.dashboard');	
			}
  		$inquiry->status = "completed";
			if($inquiry->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.dashboard');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}