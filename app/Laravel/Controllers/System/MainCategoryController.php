<?php 

namespace App\Laravel\Controllers\System;

/*
*Models
*/
use App\Laravel\Models\User;
use App\Laravel\Models\MainCategory;

/*
	Request
*/ 

use App\Laravel\Requests\System\MainCategoryRequest;	

use Carbon,Auth,DB;

class MainCategoryController extends Controller{

function __construct()
{
	$this->data = [];
}
	public function index(){

		// return MainCategory::all();
		$this->data['categories'] = MainCategory::orderBy('updated_at',"DESC")->get();
		return view('system.main-category.index',$this->data);
	}
	
	public function create(){

		return view('system.main-category.create');
	}

	public function store(MainCategoryRequest $request)
	{
		// return $request->all();
		try {
			DB::beginTransaction();
			$category = new MainCategory;
			$category->name = $request->name;
            $category->remarks = $request->remarks;
			$category->save();
			DB::commit();
			session()->flash('notification-status','success');
			session()->flash('notification-msg',"New Main category has been added.");
			return redirect()->route("system.main_category.index");

		} catch (Exception $e) {
			DB::rollback();
			session()->flash('notification-status','warning');
			session()->flash('notification-msg',"Went something wrong".$e->message());
		}
	}

	public function edit($id = NULL)
	{
		$this->data['category'] = MainCategory::find($id);
		return  view("system.main-category.update",$this->data);
	}


	public function update(MainCategoryRequest $request, $id=NULL)
	{
		// return $request->name;
		try {
			DB::beginTransaction();
			$category = MainCategory::find($id);
			$category->name = $request->name;
            $category->remarks = $request->remarks;

			$category->save();

			DB::commit();
			session()->flash('notification-status','success');
			session()->flash('notification-msg',"Main category has been modified.");
			return redirect()->route("system.main_category.index");
		} catch (Exception $e) {
			DB::rollback();
			session()->flash('notification-status','warning');
			session()->flash('notification-msg',"Went something wrong".$e->message());
			return redirect()->back();
		}
	}

	public function destroy($id=NULL)
	{
		try {
			DB::beginTransaction();
			MainCategory::destroy($id);
			DB::commit();
			session()->flash('notification-status','success');
			session()->flash('notification-msg',"Main category has been deleted.");
			return redirect()->route("system.main_category.index");
		} catch (Exception $e) {
			DB::rollback();
			session()->flash('notification-status','warning');
			session()->flash('notification-msg',"Went something wrong".$e->message());
			return redirect()->back();
		}
	}


}