<?php 

namespace App\Laravel\Controllers\System;


use App\Laravel\Models\Partner;
use App\Laravel\Models\Sale;
use App\Laravel\Models\Business;
use App\Laravel\Models\TransactionManager;
use App\Laravel\Models\Inquiry;
use Helper, Session, Str,ImageUploader,Carbon,Auth;
use App\Laravel\Requests\System\BlogsRequest;
use App\Laravel\Models\BlogCategory;

class DashboardController extends Controller{

	public function index(){
			$fullname = \Auth::user()->first_name. " " .\Auth::user()->last_name;
			$completed = Inquiry::orderBy('updated_at',"DESC")->where('status','completed')->get();;
			$this->data['count_completed'] = $completed->count();
			$pending = Inquiry::orderBy('updated_at',"DESC")->where('status','pending')->get();;
			$this->data['count_pending'] = $pending->count();
			$all_status = Inquiry::orderBy('updated_at',"DESC")->get();;
			$this->data['count_all_status'] = $all_status->count();
			if(\Auth::user()->type == 'super_user'){
				$this->data['inquiry_list'] = Inquiry::orderBy('updated_at',"DESC")->get();
			}
			else {
				$this->data['inquiry_list'] = Inquiry::orderBy('updated_at',"DESC")->where('agent',\Auth::user()->id)->get();
			}
		return view('system.dashboard.index',$this->data);
	}

}