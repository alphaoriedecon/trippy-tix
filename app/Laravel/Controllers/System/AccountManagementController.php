<?php 

namespace App\Laravel\Controllers\System;

use App\Laravel\Models\Partner;
use App\Laravel\Models\User;
use App\Laravel\Models\Business;
use App\Laravel\Models\TransactionManager;
use App\Laravel\Requests\Frontend\UserRequest;
use App\Laravel\Requests\Frontend\UserRequest1;
use Helper, Session, Str,ImageUploader,Carbon,Auth;

class AccountManagementController extends Controller{

	public function user(){
		$this->data['user_list'] = User::orderBy('updated_at',"DESC")->where('type','user')->get();
		return view('system.account-management.user.index',$this->data);
	}

	public function create_user(){
		return view('system.account-management.user.create');
	}
	public function store(UserRequest $request){

        $new_user = new User;
        $new_user->fill($request->only('first_name','last_name','mobile_number','email','username','gender','url'));
        // dd($request->get('area'));
        $new_user->status = "active";
        $new_user->type = "user";
        $new_user->password = bcrypt($request->get('password'));
        if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/images/users");
			    $new_user->path = $image['path'];
			    $new_user->directory = $image['directory'];
			    $new_user->filename = $image['filename'];
			}
        if($new_user->save()){
            session()->flash('notification-status','success');
            session()->flash('notification-msg',"Your account is successfully created");
            return redirect()->route('system.account_management.user.index');
        }else{
            session()->flash('notification-status','success');
            session()->flash('notification-msg','Successfully registered.');
            goto callback;
        }

        callback:
        return redirect()->route('system.dashboard.index');
    }

    public function edit ($id = NULL) {
		$users = User::find($id);
		if (!$users) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.account_management.user.index');
		}

		if($id < 0){
			session()->flash('notification-status',"warning");
			session()->flash('notification-msg',"Unable to update special record.");
			return redirect()->route('system.account_management.user.index');	
		}

		$this->data['users'] = $users;
		return view('system.account-management.user.edit',$this->data);
	}

    public function inactive ($id = NULL) {
		try {
			$user = User::find($id);

			if (!$user) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.account_management.user.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to remove special record.");
				return redirect()->route('system.account_management.user.index');	
			}
  		$user->status = "inactive";
			if($user->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.account_management.user.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function update (UserRequest1 $request, $id = NULL) {
		try {
			$update_user = User::find($id);

			if (!$update_user) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.account_management.user.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to update special record.");
				return redirect()->route('system.account_management.user.index');	
			}
			$user = $request->user();
       $update_user->fill($request->only('first_name','last_name','mobile_number','email','username','gender','url'));
       $update_user->status = "active";
        $update_user->type = "user";
        $update_user->password = bcrypt($request->get('password'));
      	if($request->hasFile('file')) {
      	    $image = ImageUploader::upload($request->file('file'), "uploads/images/users");
      	    $update_user->path = $image['path'];
      	    $update_user->directory = $image['directory'];
      	    $update_user->filename = $image['filename'];
      	}

			if($update_user->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.account_management.user.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
	 public function active ($id = NULL) {
		try {
			$user = User::find($id);

			if (!$user) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.account_management.user.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to remove special record.");
				return redirect()->route('system.account_management.user.index');	
			}
  		$user->status = "active";
			if($user->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.account_management.user.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}


	public function list_of_transaction(){
		return view('system.account-management.user.list-of-transaction');
	}

	public function system_user(){
		return view('system.account-management.system-user.index');
	}

	public function create_system_user(){
		return view('system.account-management.system-user.create');
	}

}