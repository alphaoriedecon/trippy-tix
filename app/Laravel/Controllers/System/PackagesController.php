<?php 

namespace App\Laravel\Controllers\System;


use App\Laravel\Models\Partner;
use App\Laravel\Models\Sale;
use App\Laravel\Models\Business;
use App\Laravel\Models\Packages;
use App\Laravel\Requests\System\BlogsRequest;
use App\Laravel\Requests\System\BlogsRequest1;
use Helper, Session, Str,ImageUploader,Carbon,Auth;

class PackagesController extends Controller{

	public function index(){
			$this->data['packages'] = Packages::orderBy('updated_at',"DESC")->get();
		return view('system.content-management.packages.index',$this->data);
	}


public function store (BlogsRequest $request) {
		try {
			$new_packages = new Packages;
			$user = $request->user();
      $new_packages->fill($request->only('title','content'));
			if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/images/users/{$user->id}/packages");
			    $new_packages->path = $image['path'];
			    $new_packages->directory = $image['directory'];
			    $new_packages->filename = $image['filename'];
			}
			// $new_packages->status = $request->get('status');
			$new_packages->user_id = $request->user()->id;
			if($new_packages->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.content_management.packages.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function create(){
		return view('system.content-management.packages.create',$this->data);
	}
	public function edit ($id = NULL) {
		$packages = Packages::find($id);
		if (!$packages) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.content_management.packages.index');
		}

		if($id < 0){
			session()->flash('notification-status',"warning");
			session()->flash('notification-msg',"Unable to update special record.");
			return redirect()->route('system.content_management.packages.index');	
		}

		$this->data['packages'] = $packages;
		return view('system.content-management.packages.edit',$this->data);
	}
	
	public function update (BlogsRequest1 $request, $id = NULL) {
		try {
			$packages = Packages::find($id);

			if (!$packages) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.content_management.packages.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to update special record.");
				return redirect()->route('system.content_management.packages.index');	
			}
			$user = $request->user();
        	$packages->fill($request->only('title','content'));
        	if($request->hasFile('file')) {
        	    $image = ImageUploader::upload($request->file('file'), "uploads/images/users/{$user->id}/packages");
        	    $packages->path = $image['path'];
        	    $packages->directory = $image['directory'];
        	    $packages->filename = $image['filename'];
        	}

			if($packages->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.content_management.packages.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$packages = Packages::find($id);

			if (!$packages) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.content_management.packages.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to remove special record.");
				return redirect()->route('system.content_management.packages.index');	
			}

			if($packages->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.content_management.packages.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}


}