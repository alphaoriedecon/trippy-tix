<?php 

namespace App\Laravel\Controllers\System;


use App\Laravel\Models\Partner;
use App\Laravel\Models\Sale;
use App\Laravel\Models\Business;
use App\Laravel\Models\TransactionManager;

use Carbon,Auth;

class PartnersController extends Controller{

	public function index(){
		return view('system.content-management.partners.index');
	}

	public function create(){
		return view('system.content-management.partners.create');
	}

	public function update(){
		return view('system.content-management.partners.update');
	}

}