<?php 

namespace App\Laravel\Controllers\System;

/*
*Models
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Category;
use App\Laravel\Models\SubCategory;

/*
	Request
*/ 

use App\Laravel\Requests\System\CategoryRequest;	

use Carbon,Auth,DB;

class CategoryController extends Controller{

function __construct()
{
	$this->data = [];
}
	public function index(){

		// return Category::all();
		$this->data['categories'] = Category::orderBy('updated_at',"DESC")->get();
		return view('system.category.index',$this->data);
	}
	
	public function create(){

		$this->data['sub_category'] = SubCategory::orderBy('name',"DESC")->get();
        return  view("system.category.create",$this->data);
	}

	public function store(CategoryRequest $request)
	{
		// return $request->all();
		try {
			DB::beginTransaction();
			$category = new Category;
			$category->name = $request->name;
            $category->remarks = $request->remarks;
            $category->sub_category_id = $request->sub_category_id;

			$category->save();
			DB::commit();
			session()->flash('notification-status','success');
			session()->flash('notification-msg',"New category has been added.");
			return redirect()->route("system.category.index");

		} catch (Exception $e) {
			DB::rollback();
			session()->flash('notification-status','warning');
			session()->flash('notification-msg',"Went something wrong".$e->message());
		}
	}

	public function edit($id = NULL)
	{
        $this->data['sub_category'] = SubCategory::all();
		$this->data['category'] = Category::find($id);
		return  view("system.category.update",$this->data);
	}


	public function update(CategoryRequest $request, $id=NULL)
	{
		// return $request->name;
		try {
			DB::beginTransaction();
			$category = Category::find($id);
			$category->remarks = $request->remarks;
            $category->sub_category_id = $request->sub_category_id;
			$category->save();

			DB::commit();
			session()->flash('notification-status','success');
			session()->flash('notification-msg',"category has been modified.");
			return redirect()->route("system.category.index");
		} catch (Exception $e) {
			DB::rollback();
			session()->flash('notification-status','warning');
			session()->flash('notification-msg',"Went something wrong".$e->message());
			return redirect()->back();
		}
	}

	public function destroy($id=NULL)
	{
		try {
			DB::beginTransaction();
			Category::destroy($id);
			DB::commit();
			session()->flash('notification-status','success');
			session()->flash('notification-msg',"category has been deleted.");
			return redirect()->route("system.category.index");
		} catch (Exception $e) {
			DB::rollback();
			session()->flash('notification-status','warning');
			session()->flash('notification-msg',"Went something wrong".$e->message());
			return redirect()->back();
		}
	}


}