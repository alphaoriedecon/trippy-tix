<?php 

namespace App\Laravel\Controllers\System;


use App\Laravel\Models\Banner;
use Carbon,Auth;
use App\Laravel\Requests\System\BannerRequest;
use App\Laravel\Requests\System\BannerRequest1;
use Helper, Session, Str,ImageUploader;
class BannersController extends Controller{
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = [ 'active' => "Active","inactive" => "Inactive"];
	}
	public function index(){
		$this->data['banners'] = Banner::orderBy('updated_at',"DESC")->paginate(15);
		return view('system.content-management.banners.index',$this->data);

	}

	public function store (BannerRequest $request) {
		try {
			$new_banner = new Banner;
      $new_banner->fill($request->only('title','description'));
				if($request->hasFile('file')) {
				    $image = ImageUploader::upload($request->file('file'), "uploads/banner");
				    $new_banner->path = $image['path'];
				    $new_banner->directory = $image['directory'];
				    $new_banner->filename = $image['filename'];
			}
			if($new_banner->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.content_management.banners.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function create(){
		return view('system.content-management.banners.create');
	}

	// public function edit($id = NULL){
	// 	return view('system.content-management.banners.edit');
	// }

	public function edit ($id = NULL) {
		$banner = Banner::find($id);

		if (!$banner) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.content_management.banners.index');
		}

		if($id < 0){
			session()->flash('notification-status',"warning");
			session()->flash('notification-msg',"Unable to update special record.");
			return redirect()->route('system.content_management.banners.index');	
		}

		$this->data['banner'] = $banner;
		return view('system.content-management.banners.edit',$this->data);
	}

	public function update (BannerRequest1 $request, $id = NULL) {
		try {
			$banner = Banner::find($id);

			if (!$banner) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.content_management.banners.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to update special record.");
				return redirect()->route('system.content_management.banners.index');	
			}
			
        	$banner->fill($request->only('title','description'));
        	if($request->hasFile('file')) {
        	    $image = ImageUploader::upload($request->file('file'), "uploads/images/banner");
        	    $banner->path = $image['path'];
        	    $banner->directory = $image['directory'];
        	    $banner->filename = $image['filename'];
        	}

			if($banner->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.content_management.banners.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$banner = Banner::find($id);

			if (!$banner) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.content_management.banners.index');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to remove special record.");
				return redirect()->route('system.content_management.banners.index');	
			}

			if($banner->delete()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.content_management.banners.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}