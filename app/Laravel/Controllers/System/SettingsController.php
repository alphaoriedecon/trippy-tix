<?php 

namespace App\Laravel\Controllers\System;


use App\Laravel\Models\Partner;
use App\Laravel\Models\Sale;
use App\Laravel\Models\Business;
use App\Laravel\Models\TransactionManager;
use App\Laravel\Requests\Frontend\ChangePasswordRequest;
use Carbon,Auth;

class SettingsController extends Controller{

	public function index(){
		return view('system.settings.index');
	}

	public function update (ChangePasswordRequest $request, $id = NULL) {
		try {
			
			$update_user = User::find($id);

			if (!$update_user) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.dashboard');
			}

			if($id < 0){
				session()->flash('notification-status',"warning");
				session()->flash('notification-msg',"Unable to update special record.");
				return redirect()->route('system.dashboard');	
			}
        $update_user->password = bcrypt($request->get('password'));
      	
			if($update_user->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.dashboard');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}