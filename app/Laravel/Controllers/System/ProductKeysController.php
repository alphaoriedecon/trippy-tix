<?php 

namespace App\Laravel\Controllers\System;


use App\Laravel\Models\Product;
use App\Laravel\Models\ProductKey;

use App\Laravel\Requests\System\ProductKeyRequest;
use App\Http\Requests\PageRequest;
use Carbon,Auth,DB;

class ProductKeysController extends Controller{

function __construct()
{
	$this->data = [];
}
	public function index(PageRequest $request){


		 $this->data['product_keys'] = ProductKey::all();
		return view('system.product-keys.index',$this->data);
	}

	public function create(){

		$this->data['products']= Product::all();
		return view('system.product-keys.create',$this->data);
	}

	public function store(ProductKeyRequest $request)
	{
		// return $request->all();
		try {
			DB::beginTransaction();
			$product = new ProductKey;
			$product->product_id = $request->product_id;
			$product->product_key = $request->product_key;
			$product->expiration = $request->expiration;
			$product->qty =	$request->qty;
			$product->price =	$request->price;
			$product->save();
			DB::commit();
			session()->flash('notification-status','success');
			session()->flash('notification-msg',"New Prodcuct key has been added.");
			return redirect()->route("system.product_keys.index");

		} catch (Exception $e) {
			DB::rollback();
			session()->flash('notification-status','warning');
			session()->flash('notification-msg',"Went something wrong".$e->message());
		}
	}

public function edit($id = NULL)
	{
		$this->data['key'] = ProductKey::find($id);
		$this->data['products'] = Product::all();
		return  view("system.product-keys.update",$this->data);
	}

	public function update(ProductKeyRequest $request, $id=NULL)
	{
		// return $request->category_name;
		try {
			DB::beginTransaction();
			$product = ProductKey::find($id);
			$product->product_id = $request->product_id;
			$product->product_key = $request->product_key;
			$product->expiration = $request->expiration;
			$product->save();

			DB::commit();
			session()->flash('notification-status','success');
			session()->flash('notification-msg',"Prodcuct  has been modified.");
			return redirect()->route("system.product_keys.index");
		} catch (Exception $e) {
			DB::rollback();
			session()->flash('notification-status','warning');
			session()->flash('notification-msg',"Went something wrong".$e->message());
			return redirect()->back();
		}
	}

	public function destroy($id=NULL)
	{
		try {
			DB::beginTransaction();
			Product::destroy($id);
			DB::commit();
			session()->flash('notification-status','success');
			session()->flash('notification-msg',"Prodcuct has been deleted.");
			return redirect()->route("system.products.index");
		} catch (Exception $e) {
			DB::rollback();
			session()->flash('notification-status','warning');
			session()->flash('notification-msg',"Went something wrong".$e->message());
			return redirect()->back();
		}
	}
}