<?php 

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Models\Partner;
use App\Laravel\Models\Blogs;
use App\Laravel\Models\CartHeader;
use App\Laravel\Models\Category;
use App\Laravel\Models\MainCategory;
use App\Laravel\Models\SubCategory;
use App\Laravel\Models\TransactionManager;

use Carbon,Auth;

class BlogsController extends Controller{

	public function index(){
		
		$this->data['items'] = CartHeader::where("user_id",Auth::check() ? auth()->user()->id:"")->get();

		return view('frontend.blogs.index',$this->data);
	}
	public function details($id = NULL){
			$blogs = Blogs::find($id);

		if (!$blogs) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('frontend.blogs.details');
		}

		if($id < 0){
			session()->flash('notification-status',"warning");
			session()->flash('notification-msg',"Unable to update special record.");
			return redirect()->route('frontend.blogs.details');	
		}

		$this->data['blogs'] = $blogs;
		return view('frontend.blogs.show',$this->data);

	}

}