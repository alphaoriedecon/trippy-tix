<?php 

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Models\User;
use App\Laravel\Models\Category;
use App\Laravel\Models\MainCategory;
use App\Laravel\Models\SubCategory;
use App\Laravel\Requests\Frontend\UserRequest1;

use ImageUploader,Carbon,Auth;

class SettingsController extends Controller{

	public function index(){
        $this->data['userdata'] = User::find(Auth::check() ? auth()->user()->id:"");

        $this->data['main_categories_header'] = MainCategory::orderBy('updated_at',"DESC")->get();

        $this->data['sub_categories_header'] = SubCategory::orderBy('updated_at',"DESC")->get();

        $this->data['categories_header'] = Category::orderBy('updated_at',"DESC")->get();
		return view('frontend.my-account.settings.index',$this->data);
	}

    public function update(UserRequest1 $request)
    {
    
    try {
            $user_data = User::find(Auth::check() ? auth()->user()->id:"");

            if (!$user_data) {
                session()->flash('notification-status',"failed");
                session()->flash('notification-msg',"Record not found.");
                return redirect()->route('frontend.my-account.settings.index',$id);
            }

           
            $user_data->fill($request->only('first_name','middle_name','last_name','mobile_number','email','birthdate','gender','department','username'));
            if($request->password != null){
                $user_data->password = bcrypt($request->get('password'));
            }
            if($request->hasFile('file')) {
                $image = ImageUploader::upload($request->file('file'), "uploads/images/users/avatar");
                $user_data->path = $image['path'];
                $user_data->directory = $image['directory'];
                $user_data->filename = $image['filename'];
            }

            if($user_data->save()) {
                session()->flash('notification-status','success');
                session()->flash('notification-msg',"Record has been modified successfully.");
                return redirect()->route('frontend.home.index');
            }

            session()->flash('notification-status','failed');
            session()->flash('notification-msg','Something went wrong.');

        } catch (Exception $e) {
            session()->flash('notification-status','failed');
            session()->flash('notification-msg',$e->getMessage());
            return redirect()->back();
        }
    }
}