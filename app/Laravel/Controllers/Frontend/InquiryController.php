<?php 

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Models\Partner;
use App\Laravel\Models\Sale;
use App\Laravel\Models\CartHeader;
use App\Laravel\Models\Category;
use App\Laravel\Models\User;
use App\Laravel\Models\MainCategory;
use App\Laravel\Models\SubCategory;
use App\Laravel\Models\TransactionManager;
use App\Http\Requests\PageRequest;
use Carbon,Auth;
use App\Laravel\Models\Inquiry;



class InquiryController extends Controller{

	public function airline(){
		$this->data['agent_list'] = User::orderBy('updated_at',"DESC")->where('type','user')->where('status','active')->get();
		return view('frontend.inquiry.airline',$this->data);
	}
	public function storeAirline (PageRequest $request) {
		try {
			$new_inquiry = new Inquiry;
			$user = $request->user();
      $new_inquiry->fill($request->only('name','email','contact_number','nationality','departure_date','departure_location','arrival_date',
      	'arrival_location','agent','message','passenger','infant','days'));
			$new_inquiry->status = 'pending';
			$new_inquiry->type = 'airline';
			// $new_inquiry->user_id = $request->user()->id;
			if($new_inquiry->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('frontend.home.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
	public function appointment(){
		$this->data['agent_list'] = User::orderBy('updated_at',"DESC")->where('type','user')->where('status','active')->get();
		return view('frontend.inquiry.appointment',$this->data);
	}

	public function storeAppointment (PageRequest $request) {
		try {
			$new_inquiry = new Inquiry;
			$user = $request->user();
      $new_inquiry->fill($request->only('name','email','contact_number','agent','appointment_date','message'));
			$new_inquiry->status = 'pending';
			$new_inquiry->type = 'appointment';
			// $new_inquiry->user_id = $request->user()->id;
			if($new_inquiry->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('frontend.home.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function insured(){
		$this->data['agent_list'] = User::orderBy('updated_at',"DESC")->where('type','user')->where('status','active')->get();
		return view('frontend.inquiry.insured',$this->data);
	}
	public function storeInsured (PageRequest $request) {
		try {
			$new_inquiry = new Inquiry;
			$user = $request->user();
      $new_inquiry->fill($request->only('name','email','contact_number','nationality','departure_date','departure_location','arrival_date',
      	'arrival_location','agent','message'));
			$new_inquiry->status = 'pending';
			$new_inquiry->type = 'insured';
			// $new_inquiry->user_id = $request->user()->id;
			if($new_inquiry->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('frontend.home.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

		public function permanent_visa(){
			$this->data['agent_list'] = User::orderBy('updated_at',"DESC")->where('type','user')->where('status','active')->get();
		return view('frontend.inquiry.permanent-visa',$this->data);
	}

	public function storePermanent_visa (PageRequest $request) {
		try {
			$new_inquiry = new Inquiry;
			$user = $request->user();
      $new_inquiry->fill($request->only('name','email','contact_number','nationality','marital_status','agent','message'));
			$new_inquiry->status = 'pending';
			$new_inquiry->type = 'permanent_visa';
			// $new_inquiry->user_id = $request->user()->id;
			if($new_inquiry->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('frontend.home.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
	public function request_travel_land(){
		$this->data['agent_list'] = User::orderBy('updated_at',"DESC")->where('type','user')->where('status','active')->get();
		return view('frontend.inquiry.request-land-tour',$this->data);
	}

	public function storeRequest_travel_land (PageRequest $request) {
		try {
			$new_inquiry = new Inquiry;
			$user = $request->user();
      $new_inquiry->fill($request->only('name','email','contact_number','nationality','departure_location','arrival_location','passenger','infant','child','days','departure_date','arrival_date','agent','message'));
			$new_inquiry->status = 'pending';
			$new_inquiry->type = 'request_travel_land';
			// $new_inquiry->user_id = $request->user()->id;
			if($new_inquiry->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('frontend.home.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
	public function request_travel(){
		$this->data['agent_list'] = User::orderBy('updated_at',"DESC")->where('type','user')->where('status','active')->get();
		return view('frontend.inquiry.request-travel',$this->data);
	}

	public function storeRequest_travel (PageRequest $request) {
		try {
			$new_inquiry = new Inquiry;
			$user = $request->user();
      $new_inquiry->fill($request->only('name','email','contact_number','nationality','departure_location','arrival_location','passenger','infant','child','days','departure_date','arrival_date','agent','message'));
			$new_inquiry->status = 'pending';
			$new_inquiry->type = 'request_travel';
			// $new_inquiry->user_id = $request->user()->id;
			if($new_inquiry->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('frontend.home.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
	public function tourist_visa(){
		$this->data['agent_list'] = User::orderBy('updated_at',"DESC")->where('type','user')->where('status','active')->get();
		return view('frontend.inquiry.tourist-visa',$this->data);
	}

	public function storeTourist_visa (PageRequest $request) {
		try {
			$new_inquiry = new Inquiry;
			$user = $request->user();
      $new_inquiry->fill($request->only('name','email','contact_number','nationality','tourist_visa','agent','message'));
			$new_inquiry->status = 'pending';
			$new_inquiry->type = 'tourist_visa';
			// $new_inquiry->user_id = $request->user()->id;
			if($new_inquiry->save()) {
				session()->flash('notification-status','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('frontend.home.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	
}