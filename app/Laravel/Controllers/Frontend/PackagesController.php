<?php 

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Models\Partner;
use App\Laravel\Models\Blogs;
use App\Laravel\Models\CartHeader;
use App\Laravel\Models\Packages;
use App\Laravel\Models\MainCategory;
use App\Laravel\Models\SubCategory;
use App\Laravel\Models\TransactionManager;

use Carbon,Auth;

class PackagesController extends Controller{

	public function index(){
		
		$this->data['items'] = CartHeader::where("user_id",Auth::check() ? auth()->user()->id:"")->get();

		return view('frontend.packages.index',$this->data);
	}
	public function details($id = NULL){
			$blogs = Packages::find($id);

		if (!$blogs) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('frontend.packages.details');
		}

		if($id < 0){
			session()->flash('notification-status',"warning");
			session()->flash('notification-msg',"Unable to update special record.");
			return redirect()->route('frontend.packages.details');	
		}

		$this->data['packages'] = $blogs;
		return view('frontend.packages.show',$this->data);

	}

}