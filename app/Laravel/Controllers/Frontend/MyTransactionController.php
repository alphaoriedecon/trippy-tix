<?php 

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Models\Partner;
use App\Laravel\Models\Sale;
use App\Laravel\Models\CartHeader;
use App\Laravel\Models\TransactionManager;

use Carbon,Auth;

class MyTransactionController extends Controller{

	public function index(){
		$this->data['items'] = CartHeader::where("user_id",Auth::check() ? auth()->user()->id:"")->get();

		return view('frontend.my-account.my-transaction.index',$this->data);
	}
}