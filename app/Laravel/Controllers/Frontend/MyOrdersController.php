<?php 

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Models\Partner;
use App\Laravel\Models\Category;
use App\Laravel\Models\MainCategory;
use App\Laravel\Models\SubCategory;
use App\Laravel\Models\CartHeader;
use App\Laravel\Models\TransactionManager;

use Carbon,Auth;

class MyOrdersController extends Controller{

	public function index(){
        $this->data['main_categories_header'] = MainCategory::orderBy('updated_at',"DESC")->get();

        $this->data['sub_categories_header'] = SubCategory::orderBy('updated_at',"DESC")->get();

        $this->data['categories_header'] = Category::orderBy('updated_at',"DESC")->get();
		$this->data['items'] = CartHeader::where("user_id",Auth::check() ? auth()->user()->id:"")->get();

		return view('frontend.my-account.my-orders.index',$this->data);
	}
}