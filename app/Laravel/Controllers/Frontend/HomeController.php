<?php 

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Models\ProductImages;
use App\Laravel\Models\Product;
use App\Laravel\Models\Category;
use App\Laravel\Models\MainCategory;
use App\Laravel\Models\SubCategory;
use App\Laravel\Models\CartHeader;
use App\Laravel\Models\Blogs;
use App\Laravel\Models\Banner;
use App\Laravel\Models\Packages;
use App\Laravel\Models\User;

use Carbon,Auth;

class HomeController extends Controller{

	public function index(){
                $this->data['blogs'] = Blogs::orderBy('updated_at',"DESC")->get();
                $this->data['banners'] = Banner::orderBy('updated_at',"DESC")->get();
                $this->data['packages'] = Packages::orderBy('updated_at',"DESC")->get();
                $this->data['agent_list'] = User::orderBy('updated_at',"DESC")->where('type','user')->where('status','active')->get();
		return view('frontend.index',$this->data);
	}
}