<?php 

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Models\ProductImages;
use App\Laravel\Models\Category;
use App\Laravel\Models\MainCategory;
use App\Laravel\Models\SubCategory;
use App\Laravel\Models\CartHeader;
use App\Laravel\Models\TransactionManager;
use App\Laravel\Models\Product;
use App\Laravel\Requests\Frontend\ProductRequest;
use App\Laravel\Requests\Frontend\ProductRequest1;

use Carbon,Auth,ImageUploader;

class MyProductsController extends Controller{

	public function index(){
        $this->data['main_categories_header'] = MainCategory::orderBy('updated_at',"DESC")->get();

        $this->data['sub_categories_header'] = SubCategory::orderBy('updated_at',"DESC")->get();

        $this->data['categories_header'] = Category::orderBy('updated_at',"DESC")->get();
		$this->data['items'] = CartHeader::where("user_id",Auth::check() ? auth()->user()->id:"")->get();
        $this->data['products'] = Product::where("user_id",Auth::check() ? auth()->user()->id:"")->get();
		return view('frontend.my-account.my-products.index',$this->data);
	}

    public function create(){
        $this->data['main_categories_header'] = MainCategory::orderBy('updated_at',"DESC")->get();

        $this->data['sub_categories_header'] = SubCategory::orderBy('updated_at',"DESC")->get();

        $this->data['categories_header'] = Category::orderBy('updated_at',"DESC")->get();
        return view('frontend.my-account.my-products.create',$this->data);
    }
   
    public function edit ($id = NULL) {
        $this->data['main_categories_header'] = MainCategory::orderBy('updated_at',"DESC")->get();

        $this->data['sub_categories_header'] = SubCategory::orderBy('updated_at',"DESC")->get();

        $this->data['categories_header'] = Category::orderBy('updated_at',"DESC")->get();
        $product = Product::find($id);
        
        // $product_image = productImages::find($product_id);


        if (!$product) {
            session()->flash('notification-status',"failed");
            session()->flash('notification-msg',"Record not found.");
            return redirect()->route('frontend.my_account.my_products.edit');
        }

        if($id < 0){
            session()->flash('notification-status',"warning");
            session()->flash('notification-msg',"Unable to update special record.");
            return redirect()->route('frontend.my_account.my_products.index');  
        }
        // $this->data['news_images'] = NewsImages::Where($product_id);
        $this->data['products'] = $product;
        // $this->data['event_image'] = $product_image;
        // dd($this->data['events']);
        return view('frontend.my-account.my-products.edit',$this->data);
    }
    public function getSubCategoryList()
    {
       
    }

     public function store (ProductRequest $request) {
        try { 
            $new_product = new Product;
            $user = $request->user();
            $temp_id = time();
            if($request->hasFile('file')){
                // dd($request->file);
                foreach ($request->file as $key => $image) {
                
                    $image = ImageUploader::upload($image, "uploads/product");
                    $new_product_images = new ProductImages;
                    $new_product_images->path = $image['path'];
                    $new_product_images->directory = $image['directory'];
                    $new_product_images->filename = $image['filename'];
                    
                    $new_product_images->product_id = $temp_id;
                    $new_product_images->save();
                }
            }
            $new_product->fill($request->only('title','main_category','sub_category','type','price','qty','category','description'));
            $new_product->status = 'active';
            $new_product->user_id = $request->user()->id;
            if($new_product->save()) {
                session()->flash('notification-status','success');
                session()->flash('notification-msg',"New record has been added.");
                ProductImages::where('product_id',$temp_id)->update(['product_id' => $new_product->id]);
                return redirect()->route('frontend.my_account.my_products.index');
            }
            session()->flash('notification-status','failed');
            session()->flash('notification-msg','Something went wrong.');

            return redirect()->back();
        } catch (Exception $e) {
            session()->flash('notification-status','failed');
            session()->flash('notification-msg',$e->getMessage());
            return redirect()->back();
        }
    }

    public function update (ProductRequest1 $request, $id = NULL) {
    try {
            $product = Product::find($id);

            if (!$product) {
                session()->flash('notification-status',"failed");
                session()->flash('notification-msg',"Record not found.");
                return redirect()->route('frontend.my_account.my_products.index');
            }

            if($id < 0){
                session()->flash('notification-status',"warning");
                session()->flash('notification-msg',"Unable to update special record.");
                return redirect()->route('frontend.my_account.my_products.index');  
            }
            // $user = $request->user();

            $temp_id = time();
            if($request->hasFile('file')){
                // dd($request->file);
                foreach ($request->file as $key => $image) {
                
                    $image = ImageUploader::upload($image, "uploads/product");
                    $new_product_images = new ProductImages;
                    $new_product_images->path = $image['path'];
                    $new_product_images->directory = $image['directory'];
                    $new_product_images->filename = $image['filename'];
                    
                    $new_product_images->product_id = $temp_id;
                    $new_product_images->save();
                }
            }
            $product->fill($request->only('title','main_category','sub_category','category','type','description','price'));
            
            // if($request->hasFile('file')) {
            //     $image = ImageUploader::upload($request->file('file'), "uploads/news");
            //     $product->path = $image['path'];
            //     $product->directory = $image['directory'];
            //     $product->filename = $image['filename'];
            // }

            if($product->save()) {
                session()->flash('notification-status','success');
                session()->flash('notification-msg',"Record has been modified successfully.");
                ProductImages::where('product_id',$temp_id)->update(['product_id' => $product->id]);
                return redirect()->route('frontend.my_account.my_products.index');
            }

            session()->flash('notification-status','failed');
            session()->flash('notification-msg','Something went wrong.');

        } catch (Exception $e) {
            session()->flash('notification-status','failed');
            session()->flash('notification-msg',$e->getMessage());
            return redirect()->back();
        }
    }

    public function destroy ($id = NULL) {
        try {
            $product = Product::find($id);

            if (!$product) {
                session()->flash('notification-status',"failed");
                session()->flash('notification-msg',"Record not found.");
                return redirect()->route('frontend.my_account.my_products.index');
            }

            if($id < 0){
                session()->flash('notification-status',"warning");
                session()->flash('notification-msg',"Unable to remove special record.");
                return redirect()->route('frontend.my_account.my_products.index');  
            }

            if($product->delete()) {
                session()->flash('notification-status','success');
                session()->flash('notification-msg',"Record has been deleted.");
                return redirect()->route('frontend.my_account.my_products.index');
            }

            session()->flash('notification-status','failed');
            session()->flash('notification-msg','Something went wrong.');

        } catch (Exception $e) {
            session()->flash('notification-status','failed');
            session()->flash('notification-msg',$e->getMessage());
            return redirect()->back();
        }
    }
    public function destroy_image ($id = NULL) {
        try {
            $product_images = ProductImages::find($id);

            if (!$product_images) {
                session()->flash('notification-status',"failed");
                session()->flash('notification-msg',"Record not found.");
                return redirect()->route('frontend.my_account.my_products.edit',$product_images->product_id);
            }

            if($id < 0){
                session()->flash('notification-status',"warning");
                session()->flash('notification-msg',"Unable to remove special record.");
                return redirect()->route('frontend.my_account.my_products.edit',$product_images->product_id);  
            }

            if($product_images->delete()) {
                session()->flash('notification-status','success');
                session()->flash('notification-msg',"Record has been deleted.");
                return redirect()->route('frontend.my_account.my_products.edit',$product_images->product_id);
            }

            session()->flash('notification-status','failed');
            session()->flash('notification-msg','Something went wrong.');

        } catch (Exception $e) {
            session()->flash('notification-status','failed');
            session()->flash('notification-msg',$e->getMessage());
            return redirect()->back();
        }
    }




  
}