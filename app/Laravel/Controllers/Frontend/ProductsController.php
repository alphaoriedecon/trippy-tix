<?php 

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Models\Category;
use App\Laravel\Models\MainCategory;
use App\Laravel\Models\SubCategory;
use App\Laravel\Models\Cart;
use App\Laravel\Models\Product;
use App\Laravel\Models\ProductKey;


use App\Laravel\Requests\RequestManager;



use Helper, Str, DB,ImageUploader;
use App\Laravel\Models\User;
use Illuminate\Http\Request;
use App\Laravel\Requests\Api\UserRequest;
use App\Laravel\Requests\Api\EmployeeRequest;
use App\Laravel\Transformers\EmployeeTransformer;
use App\Laravel\Transformers\TransformerManager;
use App\Laravel\Transformers\TransactionTrasnformer;


use App\Http\Requests\PageRequest;

//Models

use App\Laravel\Models\Employee;
use App\Laravel\Models\CartHeader;
use App\Laravel\Models\CartDetail;
use App\Laravel\Models\ProductImages;
use Carbon,Auth,Session;

class ProductsController extends Controller{

	function __construct()
	{


	}

	public function index(PageRequest $request){
       $this->data['main_categories_header'] = MainCategory::orderBy('updated_at',"DESC")->get();

        $this->data['sub_categories_header'] = SubCategory::orderBy('updated_at',"DESC")->get();

        $this->data['categories_header'] = Category::orderBy('updated_at',"DESC")->get();
		

		$this->data["products"] = Product::paginate(20);

		$this->data['items'] = CartHeader::where("user_id",Auth::check() ? auth()->user()->id:"")->get();
 
		return view('frontend.products.index',$this->data);
	}

	public function show(){
        $this->data['main_categories_header'] = MainCategory::orderBy('updated_at',"DESC")->get();

        $this->data['sub_categories_header'] = SubCategory::orderBy('updated_at',"DESC")->get();

        $this->data['categories_header'] = Category::orderBy('updated_at',"DESC")->get();
		return view('frontend.products.show',$this->data);
	}

	public function cart(){
		$this->data['details'] = CartDetail::where("user_id",Auth::check() ? auth()->user()->id:"")->get();
		$this->data['items'] = CartHeader::where("user_id",Auth::check() ? auth()->user()->id:"")->get();
		// $this->data['transaction_header'] = CartHeader::where("user_id",Auth::check() ? auth()->user()->id:"")->get();

		return view('frontend.products.view_cart',$this->data);
	}
   
	public function checkout(){
		$this->data['items'] = CartHeader::where("user_id",Auth::check() ? auth()->user()->id:"")->get();

		return view('frontend.products.checkout',$this->data);
	}

	public function addToCart(PageRequest $request)
	{

		   //variables
        $user=  $request->user();
        $total = CartHeader::count();
        $total_price = 0;
        $header = new CartHeader;


        $check = CartHeader::where("user_id",$user->id)
                                  ->where("status","pending");
         $detail = CartDetail::where("user_id",$user->id)->where("product_id",$request->product_id);                         

        switch ($check->count()) {
            case 1:

            /*
                Update Transaction Details
            */ 
             if($detail->count() > 0)
             {

              $details = $detail->get()->first();
            	$request->item_qty != null ? $details->qty +=  $request->item_qty : $details->qty++;
            	$details->sub_total += $request->item_price;
              	$details->save();
             }
             else
             {

            $details = new CartDetail;
            $details->transaction_id = str_pad($total,8,0,STR_PAD_RIGHT);
            $details->user_id = auth()->user()->id;
            $details->product_name = "Microsoft Office";
            $details->item_price = $request->item_price;
            $request->item_qty == null ?$details->qty++:  $details->qty +=  $request->item_qty ;
            $details->sub_total = $request->item_price;
            $details->product_id = $request->product_id;
            $details->save();

          ;
             }
           
            

            /*
                Update Transaction Header
            */ 
            $h  = CartHeader::find($total);
            $h->user_id = $user->id;
            $h->trans_id = str_pad($total,8,0,STR_PAD_RIGHT);
            $request->item_qty == null ? $h->qty++ : $request->item_qty ;
            $h->total_price += $details->item_price;
            $h->save();

            $data = array('qty' =>$h->qty, "total_price"=>$h->total_price);
             return $data;
            break;
            
            default:

            /*
                Add new Transaction
            */ 
            $header->trans_id = str_pad(++$total,8,0,STR_PAD_RIGHT);
            $header->user_id = auth()->user()->id;
            $header->qty = $request->qty;
            $header->total_price = $request->total_price;
            $header->save();

            $details = new CartDetail;
            $details->transaction_id = $header->trans_id;
            $details->user_id = auth()->user()->id;
            $details->sub_total = $request->item_price;
            $details->product_name = "Microsoft Office";
            $details->item_price = $request->item_price;
            $request->item_qty == null ? $details->qty++ : $request->item_qty ;
            $details->sub_total = $request->item_price;
            $details->product_id = $request->product_id;
            $details->save();
            $header->qty += 1;
            $header->total_price += $request->item_price;
            $header->save();

			$data = array('qty' =>$header->qty, "total_price"=>$header->total_price);
            return $data;
            break;
        }




		

	}

	public function destroy(PageRequest $request)
	{
			// return $request->all();
		$product = CartDetail::where("product_id",$request->product_id)->get()->first();
		$header = CartHeader::where("trans_id",$product->transaction_id)->get()->first();
		$header->qty -= $product->qty;
		$header->total_price -= $product->sub_total;
		$header->save();
		$header->qty == 0 ? $header->delete() : $header->save();
		$product->delete();




	}
}