<?php namespace App\Laravel\Requests\Frontend;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class ProductRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;

		$rules = [
			
			'type'	=> "required",
			'main_category'	=> "required",
			'title'	=> "required",
			'sub_category'	=> "required",
			'category'	=> "required",
			'price'	=> "required",
			'file'	=> "required",
			'description'	=> "required",
			

		
			

		];

		if($id != 0){
			$rules['password'] = "confirmed";
		}

		return $rules;
	}

	public function messages(){
		return [
			'required'	=> "Field is required.",
			'contact.phone'	=> "Please provide a valid PH mobile number.",
			'birthdate.date'	=> "Birthdate must be a valid date.",
			'website.url'	=> "Invalid URL format. Adding http:// or https:// is also needed.",
		];
	}
}