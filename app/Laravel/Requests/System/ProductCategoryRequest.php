<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class ProductCategoryRequest extends RequestManager{

	public function rules(){
	$id = $this->route('id')?:0;
		$rules = [
			'category_name' => "required|unique:product_categories,category_name,{$id}",
			// 'sort_type'		=> "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required'	=> "Field is required.",
			'sale_date.date' => "Please indicate sales date you want to reset."
		];
	}
}