<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class CategoryRequest extends RequestManager{

	public function rules(){
	$id = $this->route('id')?:0;
		$rules = [
            'name' => "required|unique:categories,name,{$id}",
            'sub_category_id'      => "required",			// 'sort_type'		=> "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required'	=> "Field is required.",
			'sale_date.date' => "Please indicate sales date you want to reset."
		];
	}
}