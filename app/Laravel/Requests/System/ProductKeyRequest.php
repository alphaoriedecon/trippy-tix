<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class ProductKeyRequest extends RequestManager{

	public function rules(){

		$id = $this->route('product_id')?:0;
		

		$rules = [
			'product_id'	=> "required",
			'product_key'	=> "required|unique:product_keys,product_key,{$id}",
			'expiration'	=> "required",
			'qty'	=> "required",
			'price'	=> "required",
			
		];

		if(session()->get('is_admin','no') == "yes"){
			$rules['partner_id'] = "required";
		}

		return $rules;
	}

	public function messages(){
		return [
			'required'	=> "Field is required.",
			'price.numeric'	=> "Please indicate the product price.",
			'price.min'	=> "Negative price is not allowed."

		];
	}
}