<header class="color-1 hovered menu-3">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="nav"> 
            <a href="{{route('frontend.home.index')}}" class="logo">
                <img src="{{asset('frontend/img/logo/logo.PNG')}}" style="height: 60px;width: auto" alt="lets travel">
            </a>
            <div class="nav-menu-icon">
              <a href="#"><i></i></a>
            </div>
            <nav class="menu">
                <ul>
                    <li class="type-1">
                        <a href="{{route('frontend.home.index')}}">home </a>
                        
                    </li>
                     <li class="type-1">
                        <a href="{{route('frontend.home.index' , '#packages')}}">Packages</a>
                        
                    </li>
                    <li class="type-1">
                        <a href="{{route('frontend.about.index')}}">About us</a>
                        
                    </li>
                    <li class="type-1 ">
                        <a href="{{route('frontend.home.index' , '#services')}}">Services</a>
                        
                    </li>
                    <li class="type-1 ">
                        <a href="{{route('frontend.home.index' , '#team')}}">Teams</a>
                        
                    </li>
                    <li class="type-1 ">
                        <a href="{{route('frontend.home.index' , '#blogs')}}">Blogs</a>
                        
                    </li>
                 
                    
                    
                </ul>
           </nav>
           </div>
         </div>
      </div>
   </div>
  </header>