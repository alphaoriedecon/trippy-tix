	<link href="{{asset('frontend/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('frontend/css/jquery-ui.structure.min.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('frontend/css/jquery-ui.min.css')}}" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="{{asset('frontend//font-awesome/4.3.0/css/font-awesome.min.css')}}">        
	<link href="{{asset('frontend/css/style.css')}}" rel="stylesheet" type="text/css"/>    

	<style type="text/css">
    .label-p {
    	margin-left: 10px;
    	margin-bottom: 15px;
    	font-weight: bold;
    	font-size: 14px;
    	text-transform: uppercase;
    	letter-spacing: 1px
    }
    .red {
    	color: red;
    }
    .demo-1 {
  overflow: hidden;
  display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
}
@media only screen and (min-width: 768px) {
   .demo-2 {
    overflow: hidden;
    display: -webkit-box;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
  }
}
@media only screen and (max-width: 767px) {
   .demo-2 {
    overflow: hidden;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
  }
}
</style>