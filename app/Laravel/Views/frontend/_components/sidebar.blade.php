<aside class="sidebar col-lg-3">
    <div class="widget widget-dashboard">
        <h3 class="widget-title oswold-400">My Account</h3>

        <ul class="list oswold-300">
            <li><a href="#">Dashboard</a></li>          
            <li><a href="{{route('frontend.my_account.my_products.index')}}">My Products</a></li>
            <li><a href="{{route('frontend.my_account.my_orders')}}">My Orders</a></li>
            <li><a href="{{route('frontend.my_account.my_transaction')}}">My Transaction</a></li>
            <li><a href="{{route('frontend.my_account.settings')}}">Account Information</a></li>   
        </ul>
    </div><!-- End .widget -->
</aside><!-- End .col-lg-3 -->