<script src="{{asset('frontend/js/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
<script src="{{asset('frontend/js/jquery-ui.min.js')}}"></script>
<script src="{{asset('frontend/js/idangerous.swiper.min.js')}}"></script>
<script src="{{asset('frontend/js/jquery.viewportchecker.min.js')}}"></script>
<script src="{{asset('frontend/js/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('frontend/js/jquery.mousewheel.min.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;language=en"></script>	
<script src="{{asset('frontend/js/map.js')}}"></script>
<script src="{{asset('frontend/js/all.js')}}"></script>

<script type="text/javascript">
	   $(".summernote").code()
                .replace(/<\/p>/gi, "\n")
                .replace(/<br\/?>/gi, "\n")
                .replace(/<\/?[^>]+(>|$)/g, "");
</script>