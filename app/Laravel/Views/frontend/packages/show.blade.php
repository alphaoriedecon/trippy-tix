@extends('frontend._layouts.main') 
@section('content')
 

    <div class="page-loading">
        <div class="thecube">
            <div class="cube c1"></div>
            <div class="cube c2"></div>
            <div class="cube c4"></div>
            <div class="cube c3"></div>
        </div>
    </div><!--page-loading end-->

    <div class="wrapper">
            
        @include('frontend._components.header')

        <section class="page-content pt-0">
            <div class="container">
                <div class="blog-single">
                    <div class="row" style="margin:120px 0px">
                        
                        <div class="col-lg-12">
                            <div class="blog-post single">
                                <div class="blog-thumbnail" style="margin-top: 10px">
                                    <img src="{{ "{$packages->directory}/resized/{$packages->filename}" }}" alt="">
                                </div>
                                <div class="blog-info" style="margin-top: 30px;">
                                    <span>{{ date('M d, Y',strtotime($packages->created_at))}}</span>

                                    <h2 style="margin-top: 10px" class="blog-title">{{$packages->title}}</h2> <br>
                                    
                                    <p style="margin-top: 20px">{!!Str::title($packages->content)!!}</p>
                                  
                                </div>
                               
                                <!-- <div class="pagination-mint">
                                    <nav aria-label="Page navigation example">
                                        <ul class="pagination">
                                            <li class="page-item"><a class="page-link prev" href="#"><i class="fa fa-angle-left"></i>Previous</a></li>
                                            <li class="page-item"><a class="page-link active" href="#">1</a></li>
                                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                                            <li class="page-item"><a class="page-link next" href="#">Next <i class="fa fa-angle-right"></i></a></li>
                                        </ul>
                                    </nav>
                                </div><!--pagination-mint end-->
                            </div><!--blog-post single end-->
                        </div>
                    </div>
                </div><!--blog-single end-->
            </div>
        </section><!--page-content end-->

        @include('frontend._components.footer')

    </div><!--wrapper end-->

@stop