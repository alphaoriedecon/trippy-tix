@extends('frontend._layouts.main') 
@section('content')


    <div class="page-loading">
        <div class="thecube">
            <div class="cube c1"></div>
            <div class="cube c2"></div>
            <div class="cube c4"></div>
            <div class="cube c3"></div>
        </div>
    </div><!--page-loading end-->

    <div class="wrapper">
            
         @include('frontend._components.header')

        <section class="pager-section">
            <div class="container">
                <div class="pager-info">
                    <ul class="breadcrumb">
                        <li><a href="#" title="">Home</a></li>
                        <li><span>Blog</span></li>
                    </ul><!--breadcrumb end-->
                    <h2>Blog</h2>
                    <span>Our blog and news</span>
                </div>
                <div class="pger-imgs style2">
                    <div class="abt-imgz">
                        <img src="{{asset('frontend/images/resources/about3.jpg')}}" alt="">
                    </div>
                </div><!--pger-imgs end-->
                <div class="clearfix"></div>
            </div>
        </section><!--pager-section end-->

        <section class="block">
            <div class="container">
                <h3 class="sub-title">Our Blog and News</h3><!--sub-title end-->
                <div class="blog-posts blog-page">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="blog-post">
                                <div class="blog-thumbnail">
                                    <img src="{{asset('frontend/images/resources/blog1.jpg')}}" alt="">
                                    <span class="category">Lorem ipsum</span>
                                </div>
                                <div class="blog-info">
                                    <span>Jul 21, 2020</span>
                                    <h2 class="blog-title"><a href="{{route('frontend.blogs.show')}}" title="">Sed pellentesque velit a</a></h2>
                                    <p>Sed pellentesque velit a elit mattis, a volutpat neque feugiat</p>
                                    <a href="{{route('frontend.blogs.show')}}" title="" class="lnk-default2">View more <i class="la la-arrow-right"></i></a>
                                </div>
                            </div><!--blog-post end-->
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="blog-post">
                                <div class="blog-thumbnail">
                                    <img src="{{asset('frontend/images/resources/blog1.jpg')}}" alt="">
                                    <span class="category">Lorem ipsum</span>
                                </div>
                                <div class="blog-info">
                                    <span>Jul 21, 2020</span>
                                    <h2 class="blog-title"><a href="{{route('frontend.blogs.show')}}" title="">Sed pellentesque velit a</a></h2>
                                    <p>Sed pellentesque velit a elit mattis, a volutpat neque feugiat</p>
                                    <a href="{{route('frontend.blogs.show')}}" title="" class="lnk-default2">View more <i class="la la-arrow-right"></i></a>
                                </div>
                            </div><!--blog-post end-->
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="blog-post">
                                <div class="blog-thumbnail">
                                    <img src="{{asset('frontend/images/resources/blog1.jpg')}}" alt="">
                                    <span class="category">Lorem ipsum</span>
                                </div>
                                <div class="blog-info">
                                    <span>Jul 21, 2020</span>
                                    <h2 class="blog-title"><a href="{{route('frontend.blogs.show')}}" title="">Sed pellentesque velit a</a></h2>
                                    <p>Sed pellentesque velit a elit mattis, a volutpat neque feugiat</p>
                                    <a href="{{route('frontend.blogs.show')}}" title="" class="lnk-default2">View more <i class="la la-arrow-right"></i></a>
                                </div>
                            </div><!--blog-post end-->
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="blog-post">
                                <div class="blog-thumbnail">
                                    <img src="{{asset('frontend/images/resources/blog1.jpg')}}" alt="">
                                    <span class="category">Lorem ipsum</span>
                                </div>
                                <div class="blog-info">
                                    <span>Jul 21, 2020</span>
                                    <h2 class="blog-title"><a href="{{route('frontend.blogs.show')}}" title="">Sed pellentesque velit a</a></h2>
                                    <p>Sed pellentesque velit a elit mattis, a volutpat neque feugiat</p>
                                    <a href="{{route('frontend.blogs.show')}}" title="" class="lnk-default2">View more <i class="la la-arrow-right"></i></a>
                                </div>
                            </div><!--blog-post end-->
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="blog-post">
                                <div class="blog-thumbnail">
                                    <img src="{{asset('frontend/images/resources/blog1.jpg')}}" alt="">
                                    <span class="category">Lorem ipsum</span>
                                </div>
                                <div class="blog-info">
                                    <span>Jul 21, 2020</span>
                                    <h2 class="blog-title"><a href="{{route('frontend.blogs.show')}}" title="">Sed pellentesque velit a</a></h2>
                                    <p>Sed pellentesque velit a elit mattis, a volutpat neque feugiat</p>
                                    <a href="{{route('frontend.blogs.show')}}" title="" class="lnk-default2">View more <i class="la la-arrow-right"></i></a>
                                </div>
                            </div><!--blog-post end-->
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="blog-post">
                                <div class="blog-thumbnail">
                                    <img src="{{asset('frontend/images/resources/blog1.jpg')}}" alt="">
                                    <span class="category">Lorem ipsum</span>
                                </div>
                                <div class="blog-info">
                                    <span>Jul 21, 2020</span>
                                    <h2 class="blog-title"><a href="{{route('frontend.blogs.show')}}" title="">Sed pellentesque velit a</a></h2>
                                    <p>Sed pellentesque velit a elit mattis, a volutpat neque feugiat</p>
                                    <a href="{{route('frontend.blogs.show')}}" title="" class="lnk-default2">View more <i class="la la-arrow-right"></i></a>
                                </div>
                            </div><!--blog-post end-->
                        </div>
                    </div>
                </div><!--blog-posts end-->
                <div class="mint-pagination">
                    <ul class="paginated">
                        <li>
                            <a href="#" title=""><i class="fa fa-angle-left"></i></a>
                            <a href="#" title=""><i class="fa fa-angle-right"></i></a>
                        </li>
                    </ul>
                </div><!--mint-pagination end-->
            </div>
        </section>

        @include('frontend._components.footer')

    </div><!--wrapper end-->

@stop