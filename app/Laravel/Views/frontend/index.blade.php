@extends('frontend._layouts.main') 
@section('content')
 
 
  
     
<div class="loading blue-2">
    <div class="loading-center">
        <div class="loading-center-absolute">
            <div class="object object_four"></div>
            <div class="object object_three"></div>
            <div class="object object_two"></div>
            <div class="object object_one"></div>
        </div>
    </div>
</div>
  
  @include('frontend._components.header')

<!-- TOP-BANER -->
<div class="top-baner">
    <div class="swiper-container main-slider-3" data-autoplay="0" data-loop="1" data-speed="900" data-center="0" data-slides-per-view="1">
        <div class="swiper-wrapper">
            @foreach($banners as $banner)
            <div class="swiper-slide" data-val="{{$banner->id}}">
             
                    <div class="clip">
                        <div class="bg bg-bg-chrome act" style="background-image:url('{{ "{$banner->directory}/resized/{$banner->filename}" }}"></div>
                    </div>
                    <div class="vertical-bottom">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="main-title style-2 left">
                                   {{--    <h1>{{$banner->title}}</h1>
                                        <p class="color-white-light">{{$banner->content}}</p> --}}
                                    </div>        
                                </div>
                            </div>
                        </div>
                    </div>   
                       
            </div>
            @endforeach
    
        </div>    
        <div class="pagination pagination-left-2 poin-style-1"></div>       
    </div>              
</div>

<!--HOTEL-ITEM-->
<div class="main-wraper padd-120 bg-grey-2" id="packages">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="second-title">
                        <h2>Packages</h2>
{{--                           <p class="color-grey">Curabitur nunc erat, consequat in erat ut, congue bibendum nulla. Suspendisse id tor.</p>
 --}}                    </div>
                </div>
            </div>
            <div class="row">
                   <div class="arrows">                
                    <div class="swiper-container pad-15" data-autoplay="0" data-loop="0" data-speed="1000" data-slides-per-view="responsive" data-mob-slides="2" data-xs-slides="2" data-sm-slides="2" data-md-slides="3" data-lg-slides="4" data-add-slides="4">
                          <div class="swiper-wrapper">
                                 @foreach($packages as $package)
                              <div class="swiper-slide">
                                  <div class="hotel-item style-2">
                                     <div class="radius-top">
                                         <img style="height: 200px;width: 100%" src="{{ "{$package->directory}/resized/{$package->filename}" }}" alt="">             
                                     </div>
                                     <div class="title">
                                       
                                         <h4 class="demo-2"><b>{{$package->title}}</b></h4>
                                           
                                      <br>
                                     <a href="{{route('frontend.packages.details',[$package->id])}}" class="c-button bg-dr-blue hv-dr-blue-o b-50"><i class="fa fa-flag"></i> view more</a>
                                     
                                     </div>
                                  </div>
                              </div>
                              @endforeach
                             
                                               
                          </div>
                        <div class="pagination poin-style-2"></div>  
                    </div>
                  </div>
            </div>
        </div>
    </div>

<!-- HOTEL-SMALL-->
<div class="main-wraper padd-90  bg-grey-2" id="services">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="second-title">
                        <h4 class="subtitle color-blue-2 underline">our services</h4>
                    <h2>we are the best</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                   <div class="arrows">
                    <div class="swiper-container hotel-slider" data-autoplay="5000" data-loop="0" data-speed="1000" data-center="0" data-slides-per-view="responsive" data-mob-slides="1" data-xs-slides="2" data-sm-slides="2" data-md-slides="3" data-lg-slides="4" data-add-slides="4">
                          <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                        <div class="icon-block style-2 bg-white">
                                            <img class="icon-img bg-blue-2 border-grey-2" src="{{asset('frontend/img/icon_1.png')}}" alt="">
                                            <h5 class="icon-title color-dark-2">Airline Ticket <br> Inquiry</h5>
                                            {{-- <div class="icon-text color-dark-2-light">Sed sit amet leo orci. Fusce tincidunt accumsan pretium. Donec fermentum, ex non placerat.</div> --}}
                                            <a href="{{route('frontend.inquiry.airline')}}" class="c-button small bg-blue hv-blue delay-2"><span>Inquiry</span></a>
                                        </div>
                              </div>
                              <div class="swiper-slide">
                                        <div class="icon-block style-2 bg-white">
                                            <img class="icon-img bg-blue-2 border-grey-2" src="{{asset('frontend/img/icon_1.png')}}" alt="">
                                            <h5 class="icon-title color-dark-2">Request All in Inquiry</h5>
                                            {{-- <div class="icon-text color-dark-2-light">Sed sit amet leo orci. Fusce tincidunt accumsan pretium. Donec fermentum, ex non placerat.</div> --}}
                                            <a href="{{route('frontend.inquiry.request_travel')}}" class="c-button small bg-blue hv-blue delay-2"><span>Inquiry</span></a>
                                        </div>
                              </div>
                              <div class="swiper-slide">
                                  <div class="icon-block style-2 bg-white">
                                            <img class="icon-img bg-blue-2 border-grey-2" src="{{asset('frontend/img/icon_1.png')}}" alt="">
                                            <h5 class="icon-title color-dark-2">Consult Tourist Visa Inquiry</h5>
                                            {{-- <div class="icon-text color-dark-2-light">Sed sit amet leo orci. Fusce tincidunt accumsan pretium. Donec fermentum, ex non placerat.</div> --}}
                                            <a href="{{route('frontend.inquiry.tourist_visa')}}" class="c-button small bg-blue hv-blue delay-2"><span>Inquiry</span></a>
                                        </div>
                              </div>
                                <div class="swiper-slide">
                                        <div class="icon-block style-2 bg-white">
                                            <img class="icon-img bg-blue-2 border-grey-2" src="{{asset('frontend/img/icon_1.png')}}" alt="">
                                            <h5 class="icon-title color-dark-2">Request Land Tour Itinerary</h5>
                                            {{-- <div class="icon-text color-dark-2-light">Sed sit amet leo orci. Fusce tincidunt accumsan pretium. Donec fermentum, ex non placerat.</div> --}}
                                            <a href="{{route('frontend.inquiry.request_travel_land')}}" class="c-button small bg-blue hv-blue delay-2"><span>Inquiry</span></a>
                                        </div>
                              </div>
                              <div class="swiper-slide">
                                  <div class="icon-block style-2 bg-white">
                                            <img class="icon-img bg-blue-2 border-grey-2" src="{{asset('frontend/img/icon_1.png')}}" alt="">
                                            <h5 class="icon-title color-dark-2">Need Travel Insurance Inquiry</h5>
                                            {{-- <div class="icon-text color-dark-2-light">Sed sit amet leo orci. Fusce tincidunt accumsan pretium. Donec fermentum, ex non placerat.</div> --}}
                                            <a href="{{route('frontend.inquiry.insured')}}" class="c-button small bg-blue hv-blue delay-2"><span>Inquiry</span></a>
                                        </div>
                              </div>
                              {{-- <div class="swiper-slide">
                                  <div class="icon-block style-2 bg-white">
                                            <img class="icon-img bg-blue-2 border-grey-2" src="{{asset('frontend/img/icon_1.png')}}" alt="">
                                            <h5 class="icon-title color-dark-2">Promo <br> Packages</h5>
                                            <div class="icon-text color-dark-2-light">Sed sit amet leo orci. Fusce tincidunt accumsan pretium. Donec fermentum, ex non placerat.</div>
                                            <a href="{{route('frontend.home.index')}}" class="c-button small bg-blue hv-blue delay-2"><span>Inquiry</span></a>
                                        </div>
                              </div> --}}
                                <div class="swiper-slide">
                                  <div class="icon-block style-2 bg-white">
                                            <img class="icon-img bg-blue-2 border-grey-2" src="{{asset('frontend/img/icon_1.png')}}" alt="">
                                            <h5 class="icon-title color-dark-2">Apply Resident <br> Visa Inquiry</h5>
                                            {{-- <div class="icon-text color-dark-2-light">Sed sit amet leo orci. Fusce tincidunt accumsan pretium. Donec fermentum, ex non placerat.</div> --}}
                                            <a href="{{route('frontend.inquiry.permanent_visa')}}" class="c-button small bg-blue hv-blue delay-2"><span>Inquiry</span></a>
                                        </div>
                              </div>
                              <div class="swiper-slide">
                                  <div class="icon-block style-2 bg-white">
                                            <img class="icon-img bg-blue-2 border-grey-2" src="{{asset('frontend/img/icon_1.png')}}" alt="">
                                            <h5 class="icon-title color-dark-2">Set <br> Appointment</h5>
                                            {{-- <div class="icon-text color-dark-2-light">Sed sit amet leo orci. Fusce tincidunt accumsan pretium. Donec fermentum, ex non placerat.</div> --}}
                                            <a href="{{route('frontend.inquiry.appointment')}}" class="c-button small bg-blue hv-blue delay-2"><span>Inquiry</span></a>
                                        </div>
                              </div>
                              
                          </div>
                        <div class="pagination"></div>  
                            <div class="swiper-arrow-left arrows-travel"><span class="fa fa-angle-left"></span></div>
                            <div class="swiper-arrow-right arrows-travel"><span class="fa fa-angle-right"></span></div>
                    </div>
                  </div>
            </div>
        </div>
    </div>


<!-- TEAM -->
<div class="main-wraper padd-90" id="team">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="second-title">
                    <h4 class="subtitle color-dr-blue-2 underline">our team</h4>
                    <h2>MEET OUR TEAM</h2>
                </div>
            </div>
        </div>
        <div class="row">
                   <div class="arrows">
                    <div class="swiper-container hotel-slider" data-autoplay="5000" data-loop="0" data-speed="1000" data-center="0" data-slides-per-view="responsive" data-mob-slides="1" data-xs-slides="2" data-sm-slides="2" data-md-slides="3" data-lg-slides="4" data-add-slides="4">
                          <div class="swiper-wrapper">
                            @foreach($agent_list as $agent)
                              <div class="swiper-slide">
                                <a href="{{$agent->url}}" target="_blank">
                                  <div class="team-entry style-2">
                                     
{{--                                       <h5 class="team-position color-dark-2-light">Manager of Tourism</h5>                
 --}}                                      <div class="image d-flex justify-content-center">         
                                         <img class="team-img img-responsive" style="height:300px;width: auto" src="{{ "{$agent->directory}/resized/{$agent->filename}" }}" alt="">
                                      </div>  
                                       <h3 style="font-size: 18px;letter-spacing: .8px !important" class="team-name color-dark-2">{{$agent->first_name}} {{$agent->last_name}}</h3>
                                   {{-- <div class="team-social">
                                          <a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-facebook"></i></a>
                                          <a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-twitter"></i></a>
                                          <a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-skype"></i></a>
                                          <a class="color-grey-3 link-dr-blue-2" href="#"><i class="fa fa-google-plus"></i></a>
                                      </div> --}}
                                  </div>
                                </a>
                              </div>
                              
                              @endforeach
                             
            
                              
                          </div>
                        <div class="pagination"></div>  
                            <div class="swiper-arrow-left arrows-travel"><span class="fa fa-angle-left"></span></div>
                            <div class="swiper-arrow-right arrows-travel"><span class="fa fa-angle-right"></span></div>
                    </div>
                  </div>
            </div>
        
    </div>
</div>


<!-- S_NEWS-ENTRY -->  
<div class="main-wraper bg-grey-2 padd-90" id="blogs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="second-title">
                    <h4 class="subtitle color-blue-2 underline">Blogs</h4>
                    <h2>latest Blogs</h2>
                </div>
            </div>
        </div>
        <div class="row">
                   <div class="arrows">
                    <div class="swiper-container hotel-slider" data-autoplay="5000" data-loop="0" data-speed="1000" data-center="0" data-slides-per-view="responsive" data-mob-slides="2" data-xs-slides="2" data-sm-slides="2" data-md-slides="4" data-lg-slides="4" data-add-slides="4">
                          <div class="swiper-wrapper">
                            @foreach($blogs as $blog)
                              <div class="swiper-slide">
                                 <a href="{{route('frontend.blogs.details',[$blog->id])}}">
                                    <div class="s_news-entry">
                                      <img class="s_news-img img-full img-responsive" style="height: 200px;width: 100%" src="{{ "{$blog->directory}/resized/{$blog->filename}" }}" alt="">
                                      <h4 class="s_news-title demo-2"><a href="#">{{$blog->title}}</a></h4>
                                      <div class="tour-info-line clearfix">
                                          <div class="tour-info fl">
                                              <img src="{{asset('frontend/img/calendar_icon_grey.png')}}" alt="">
                                              <span class="font-style-2 color-dark-2">{{$blog->created_at->format('d/m/Y')}}</span>
                                          </div>
                                          <div class="tour-info fl">
                                              <img src="{{asset('frontend/img/people_icon_grey.png')}}" alt="">
                                              <span class="font-style-2 color-dark-2">By Admin</span>
                                          </div>
                                                              
                                      </div>
                                     {{--  <div class="s_news-text color-grey-3 demo-1">{{Str::title($blog->content)}}</div>     --}}     
                                  </div>  
                                </a>
                              </div>
                              @endforeach
                              
                             
                          
                              
                          </div>
                        <div class="pagination"></div>  
                            <div class="swiper-arrow-left arrows-travel"><span class="fa fa-angle-left"></span></div>
                            <div class="swiper-arrow-right arrows-travel"><span class="fa fa-angle-right"></span></div>
                    </div>
                  </div>
            </div>
        
    </div>
</div>


{{-- <div class="main-wraper bg-grey-2 padd-90" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="second-title">
                    <h4 class="subtitle color-blue-2 underline">contact</h4>
                    <h2>get in touch</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-8">
                <form class="contact-form">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="input-style-1 type-2 color-2">
                                <input type="text" required="" placeholder="Enter your name">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="input-style-1 type-2 color-2">
                                <input type="text" required="" placeholder="Enter your email">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="input-style-1 type-2 color-2">
                                <input type="number" required="" placeholder="Contact Number">
                            </div>
                        </div>  
                        <div class="col-xs-12 col-sm-6">
                            <div class="input-style-1 type-2 color-2">
                                <input type="text" required="" placeholder="Subject">
                            </div>
                        </div>  
                        
                        <div class="col-xs-12">
                            <textarea class="area-style-1 color-1" required="" placeholder="Enter your message"></textarea>
                            <button type="submit" class="c-button bg-blue-2 hv-blue-2-o"><span>submit</span></button>
                        </div>
                    </div>                  
                </form>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="contact-about">
                    <h4 class="color-dark-2"><strong>about us</strong></h4>
                    <p class="color-grey-3">Donec gravida euismod felis, quis dictum justo scelerisque in. Aenean nec lacus ipsum. Suspendisse vel lobortis libero, eu imperdiet purus.  Aenean nec lacus ipsum.</p>                    
                </div>
                <div class="contact-info">
                    <h4 class="color-dark-2"><strong>contact info</strong></h4>
                    <div class="contact-line color-grey-3"><img src="{{asset('frontend/img/phone_icon_2_blue.png')}}" alt="">Phone: <a class="color-dark-2" href="tel:93123456789">+93 123 456 789</a></div>                  
                    <div class="contact-line color-grey-3"><img src="{{asset('frontend/img/mail_icon_b_blue.png')}}" alt="">Email us: <a class="color-dark-2 tt" href="mailto:letstravel@world.com">sample@world.com</a></div>                    
                    <div class="contact-line color-grey-3"><img src="{{asset('frontend/img/gc_icon_blue.png')}}" alt="">Address: <span class="color-dark-2 tt">Aenean vulputate porttitor</span></div>                   
                </div>
                <div class="contact-socail">
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-skype"></i></a>
                    <a href="#"><i class="fa fa-google-plus"></i></a>
                    <a href="#"><i class="fa fa-pinterest-p"></i></a>
                    <a href="#"><i class="fa fa-instagram"></i></a>
                    <a href="#"><i class="fa fa-behance"></i></a>
                </div>
            </div>
        </div>
    </div>
</div> --}}

<!-- <div class="map-block">
    <div id="map-canvas" class="style-2" data-lat="33.664467" data-lng="-117.601586" data-zoom="10" data-style="2"></div>
    <div class="addresses-block">
        <a data-lat="33.664467" data-lng="-117.601586" data-string="Santa Monica Hotel"></a>
    </div>
</div>
 -->              

<!-- FOOTER -->
  @include('frontend._components.footer')

@stop

