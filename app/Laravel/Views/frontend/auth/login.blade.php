@extends('system._layouts.auth')

@section('content')
<a class="btn oswold-300 text-uppercase text-white btn--back bg-red" href="{{route('frontend.home.index')}}">Back to Homepage</a>
	<div class="content mt-5 content-fixed content-auth">
    <div class="container">
      <div class="media  align-items-stretch justify-content-center ht-100p pos-relative">
        {{-- <div class="media-body align-items-center d-none d-lg-flex">
          <div class="mx-wd-600">
            <img src="{{asset('assets/img/img15.png')}}" class="img-fluid" alt="">
          </div>
        </div><!-- media-body --> --}}

        <div class="sign-wrapper mg-lg-l-50 mg-xl-l-60 card p-5">

          <div class="" style="width: 300px">
            <h3 class="tx-color-01 mg-b-5 oswold-400">Sign In</h3>
            <p class="tx-color-03 tx-16 mg-b-40 oswold-300">Welcome back! Please signin to continue.</p>
            @include('system._components.notifications')
            <form action="" method="POST" id="loginForm">
              {!!csrf_field()!!}
              <div class="form-group">
                <label class="oswold-300">Username</label>
                <input type="text" class="form-control" id="username" name="username" value="{{old('username')}}">
              </div>
              <div class="form-group mb-07">
                <div class="d-flex justify-content-between mg-b-5">
                  <label class="mg-b-0-f oswold-300">Password</label>
                </div>
                <input type="password" id="password" class="form-control mb-2" name="password" placeholder="Enter your password">
                <span><a href="#" class="text-gray oswold-300">Forgot Password?</a></span>
              </div>

              <button type="submit" data-role="login" id="login" class="btn text-white btn-block bg-red oswold-300 text-uppercase">Sign in</button>
               <div class="text-center mt-2">   
                  <span class="oswold-300">Don't have an account? <a href="{{ route('frontend.auth.register') }}" class="text-red font-weight-bold">Create an Account</a></span>
                </div>
            </form>
          </div>
        </div><!-- sign-wrapper -->
      </div><!-- media -->
    </div><!-- container -->
  </div><!-- content -->
@stop


 