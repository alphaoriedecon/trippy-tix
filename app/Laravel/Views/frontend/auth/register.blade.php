@extends('system._layouts.auth')

@section('content')
<a class="btn text-white btn--back bg-red oswold-300 text-uppercase" href="{{route('frontend.home.index')}}">Back to Homepage</a>
	<div class="content mt-5 content-fixed content-auth">
    <div class="container">
      <div class="media align-items-stretch card justify-content-center ht-100p pos-relative">

        <div class="sign-wrapper mg-lg-l-50 mg-xl-l-60 p-5">

          <div class="wd-100p">
            <h3 class="tx-color-01 mg-b-5 oswold-400">Sign Up</h3>
            <p class="tx-color-03 tx-16 mg-b-40 oswold-300">Welcome back! Please sign up to continue.</p>
            @include('system._components.notifications')
            <form action="" method="POST" enctype="multipart/form-data"> 
            {{ csrf_field() }}
                
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group {{$errors->first('first_name') ? 'has-error' : NULL}}">
                    <label class="oswold-300">First Name</label>
                    <input type="text" class="form-control" value="{{old('first_name')}}" name="first_name" placeholder="" >
                    @if($errors->first('first_name'))
                    <span class="help-block">{{$errors->first('first_name')}}</span>
                    @endif
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group {{$errors->first('middle_name') ? 'has-error' : NULL}}">
                    <label class="oswold-300">Middle Name</label>
                    <input type="text" class="form-control" value="{{old('middle_name')}}" name="middle_name" placeholder="" >
                    @if($errors->first('middle_name'))
                    <span class="help-block">{{$errors->first('middle_name')}}</span>
                    @endif
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group {{$errors->first('last_name') ? 'has-error' : NULL}}">
                    <label class="oswold-300">Last Name</label>
                    <input type="text" class="form-control" value="{{old('last_name')}}" name="last_name" placeholder="" >
                    @if($errors->first('last_name'))
                    <span class="help-block">{{$errors->first('last_name')}}</span>
                    @endif
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group {{$errors->first('mobile_number') ? 'has-error' : NULL}}">
                    <label class="oswold-300">Mobile Number</label>
                    <input type="number" class="form-control" value="{{old('mobile_number')}}" name="mobile_number" placeholder="" >
                    @if($errors->first('mobile_number'))
                    <span class="help-block">{{$errors->first('mobile_number')}}</span>
                    @endif
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group {{$errors->first('gender') ? 'has-error' : NULL}}">
                    <label class="oswold-300">Gender</label>
                    <select class="form-control" value="{{old('gender')}}" name="gender">
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                  
                  @if($errors->first('gender'))
                    <span class="help-block">{{$errors->first('gender')}}</span>
                    @endif</div>
                </div>
                <div class="col-md-4">
                  <div class="form-group {{$errors->first('birthdate') ? 'has-error' : NULL}}">
                    <label class="oswold-300">Birthdate</label>
                    <input type="date" class="form-control" value="{{old('birthdate')}}" name="birthdate" >
                  
                  @if($errors->first('birthdate'))
                    <span class="help-block">{{$errors->first('birthdate')}}</span>
                    @endif</div>
                </div>
                <div class="col-md-12">
                  <div class="form-group {{$errors->first('address') ? 'has-error' : NULL}}">
                    <label class="oswold-300">Address</label>
                    <input type="text" class="form-control" value="{{old('address')}}" name="address" placeholder="" >
                  
                  @if($errors->first('address'))
                    <span class="help-block">{{$errors->first('address')}}</span>
                    @endif</div>
                </div>
                <div class="col-md-4">
                  <div class="form-group {{$errors->first('province') ? 'has-error' : NULL}}">
                    <label class="oswold-300">Province</label>
                    <input type="text" class="form-control" value="{{old('province')}}" name="province" placeholder="" >
                    @if($errors->first('province'))
                    <span class="help-block">{{$errors->first('province')}}</span>
                    @endif
                  </div>
                </div>
                 <div class="col-md-4">
                  <div class="form-group {{$errors->first('city') ? 'has-error' : NULL}}">
                    <label class="oswold-300">City</label>
                    <input type="text" class="form-control" value="{{old('city')}}" name="city" placeholder="" >
                  
                  @if($errors->first('city'))
                    <span class="help-block">{{$errors->first('city')}}</span>
                    @endif</div>
                </div>
                
                <div class="col-md-4">
                  <div class="form-group {{$errors->first('email') ? 'has-error' : NULL}}">
                    <label class="oswold-300">Email Address</label>
                    <input type="text" class="form-control" value="{{old('email')}}" name="email" placeholder="" >
                  
                  @if($errors->first('email'))
                    <span class="help-block">{{$errors->first('email')}}</span>
                    @endif</div>
                </div>
                <div class="col-md-4">
                  <div class="form-group {{$errors->first('username') ? 'has-error' : NULL}}">
                    <label class="oswold-300">Username</label>
                    <input type="text" class="form-control" value="{{old('username')}}" name="username" placeholder="" >
                    @if($errors->first('username'))
                    <span class="help-block">{{$errors->first('username')}}</span>
                    @endif
                  </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group {{$errors->first('password') ? 'has-error' : NULL}}">
                      <div class="d-flex justify-content-between mg-b-5">
                        <label class="mg-b-0-f oswold-300">Password</label>
                      </div>
                      <input type="password" value="{{old('password')}}" class="form-control mb-2" name="password">
                      @if($errors->first('password'))
                    <span class="help-block">{{$errors->first('password')}}</span>
                    @endif
                    </div>
                </div>
                 <div class="col-md-4">
                    <div class="form-group {{$errors->first('password_confirmation') ? 'has-error' : NULL}}">
                      <div class="d-flex justify-content-between mg-b-5">
                        <label class="mg-b-0-f oswold-300">Confirm Password</label>
                      </div>
                      <input type="password" value="{{old('password_confirmation')}}" class="form-control mb-2" name="password_confirmation">
                      @if($errors->first('password_confirmation'))
                    <span class="help-block">{{$errors->first('password_confirmation')}}</span>
                    @endif
                    </div>
                </div>
                 <div class="col-md-12">

                    <button type="submit" class="oswold-300 text-uppercase btn text-white btn-block bg-red">Sign Up</button>
                </div>
                <div class="col-md-12">

                 <div class="text-center mt-2">   
                    <span class="oswold-300">Already have an account? <a href="{{ route('frontend.auth.login') }}" class="text-red font-weight-bold">Sign In</a></span>
                  </div>
              </div>
                </div>
            </form>
          </div>
        </div><!-- sign-wrapper -->
      </div><!-- media -->
    </div><!-- container -->
  </div><!-- content -->
@stop