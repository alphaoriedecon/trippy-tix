@extends('frontend._layouts.main') 
@section('content')
 <div class="page-wrapper">
        @include('frontend._components.header') 
            <main class="main">
            <nav aria-label="breadcrumb" class="breadcrumb-nav">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index-2.html"><i class="icon-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">My Transaction</li>
                    </ol>
                </div><!-- End .container -->
            </nav>

            <div class="container">
                <div class="row">
                    <div class="col-lg-9 order-lg-last dashboard-content">
                        <div class="row">
                            <div class="col-md-4">
                                    <h2>My Transaction</h2>
                            </div>
                            <div class="col-md-8 text-md-right">
                                <div class="toolbox-item">
                                    <div class="toolbox-item toolbox-show">
                                        <label>Showing 1–9 of 60 results</label>
                                    </div><!-- End .toolbox-item -->

                                    <div class="layout-modes">
                                        <a href="#" class="layout-btn btn-grid active" title="Grid">
                                            <i class="icon-mode-grid"></i>
                                        </a>
                                    </div><!-- End .layout-modes -->
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <div class="table-responsive">
                                <table class="table">
                                  <thead class="bg-violet text-white">
                                    <tr>
                                      <th class="p-3" scope="col">Transaction No.</th>
                                      <th class="p-3" scope="col">Product Sold</th>
                                      <th class="p-3" scope="col">Transaction Date</th>
                                      <th class="p-3" scope="col">Payment Date</th>
                                      <th class="p-3" scope="col">Transaction Status</th>
                                      <th class="p-3" scope="col">Payment Status</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                        <th class="p-3" scope="row">13123</th>
                                        <th class="p-3" scope="row">1</th>
                                        <td class="p-3"><p>02-29-2020</p></td>
                                        <td class="p-3"><p>02-29-2020</p></td>
                                        <td class="p-3"><span class="bg-warning text-dark p-2 rounded">Pending</span></td>
                                        <td class="p-3"><span class="bg-success text-white p-2 rounded">Completed</span></td>
                                    </tr>
                                   
                                  </tbody>
                                </table>
                                <nav class="toolbox toolbox-pagination">
                                    <div class="toolbox-item toolbox-show">
                                        <label>Showing 1–9 of 60 results</label>
                                    </div><!-- End .toolbox-item -->

                                    <ul class="pagination">
                                        <li class="page-item disabled">
                                            <a class="page-link page-link-btn" href="#"><i class="icon-angle-left"></i></a>
                                        </li>
                                        <li class="page-item active">
                                            <a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
                                        </li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                                        <li class="page-item"><span>...</span></li>
                                        <li class="page-item"><a class="page-link" href="#">15</a></li>
                                        <li class="page-item">
                                            <a class="page-link page-link-btn" href="#"><i class="icon-angle-right"></i></a>
                                        </li>
                                    </ul>
                                </nav>
                            </div><!-- End .cart-table-container -->

                        
                    </div><!-- End .col-lg-8 -->
                        
                    </div><!-- End .col-lg-9 -->

                     @include('frontend._components.sidebar')
                </div><!-- End .row -->
            </div><!-- End .container -->

            <div class="mb-5"></div><!-- margin -->
        </main><!-- End .main -->
        @include('frontend._components.footer') 
       
    </div><!-- End .page-wrapper -->
    @include('frontend._components.mobile_header') 
   


    <a id="scroll-top" href="#top" role="button"><i class="icon-angle-up"></i></a>

@stop