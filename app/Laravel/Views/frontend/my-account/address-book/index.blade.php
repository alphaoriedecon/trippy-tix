@extends('frontend._layouts.main') 
@section('content')
 <div class="page-wrapper">
        @include('frontend._components.header') 
      		<main class="main">
            <nav aria-label="breadcrumb" class="breadcrumb-nav">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index-2.html"><i class="icon-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                    </ol>
                </div><!-- End .container -->
            </nav>

            <div class="container">
                <div class="row">
                    <div class="col-lg-9 order-lg-last dashboard-content">
                        <h2>Address Book</h2>
                            <div class="shipping-step-addresses">
                                <div class="shipping-address-box active">
                                    <address>
                                        Desmond Mason <br>
                                        123 Street Name, City Name <br>
                                        Los Angeles, California 03100 <br>
                                        United States <br>
                                        (123) 456-7890 <br>
                                    </address>

                                    <div class="address-box-action clearfix">
                                        <a href="#" class="btn btn-sm btn-link">
                                            Edit
                                        </a>

                                        <a href="#" class="btn btn-sm btn-outline-secondary float-right">
                                            Ship Here
                                        </a>
                                    </div><!-- End .address-box-action -->
                                </div><!-- End .shipping-address-box -->
                                <div class="shipping-address-box">
                                    <address>
                                        Susan Mason <br>
                                        123 Street Name, City Name <br>
                                        Los Angeles, California 03100 <br>
                                        United States <br>
                                        (123) 789-6150 <br>
                                    </address>

                                    <div class="address-box-action clearfix">
                                        <a href="#" class="btn btn-sm btn-link">
                                            Edit
                                        </a>

                                        <a href="#" class="btn btn-sm btn-outline-secondary float-right">
                                            Main Address
                                        </a>
                                    </div><!-- End .address-box-action -->
                                </div><!-- End .shipping-address-box -->
                            </div><!-- End .shipping-step-addresses -->
                            <a href="#" class="btn btn-sm btn-outline-secondary btn-new-address" data-toggle="modal" data-target="#addressModal">+ New Address</a>      
                        </div><!-- End .row -->
                 @include('frontend._components.sidebar')
            </div><!-- End .container -->

            <div class="mb-5"></div><!-- margin -->
        </main><!-- End .main -->
        @include('frontend._components.footer') 
       
    </div><!-- End .page-wrapper -->
    @include('frontend._components.mobile_header') 
    @include('frontend.my-account.address-book..modals.new-address') 
   


    <a id="scroll-top" href="#top" role="button"><i class="icon-angle-up"></i></a>

@stop