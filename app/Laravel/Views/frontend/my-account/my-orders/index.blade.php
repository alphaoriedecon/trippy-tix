@extends('frontend._layouts.main') 
@section('content')
 <div class="page-wrapper">
        @include('frontend._components.header') 
      		<main class="main mt-1">
            <nav aria-label="breadcrumb" class="breadcrumb-nav">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index-2.html"><i class="icon-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">My Orders</li>
                    </ol>
                </div><!-- End .container -->
            </nav>

            <div class="container">
                <div class="row">
                    <div class="col-lg-12 order-lg-last dashboard-content">
                        <div class="row">
                            <div class="col-md-4">
{{--                                     <h2 class="oswold-400">My Orders</h2>
 --}}                            </div>
                            <div class="col-md-8 text-md-right">
                                <div class="toolbox-item">
                                    <div class="toolbox-item toolbox-show">
                                        <label class="oswold-300">Showing 1–9 of 60 results</label>
                                    </div><!-- End .toolbox-item -->

                                    <div class="layout-modes">
                                        <a href="#" class="layout-btn btn-grid active" title="Grid">
                                            <i class="icon-mode-grid"></i>
                                        </a>
                                    </div><!-- End .layout-modes -->
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        	<div class="col-md-6">
                        		<div class="row">
		                            <div class="col-md-4">
		                                <img style="height: 160px;width: auto" src="{{ asset('frontend/images/house.jfif') }}" alt="product">
		                            </div>
		                            <div class="col-md-8">
		                              	<span class="oswold-300 text-uppercase">Title: 
		                              		<span class="oswold-400">Lorem ipsum</span></span>
		                              		<br>
		                              	<span class="oswold-300 text-uppercase">Location: 
		                              		<span class="oswold-400">Makati City</span></span> 
		                              		<br>
		                              	<span class="oswold-300 text-uppercase">Type: 
		                              		<span class="oswold-400">New</span></span>
		                              		<br>
		                              	<span class="oswold-300 text-uppercase">Price: 
		                              		<span class="oswold-400">PHP 1,000.00</span></span>
		                              		<br>
		                              	<span class="oswold-300 text-uppercase">Posted By: 
		                              		<span class="oswold-400">Lorem ipsum</span></span>
		                              		<br>
		                              	<span class="oswold-300 text-uppercase">Date Added: 
		                              		<span class="oswold-400">02/18/2020</span></span>

		                            </div>
		                        </div>
		                    </div>
                        </div>
                        <div>
                            <div class="cart-table-container mt-3">
                                
                                <nav class="toolbox toolbox-pagination">
                                    <div class="toolbox-item toolbox-show">
                                        <label class="oswold-300">Showing 1–9 of 60 results</label>
                                    </div><!-- End .toolbox-item -->

                                    <ul class="pagination oswold-300">
                                        <li class="page-item disabled">
                                            <a class="page-link page-link-btn" href="#"><i class="icon-angle-left"></i></a>
                                        </li>
                                        <li class="page-item active">
                                            <a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
                                        </li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                                        <li class="page-item"><span>...</span></li>
                                        <li class="page-item"><a class="page-link" href="#">15</a></li>
                                        <li class="page-item">
                                            <a class="page-link page-link-btn" href="#"><i class="icon-angle-right"></i></a>
                                        </li>
                                    </ul>
                                </nav>
                            </div><!-- End .cart-table-container -->

                        
                    </div><!-- End .col-lg-8 -->
                        
                    </div><!-- End .col-lg-9 -->

                </div><!-- End .row -->
            </div><!-- End .container -->

            <div class="mb-5"></div><!-- margin -->
        </main><!-- End .main -->
        @include('frontend._components.footer') 
       
    </div><!-- End .page-wrapper -->
    @include('frontend._components.mobile_header') 
   


    <a id="scroll-top" href="#top" role="button"><i class="icon-angle-up"></i></a>

@stop