@extends('frontend._layouts.main') 
@section('content')
 <div class="page-wrapper">
        @include('frontend._components.header') 
      		<main class="main">
            <nav aria-label="breadcrumb" class="breadcrumb-nav">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index-2.html"><i class="icon-home"></i></a></li>
                        <li class="breadcrumb-item " aria-current="page">Settings</li>
                        <li class="breadcrumb-item active" aria-current="page">Update Information</li>
                    </ol>
                </div><!-- End .container -->
            </nav>

            <div class="container mt-2">

                <div class="row">
                    <div class="col-lg-12 order-lg-last dashboard-content">
{{--                         <h2>Edit Account Information</h2>
 --}}                        
                        <form action="#" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                 <div class="col-md-12 text-center mb-3 {{$errors->first('file') ? 'has-error' : NULL}}">
                                        <div class="row">
                                             <div class="col-md-12 mb-2 d-flex justify-content-center">
                                        <img src="{{\Auth::user()->avatar}}" style="border-radius: 50%;height: 170px" id="blah"  width="auto" />

                                            </div>
                                        <div class="col-md-12">
                                        <input name="file" type="file" 
                                            onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])">
                                            @if($errors->first('file'))
                                            <span class="help-block oswold-300 text-danger">{{$errors->first('file')}}</span>
                                            @endif
                                        </div>
                                        </div>
                                    </div>
                                <div class="col-md-4">
                                  <div class="form-group {{$errors->first('first_name') ? 'has-error' : NULL}}">
                                    <label class="oswold-300">First Name</label>
                                    <input type="text" class="form-control" value="{{old('first_name',(\Auth::user()->first_name))}}" name="first_name" placeholder="" >
                                    @if($errors->first('first_name'))
                                    <span class="help-block oswold-300 text-danger">{{$errors->first('first_name')}}</span>
                                    @endif
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="form-group {{$errors->first('middle_name') ? 'has-error' : NULL}}">
                                    <label class="oswold-300">Middle Name</label>
                                    <input type="text" class="form-control" value="{{old('middle_name',(\Auth::user()->middle_name))}}" name="middle_name" placeholder="" >
                                    @if($errors->first('middle_name'))
                                    <span class="help-block oswold-300 text-danger">{{$errors->first('middle_name')}}</span>
                                    @endif
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="form-group {{$errors->first('last_name') ? 'has-error' : NULL}}">
                                    <label class="oswold-300">Last Name</label>
                                    <input type="text" class="form-control" value="{{old('last_name',(\Auth::user()->last_name))}}" name="last_name" placeholder="" >
                                    @if($errors->first('last_name'))
                                    <span class="help-block oswold-300 text-danger">{{$errors->first('last_name')}}</span>
                                    @endif
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="form-group {{$errors->first('mobile_number') ? 'has-error' : NULL}}">
                                    <label class="oswold-300">Mobile Number</label>
                                    <input type="number" class="form-control" value="{{old('mobile_number',(\Auth::user()->mobile_number))}}" name="mobile_number" placeholder="" >
                                    @if($errors->first('mobile_number'))
                                    <span class="help-block oswold-300 text-danger">{{$errors->first('mobile_number')}}</span>
                                    @endif
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="form-group {{$errors->first('gender') ? 'has-error' : NULL}}">
                                    <label class="oswold-300">Gender</label>
                                    <select class="form-control"  name="gender">
                                        @if(\Auth::user()->gender == 'male')
                                        <option selected="" value="male">Male</option>
                                        <option value="female">Female</option>
                                        @else
                                        <option value="male">Male</option>

                                        <option selected="" value="female">Female</option>
                                        @endif
                                    </select>
                                  
                                  @if($errors->first('gender'))
                                    <span class="help-block oswold-300 text-danger">{{$errors->first('gender')}}</span>
                                    @endif</div>
                                </div>
                                <div class="col-md-4">
                                  <div class="form-group {{$errors->first('birthdate') ? 'has-error' : NULL}}">
                                    <label class="oswold-300">Birthdate</label>
                                    <input type="date" class="form-control" value="{{old('birthdate',(\Auth::user()->birthdate))}}" name="birthdate" >
                                  
                                  @if($errors->first('birthdate'))
                                    <span class="help-block oswold-300 text-danger">{{$errors->first('birthdate')}}</span>
                                    @endif</div>
                                </div>
                                <div class="col-md-12">
                                  <div class="form-group {{$errors->first('address') ? 'has-error' : NULL}}">
                                    <label class="oswold-300">Address</label>
                                    <input type="text" class="form-control" value="{{old('address',(\Auth::user()->address))}}" name="address" placeholder="" >
                                  
                                  @if($errors->first('address'))
                                    <span class="help-block oswold-300 text-danger">{{$errors->first('address')}}</span>
                                    @endif</div>
                                </div>
                                <div class="col-md-4">
                                  <div class="form-group {{$errors->first('province') ? 'has-error' : NULL}}">
                                    <label class="oswold-300">Province</label>
                                    <input type="text" class="form-control" value="{{old('province',(\Auth::user()->province))}}" name="province" placeholder="" >
                                    @if($errors->first('province'))
                                    <span class="help-block oswold-300 text-danger">{{$errors->first('province')}}</span>
                                    @endif
                                  </div>
                                </div>
                                 <div class="col-md-4">
                                  <div class="form-group {{$errors->first('city') ? 'has-error' : NULL}}">
                                    <label class="oswold-300">City</label>
                                    <input type="text" class="form-control" value="{{old('city',(\Auth::user()->city))}}" name="city" placeholder="" >
                                  
                                  @if($errors->first('city'))
                                    <span class="help-block oswold-300 text-danger">{{$errors->first('city')}}</span>
                                    @endif</div>
                                </div>
                                
                                <div class="col-md-4">
                                  <div class="form-group {{$errors->first('email') ? 'has-error' : NULL}}">
                                    <label class="oswold-300">Email Address</label>
                                    <input type="text" disabled="" class="form-control" value="{{old('email',(\Auth::user()->email))}}" name="email" placeholder="" >
                                  
                                  @if($errors->first('email'))
                                    <span class="help-block oswold-300 text-danger">{{$errors->first('email')}}</span>
                                    @endif</div>
                                </div>
                                <div class="col-md-4">
                                  <div class="form-group {{$errors->first('username') ? 'has-error' : NULL}}">
                                    <label class="oswold-300">Username</label>
                                    <input disabled="" type="text" class="form-control" value="{{old('username',(\Auth::user()->username))}}" name="username" placeholder="" >
                                    @if($errors->first('username'))
                                    <span class="help-block oswold-300 text-danger">{{$errors->first('username')}}</span>
                                    @endif
                                  </div>
                                </div>
                                
                            </div>

                            <div class="mb-2"></div><!-- margin -->

                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="change-pass-checkbox" value="1">
                                <label class="custom-control-label ml-3" for="change-pass-checkbox">Change Password</label>
                            </div><!-- End .custom-checkbox -->

                            <div id="account-chage-pass">
                                <h3 class="mb-2 mt-3">Change Password</h3>
                                <div class="row">
                                    <div class="col-md-6">
                                    <div class="form-group {{$errors->first('password') ? 'has-error' : NULL}}">
                                      <div class="d-flex justify-content-between mg-b-5">
                                        <label class="mg-b-0-f oswold-300">Password</label>
                                      </div>
                                      <input type="password" value="{{old('password')}}" class="form-control mb-2" name="password">
                                      @if($errors->first('password'))
                                    <span class="help-block oswold-300 text-danger">{{$errors->first('password')}}</span>
                                    @endif
                                </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group {{$errors->first('password_confirmation') ? 'has-error' : NULL}}">
                                      <div class="d-flex justify-content-between mg-b-5">
                                        <label class="mg-b-0-f oswold-300">Confirm Password</label>
                                      </div>
                                      <input type="password" value="{{old('password_confirmation')}}" class="form-control mb-2" name="password_confirmation">
                                      @if($errors->first('password_confirmation'))
                                    <span class="help-block oswold-300 text-danger">{{$errors->first('password_confirmation')}}</span>
                                    @endif
                                    </div>
                                </div>
                                </div><!-- End .row -->
                            </div><!-- End #account-chage-pass -->

                            <div class="required text-right">* Required Field</div>
                            <div class="form-footer">
                                <a href="#"><i class="icon-angle-double-left"></i>Back</a>

                                <div class="form-footer-right">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div><!-- End .form-footer -->
                        </form>
                    </div><!-- End .col-lg-9 -->

{{--                      @include('frontend._components.sidebar')
 --}}                </div><!-- End .row -->
            </div><!-- End .container -->

            <div class="mb-5"></div><!-- margin -->
        </main><!-- End .main -->
        @include('frontend._components.footer') 
       
    </div><!-- End .page-wrapper -->
    @include('frontend._components.mobile_header') 
   


    <a id="scroll-top" href="#top" role="button"><i class="icon-angle-up"></i></a>

@stop