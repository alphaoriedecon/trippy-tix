@extends('frontend._layouts.main') 
@section('content')
 <div class="page-wrapper">
        @include('frontend._components.header') 
      		<main class="main mt-1">
            <nav aria-label="breadcrumb" class="breadcrumb-nav">
                <div class="container">
                    <ol class="breadcrumb ">
                        <li class="breadcrumb-item oswold-300"><a href="{{ route('frontend.home.index') }}"><i class="icon-home"></i></a></li>
                        <li class="breadcrumb-item oswold-300"><a href="{{ route('frontend.my_account.my_products.index') }}">My Products</a></li>
                        <li class="breadcrumb-item oswold-400 active" aria-current="page">Create Products</li>
                    </ol>
                </div><!-- End .container -->
            </nav>

            <div class="container">
            <form action="" method="POST" enctype="multipart/form-data">                 
               {{ csrf_field() }}
                <div class="row">

                    <div class="col-lg-12 order-lg-last dashboard-content">
                        <div class="row">
                            <div class="mt-2 col-md-6">
                                <label class="oswold-400 text-uppercase">Title</label>
                                <input type="text" class="form-control" name="title" value="{{old('title')}}">
                                 @if($errors->has("title"))
                                  <span class="text text-danger text-right oswold-300">{{ $errors->first("title") }}</span>
                                  @endif
                            </div>
                          
                            <div class="mt-2 col-md-6">
                                <label class="oswold-400 text-uppercase">Main Category</label>
                                <select name="main_category" id="main_category" class="form-control">
                                    <option value="" selected="" disabled="">-- select type --</option>
                                    @foreach ($main_categories_header as $main_category)
                                    <option value="{{$main_category['name'] }}">{{ $main_category['name'] }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has("main_category"))
                                  <span class="text text-danger text-right oswold-300">{{ $errors->first("main_category") }}</span>
                                  @endif
                            </div>
                            <div class="mt-2 col-md-6">
                                <label class="oswold-400 text-uppercase">Sub Category</label>
                                <select name="sub_category" id="sub_category" class="form-control">
                                    <option value="" selected="" disabled="">-- select type --</option>
                                    @foreach ($sub_categories_header as $sub_category)
                                    <option value="{{$sub_category['name'] }}">{{ $sub_category['name'] }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has("sub_category"))
                                  <span class="text text-danger text-right oswold-300">{{ $errors->first("sub_category") }}</span>
                                  @endif
                            </div>
                            <div class="mt-2 col-md-6">
                                <label class="oswold-400 text-uppercase">Category</label>
                                <select name="category" id="category" class="form-control">
                                    <option value="" selected="" disabled="">-- select type --</option>
                                   @foreach ($categories_header as $category)
                                    <option value="{{$category['name'] }}">{{ $category['name'] }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has("category"))
                                  <span class="text text-danger text-right oswold-300">{{ $errors->first("category") }}</span>
                                  @endif
                            </div>
                            <div class="mt-2 col-md-6">
                                <label class="oswold-400 text-uppercase">Type</label>
                                <select name="type" class="form-control">
                                    <option value="" selected="" disabled="">-- select type --</option>
                                    <option value="new">New</option>
                                    <option value="used">Used</option>

                                </select>
                                @if($errors->has("type"))
                                  <span class="text text-danger text-right oswold-300">{{ $errors->first("type") }}</span>
                                  @endif
                            </div>
                            <div class="mt-2 col-md-6">
                                <label class="oswold-400 text-uppercase">Price</label>
                                <input type="number" class="form-control" name="price">
                                @if($errors->has("price"))
                                  <span class="text text-danger text-right oswold-300">{{ $errors->first("price") }}</span>
                                  @endif
                            </div>
                              <div class="mt-2 col-md-6">
                                <label class="oswold-400 text-uppercase">Images</label>
                                <input type="file" multiple="" class="form-control" name="file[]">
                                @if($errors->has("file"))
                                  <span class="text text-danger text-right oswold-300">{{ $errors->first("file") }}</span>
                                  @endif
                            </div>
                            <div class="mt-2 col-md-12">
                                <label class="oswold-400 text-uppercase">Description</label>
                                <textarea name="description" id="ckEditor" value="{{old('description')}}"></textarea>
                                @if($errors->has("Description"))
                                  <span class="text text-danger text-right oswold-300">{{ $errors->first("Description") }}</span>
                                  @endif
                            </div>
                             <div class="mt-2 col-md-12 text-md-right">
                                <button type="submit" class="btn bg-red text-white oswold-300 rounded">Submit</button>
                            </div>
                        </div>
                        
                        
                        
                        
                    </div><!-- End .col-lg-9 -->

                </div><!-- End .row -->
            </form>
            </div><!-- End .container -->

            <div class="mb-5"></div><!-- margin -->
        </main><!-- End .main -->
        @include('frontend._components.footer') 
       
    </div><!-- End .page-wrapper -->
    @include('frontend._components.mobile_header') 
   


    <a id="scroll-top" href="#top" role="button"><i class="icon-angle-up"></i></a>

@stop
