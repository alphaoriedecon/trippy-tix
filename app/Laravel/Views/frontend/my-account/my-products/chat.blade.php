@extends('frontend._layouts.main') @section('content')
<div class="page-wrapper">
    @include('frontend._components.header')
    <main class="main mt-1">
        <nav aria-label="breadcrumb" class="breadcrumb-nav">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item oswold-300">
                        <a href="{{ route('frontend.home.index') }}"><i class="icon-home"></i></a>
                    </li>

                    <li class="breadcrumb-item oswold-400 active" aria-current="page">Findgum Chat Room</li>
                </ol>
            </div>
            <!-- End .container -->
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-lg-12 mt-1">
                    <h3 class="oswold-400"></h3>
                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-8 "></div>
                        <div class="col-md-4 overflow-400">
                            <div class="p-4">
                                <div class="row chat-wrapper">
                                    <div class="col-3">
                                        <img src="{{ asset('frontend/images/avatar.png') }}" alt="product" class="avatar-items-chat" />
                                        <span class="notification-num">2</span>
                                    </div>
                                    <div class="col-9">
                                        <span class="oswold-400 text-red">Dhen Mark Torreno (Buyer)</span>
                                        <p class="oswold-300">asdsadsadsa sadadsads</p>
                                    </div>
                                </div>
                            </div>
                            <div class="p-4">
                                <div class="row chat-wrapper">
                                    <div class="col-3">
                                        <img src="{{ asset('frontend/images/avatar.png') }}" alt="product" class="avatar-items-chat" />
                                        <span class="notification-num">2</span>
                                    </div>
                                    <div class="col-9">
                                        <span class="oswold-400 text-red">Dhen Mark Torreno (Buyer)</span>
                                        <p class="oswold-300">asdsadsadsa sadadsads</p>
                                    </div>
                                </div>
                            </div>
                            <div class="p-4">
                                <div class="row chat-wrapper">
                                    <div class="col-3">
                                        <img src="{{ asset('frontend/images/avatar.png') }}" alt="product" class="avatar-items-chat" />
                                        <span class="notification-num">2</span>
                                    </div>
                                    <div class="col-9">
                                        <span class="oswold-400 text-red">Dhen Mark Torreno (Buyer)</span>
                                        <p class="oswold-300">asdsadsadsa sadadsads</p>
                                    </div>
                                </div>
                            </div>
                            <div class="p-4">
                                <div class="row chat-wrapper">
                                    <div class="col-3">
                                        <img src="{{ asset('frontend/images/avatar.png') }}" alt="product" class="avatar-items-chat" />
                                        <span class="notification-num">2</span>
                                    </div>
                                    <div class="col-9">
                                        <span class="oswold-400 text-red">Dhen Mark Torreno (Buyer)</span>
                                        <p class="oswold-300">asdsadsadsa sadadsads</p>
                                    </div>
                                </div>
                            </div>
                            <div class="p-4">
                                <div class="row chat-wrapper">
                                    <div class="col-3">
                                        <img src="{{ asset('frontend/images/avatar.png') }}" alt="product" class="avatar-items-chat" />
                                        <span class="notification-num">2</span>
                                    </div>
                                    <div class="col-9">
                                        <span class="oswold-400 text-red">Dhen Mark Torreno (Buyer)</span>
                                        <p class="oswold-300">asdsadsadsa sadadsads</p>
                                    </div>
                                </div>
                            </div>
                            <div class="p-4">
                                <div class="row chat-wrapper">
                                    <div class="col-3">
                                        <img src="{{ asset('frontend/images/avatar.png') }}" alt="product" class="avatar-items-chat" />
                                        <span class="notification-num">2</span>
                                    </div>
                                    <div class="col-9">
                                        <span class="oswold-400 text-red">Dhen Mark Torreno (Buyer)</span>
                                        <p class="oswold-300">asdsadsadsa sadadsads</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8" style="background-color: #ecf0f5; height: 400px;">
                            <p class="mt-1 oswold-400 text-uppercase title-chat">#2132 Title of the product (used) - <span class="text-red">PHP 1000.00</span></p>
                            <div class="row overflow-335">
                                {{-- Receiver --}}
                                <div class="col-md-12">
                                    <div class="d-flex flex-row">
                                        <img src="{{ asset('frontend/images/avatar.png') }}" alt="product" class="avatar-items" />
                                        <div class="d-flex flex-column">
                                            <span class="ml-2 mt-1 p-2 pl-3 pr-3 chat-receiver">dadasdasd sadasdasdsa dasdas d </span>
                                            <div class="d-flex justify-content-end">
                                                <span class="oswold-300 f-12-size">1 Min Ago</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- Sender --}}
                                <div class="col-md-12">
                                    <div class="d-flex justify-content-end">
                                        <div class="d-flex flex-row">
                                            <div class="d-flex flex-column">
                                                <span class="mr-2 mt-1 p-2 pl-3 pr-3 chat-sender">dadasdasd sadasdasdsa dasdas d </span>
                                                <div class="d-flex justify-content-start">
                                                    <span class="oswold-300 f-12-size">1 Min Ago</span>
                                                </div>
                                            </div>
                                            <img src="{{ asset('frontend/images/avatar.png') }}" alt="product" class="avatar-items" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="d-flex justify-content-end">
                                        <div class="d-flex flex-row">
                                            <div class="d-flex flex-column">
                                                <span class="mr-2 mt-1 p-2 pl-3 pr-3 chat-sender">dadasdasd sadasdasdsa dasdas d </span>
                                                <div class="d-flex justify-content-start">
                                                    <span class="oswold-300 f-12-size">1 Min Ago</span>
                                                </div>
                                            </div>
                                            <img src="{{ asset('frontend/images/avatar.png') }}" alt="product" class="avatar-items" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="d-flex justify-content-end">
                                        <div class="d-flex flex-row">
                                            <div class="d-flex flex-column">
                                                <span class="mr-2 mt-1 p-2 pl-3 pr-3 chat-sender">dadasdasd sadasdasdsa dasdas d </span>
                                                <div class="d-flex justify-content-start">
                                                    <span class="oswold-300 f-12-size">1 Min Ago</span>
                                                </div>
                                            </div>
                                            <img src="{{ asset('frontend/images/avatar.png') }}" alt="product" class="avatar-items" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="d-flex justify-content-end">
                                        <div class="d-flex flex-row">
                                            <div class="d-flex flex-column">
                                                <span class="mr-2 mt-1 p-2 pl-3 pr-3 chat-sender">dadasdasd sadasdasdsa dasdas d </span>
                                                <div class="d-flex justify-content-start">
                                                    <span class="oswold-300 f-12-size">1 Min Ago</span>
                                                </div>
                                            </div>
                                            <img src="{{ asset('frontend/images/avatar.png') }}" alt="product" class="avatar-items" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="d-flex justify-content-end">
                                        <div class="d-flex flex-row">
                                            <div class="d-flex flex-column">
                                                <span class="mr-2 mt-1 p-2 pl-3 pr-3 chat-sender">dadasdasd sadasdasdsa dasdas d </span>
                                                <div class="d-flex justify-content-start">
                                                    <span class="oswold-300 f-12-size">1 Min Ago</span>
                                                </div>
                                            </div>
                                            <img src="{{ asset('frontend/images/avatar.png') }}" alt="product" class="avatar-items" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="d-flex justify-content-end">
                                        <div class="d-flex flex-row">
                                            <div class="d-flex flex-column">
                                                <span class="mr-2 mt-1 p-2 pl-3 pr-3 chat-sender">dadasdasd sadasdasdsa dasdas d </span>
                                                <div class="d-flex justify-content-start">
                                                    <span class="oswold-300 f-12-size">1 Min Ago</span>
                                                </div>
                                            </div>
                                            <img src="{{ asset('frontend/images/avatar.png') }}" alt="product" class="avatar-items" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="d-flex justify-content-end">
                                        <div class="d-flex flex-row">
                                            <div class="d-flex flex-column">
                                                <span class="mr-2 mt-1 p-2 pl-3 pr-3 chat-sender">dadasdasd sadasdasdsa dasdas d </span>
                                                <div class="d-flex justify-content-start">
                                                    <span class="oswold-300 f-12-size">1 Min Ago</span>
                                                </div>
                                            </div>
                                            <img src="{{ asset('frontend/images/avatar.png') }}" alt="product" class="avatar-items" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-1">
                        <div class="col-md-4"></div>
                        <div class="col-md-8" style="padding-left: 0px !important; padding-right: 0px !important;">
                            <input type="text" style="max-width: 100%;" class="form-control" name="" />
                        </div>
                    </div>
                </div>
                <!-- End .col-lg-9 -->
            </div>
            <!-- End .row -->
        </div>
        <!-- End .container -->

        <div class="mb-5"></div>
        <!-- margin -->
    </main>
    <!-- End .main -->
    @include('frontend._components.footer')
</div>
<!-- End .page-wrapper -->
@include('frontend._components.mobile_header')

<a id="scroll-top" href="#top" role="button"><i class="icon-angle-up"></i></a>

@stop
