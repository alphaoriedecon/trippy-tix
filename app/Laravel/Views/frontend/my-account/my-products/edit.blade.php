@extends('frontend._layouts.main') 
@section('content')
 <div class="page-wrapper">
        @include('frontend._components.header') 
      		<main class="main mt-1">
            <nav aria-label="breadcrumb" class="breadcrumb-nav">
                <div class="container">
                    <ol class="breadcrumb ">
                        <li class="breadcrumb-item oswold-300"><a href="{{ route('frontend.home.index') }}"><i class="icon-home"></i></a></li>
                        <li class="breadcrumb-item oswold-300"><a href="{{ route('frontend.my_account.my_products.index') }}">My Products</a></li>
                        <li class="breadcrumb-item oswold-400 active" aria-current="page">Create Products</li>
                    </ol>
                </div><!-- End .container -->
            </nav>

            <div class="container">
            <form action="" method="POST" enctype="multipart/form-data">                 
               {{ csrf_field() }}
                <div class="row">

                    <div class="col-lg-12 order-lg-last dashboard-content">
                        <div class="row">
                            <div class="mt-2 col-md-6">
                                <label class="oswold-400 text-uppercase">Title</label>
                                <input type="text" class="form-control" name="title" value="{{old('title',$products->title)}}">
                                 @if($errors->has("title"))
                                  <span class="text text-danger text-right oswold-300">{{ $errors->first("title") }}</span>
                                  @endif
                            </div>
                          
                            <div class="mt-2 col-md-6">
                                <label class="oswold-400 text-uppercase">Main Category</label>
                                <select name="main_category" id="main_category" class="form-control">
                                    <option value="" selected="" disabled="">-- select type --</option>
                                    @foreach ($main_categories_header as $main_category)
                                    <option @if ($main_category->name == $products->main_category) {{ 'selected' }} @endif value="{{ $main_category->name }}">{{Str::title( $main_category->name )}}
                                    </option>
{{--                                     <option value="{{$main_category['name'] }}">{{ $main_category['name'] }}</option>
 --}}                                @endforeach
                                </select>
                                @if($errors->has("main_category"))
                                  <span class="text text-danger text-right oswold-300">{{ $errors->first("main_category") }}</span>
                                  @endif
                            </div>
                            <div class="mt-2 col-md-6">
                                <label class="oswold-400 text-uppercase">Sub Category</label>
                                <select name="sub_category" id="sub_category" class="form-control">
                                    <option value="" selected="" disabled="">-- select type --</option>
                                    @foreach ($sub_categories_header as $sub_category)
                                    <option @if ($sub_category->name == $products->sub_category) {{ 'selected' }} @endif value="{{ $sub_category->name }}">{{Str::title( $sub_category->name )}}
                                    </option>
                                    @endforeach
                                </select>
                                @if($errors->has("sub_category"))
                                  <span class="text text-danger text-right oswold-300">{{ $errors->first("sub_category") }}</span>
                                  @endif
                            </div>
                            <div class="mt-2 col-md-6">
                                <label class="oswold-400 text-uppercase">Category</label>
                                <select name="category" id="category" class="form-control">
                                    <option value="" selected="" disabled="">-- select type --</option>
                                   @foreach ($categories_header as $category)
                                   <option @if ($category->name == $products->category) {{ 'selected' }} @endif value="{{ $category->name }}">{{Str::title( $category->name )}}
                                    </option>
                                    @endforeach
                                </select>
                                @if($errors->has("category"))
                                  <span class="text text-danger text-right oswold-300">{{ $errors->first("category") }}</span>
                                  @endif
                            </div>
                            <div class="mt-2 col-md-6">
                                <label class="oswold-400 text-uppercase">Type</label>
                                <select name="type" class="form-control">
                                    <option value="" selected="" disabled="">-- select type --</option>
                                    @if($products->type == 'new')
                                    <option selected="" value="new">New</option>
                                    <option value="used">Used</option>

                                    @else
                                     <option value="new">New</option>
                                    <option selected=""  value="used">Used</option>
                                    @endif

                                </select>
                                @if($errors->has("type"))
                                  <span class="text text-danger text-right oswold-300">{{ $errors->first("type") }}</span>
                                  @endif
                            </div>
                            <div class="mt-2 col-md-6">
                                <label class="oswold-400 text-uppercase">Price</label>
                                <input type="number" class="form-control" value="{{old('price',$products->price)}}" name="price">
                                @if($errors->has("price"))
                                  <span class="text text-danger text-right oswold-300">{{ $errors->first("price") }}</span>
                                  @endif
                            </div>
                            <div class="col-md-12">
                              <div class="row">
                                 @forelse($products->images as $index)
                                 <div class="col-md-3">
                                  <a class="action-delete" style="position: absolute;top: 0;z-index: 100;left: 0" data-toggle="modal" data-target="#confirm-delete" title="Remove Record" data-url="{{route('frontend.my_account.my_products.destroy_image',[$index->id])}}">
                                    <i class="far fa-times-circle fa-2x text-danger bg-white mb-3" style="border-radius: 50%;"></i>
                                  </a>
                                  <input type="checkbox" id="cb{{$index->id}}"  name="file" />
                                    <label class="checkbox-edit--image" for="cb{{$index->id}}">
                                        <div class="">
                                            <div class="card-body" style="padding: 0px !important;">
                                                <div class="row">
                                                         <img src="{{ "{$index->directory}/resized/{$index->filename}" }}" style="width: auto; height: 100px;">
                                                  
                                                </div>
                                            </div>
                                        </div>
                                    </label>
                                  
                                  </div>
                                
                                @empty

                                 
                                @endforelse
                              </div>
                            </div>
                              <div class="mt-2 col-md-6">
                                <label class="oswold-400 text-uppercase">Images</label>
                                <input type="file" multiple="" class="form-control" name="file[]">
                                @if($errors->has("file"))
                                  <span class="text text-danger text-right oswold-300">{{ $errors->first("file") }}</span>
                                  @endif
                            </div>
                            <div class="mt-2 col-md-12">
                                <label class="oswold-400 text-uppercase">Description</label>
                                <textarea name="description" id="ckEditor" value="">{{old('description',$products->description)}}</textarea>
                                @if($errors->has("Description"))
                                  <span class="text text-danger text-right oswold-300">{{ $errors->first("Description") }}</span>
                                  @endif
                            </div>
                             <div class="mt-2 col-md-12 text-md-right">
                                <button type="submit" class="btn bg-red text-white oswold-300 rounded">Submit</button>
                            </div>
                        </div>
                        
                        
                        
                        
                    </div><!-- End .col-lg-9 -->

                </div><!-- End .row -->
            </form>
            </div><!-- End .container -->

            <div class="mb-5"></div><!-- margin -->
        </main><!-- End .main -->
        @include('frontend._components.footer') 
       
    </div><!-- End .page-wrapper -->
    @include('frontend._components.mobile_header') 
   


    <a id="scroll-top" href="#top" role="button"><i class="icon-angle-up"></i></a>
    {{-- <div id="confirm-delete" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Confirm your action</h1>
      </div>

      <div class="modal-body">
        <div role="alert" class="alert alert-warning alert-icon alert-icon-border alert-dismissible">
          <div class="icon"><span class="mdi mdi-close-circle-o"></span></div>
          <div class="message">
            <strong>Warning!</strong> This action can not be undone.
          </div>
        </div>
        <h3 class="text-semibold">Are you sure ...</h3>
        <p>You are about to delete the record?</strong></p>

        <hr>

        <h3 class="text-semibold">What is this message?</h3>
        <p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command or upon submission of your task of the day.</p>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
        <a href="#" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Removing record ..." class="btn btn-primary btn-raised btn-loading" id="btn-confirm-delete">Delete Record</a>
      </div>
    </div>
  </div>
</div> --}}
<div class="modal fade" id="confirm-delete" style="z-index: 1000000000" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xs" role="document">
    <div class="modal-content">   
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="row mt-5">
          <div class="col-lg-12 text-center">
            <i class="far fa-times-circle fa-7x text-danger mb-3"></i>
            <p class="oswold-400">Are you sure you want to remove this image ?</p>
          </div>
          <div class="col-lg-12 text-center mb-3">
            <a href="#" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Removing record ..." class="btn btn-danger w-25 btn-loading" id="btn-confirm-delete">Yes</a>
{{--             <a  href="#" id="confirm_delete" data-url="" type="button" class="btn btn-danger w-25">Yes</a>
 --}}            <button type="button" class="btn btn-secondary w-25" data-dismiss="modal">No</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('page-scripts')
<script type="text/javascript">
  $(function(){
    $(".action-delete").on("click",function(){
      var btn = $(this);
      $("#btn-confirm-delete").attr({"href" : btn.data('url')});
    });

  

  });
</script>
@stop