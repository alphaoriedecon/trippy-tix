@extends('frontend._layouts.main') 
@section('content')
 <div class="page-wrapper">
        @include('frontend._components.header') 
      		<main class="main mt-1">
            <nav aria-label="breadcrumb" class="breadcrumb-nav">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <ol class="breadcrumb">

                                <li class="breadcrumb-item"><a href="index-2.html"><i class="icon-home"></i></a></li>
                                <li class="breadcrumb-item active" aria-current="page">My Products</li>

                            </ol>
                        </div>
                        <div class="col-md-4 text-md-right">
                             <a class="oswold-300 mr-4 text-white p-2 pl-3 pr-3 no-decoration text-uppercase rounded bg-red" href="{{ route('frontend.my_account.my_products.create') }}">Add Product
                                        </a>
                        </div>
                    </div>
                </div><!-- End .container -->
            </nav>

            <div class="container">
                <div class="row">

                    <div class="col-lg-12 order-lg-last dashboard-content">
                        <div class="row">
                            <div class="col-md-4 mb-2">
                                      {{--   <a href="{{ route('frontend.my_account.my_products.create') }}"><i title="Create Product" style="font-size: 20px;" class="fas fa-plus-circle"></i>
                                        </a> --}}
                                       
                            </div>
                            <div class="col-md-8 text-md-right">
                                <div class="toolbox-item">
                                    <div class="toolbox-item toolbox-show">
                                        <label class="oswold-300">Showing 1–9 of 60 results</label>
                                    </div><!-- End .toolbox-item -->

                                    <div class="layout-modes">
                                        <a href="#" class="layout-btn btn-grid active" title="Grid">
                                            <i class="icon-mode-grid"></i>
                                        </a>

                                    </div><!-- End .layout-modes -->
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            @forelse($products as $index )
                        	<div class="col-md-6">
                        		<div class="row">
		                            <div class="col-md-4">
                                        <div class="product-items1 owl-carousel owl-theme">
                                            @foreach($index->images as $value)
                                            <div class="item">
                                                <img style="height: 160px;width: auto" src="{{ "{$value->directory}/resized/{$value->filename}" }}" alt="product">
                                            </div>    
                                            @endforeach               
                                        </div>
		                                
		                            </div>
		                            <div class="col-md-8">
                                        <a class="no-decoration" href="{{route('frontend.my_account.my_products.edit',[$index->id])}}">
		                              	<span class="oswold-300 text-uppercase">Title: 
		                              		<span class="oswold-400">{{ $index->title}}</span></span>
		                              		<br>
                                        <span class="oswold-300 text-uppercase">Main Category: 
                                            <span class="oswold-400">{{ $index->main_category}}</span></span>
                                            <br>
                                        <span class="oswold-300 text-uppercase">Sub Category: 
                                            <span class="oswold-400">{{ $index->sub_category}}</span></span>
                                            <br>
                                        <span class="oswold-300 text-uppercase">Category: 
                                            <span class="oswold-400">{{ $index->category}}</span></span>
                                            <br>
		                              	{{-- <span class="oswold-300 text-uppercase">Location: 
		                              		<span class="oswold-400">Makati City</span></span> 
		                              		<br> --}}
		                              	<span class="oswold-300 text-uppercase">Type: 
		                              		<span class="oswold-400">{{ $index->type}}</span></span>
		                              		<br>
		                              	<span class="oswold-300 text-uppercase">Price: 
		                              		<span class="oswold-400">PHP {{ number_format($index->price,2)}}</span></span>
		                              		
		                              		<br>
		                              	<span class="oswold-300 text-uppercase">Date Created: 
		                              		<span class="oswold-400">{{ $index->created_at->format('M, d Y') }}</span></span>
                                            <br>
                                       {{--  <span class="oswold-300 text-uppercase">Buyer Chat: 
                                            <span class="oswold-400">2</span></span> --}}
                                        </a>
		                            </div>
		                        </div>
		                    </div>
                            @empty
                            <p>No Product</p>
                            @endforelse
                        </div>
                        <div>
                            <div class="cart-table-container mt-3">
                                
                                <nav class="toolbox toolbox-pagination">
                                    <div class="toolbox-item toolbox-show">
                                        <label class="oswold-300">Showing 1–9 of 60 results</label>
                                    </div><!-- End .toolbox-item -->

                                    <ul class="pagination oswold-300">
                                        <li class="page-item disabled">
                                            <a class="page-link page-link-btn" href="#"><i class="icon-angle-left"></i></a>
                                        </li>
                                        <li class="page-item active">
                                            <a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
                                        </li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                                        <li class="page-item"><span>...</span></li>
                                        <li class="page-item"><a class="page-link" href="#">15</a></li>
                                        <li class="page-item">
                                            <a class="page-link page-link-btn" href="#"><i class="icon-angle-right"></i></a>
                                        </li>
                                    </ul>
                                </nav>
                            </div><!-- End .cart-table-container -->

                        
                    </div><!-- End .col-lg-8 -->
                        
                    </div><!-- End .col-lg-9 -->

{{--                      @include('frontend._components.sidebar')
 --}}                </div><!-- End .row -->
            </div><!-- End .container -->

            <div class="mb-5"></div><!-- margin -->
        </main><!-- End .main -->
        @include('frontend._components.footer') 
       
    </div><!-- End .page-wrapper -->
    @include('frontend._components.mobile_header') 
   


    <a id="scroll-top" href="#top" role="button"><i class="icon-angle-up"></i></a>

@stop

@section('page-styles')
<style type="text/css">
    .owl-theme .owl-dots .owl-dot span {
        display: none !important;
    }
</style>
@stop