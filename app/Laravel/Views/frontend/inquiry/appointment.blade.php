@extends('frontend._layouts.main') 
@section('content')
 
 
  
     
<div class="loading blue-2">
    <div class="loading-center">
        <div class="loading-center-absolute">
            <div class="object object_four"></div>
            <div class="object object_three"></div>
            <div class="object object_two"></div>
            <div class="object object_one"></div>
        </div>
    </div>
</div>
  
  @include('frontend._components.header')



<div class="main-wraper  padd-90" id="contact" style="background-color: #f5f6fa">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="second-title">
                    <h4 class="subtitle color-blue-2 underline">Inquiry</h4>
                    <h2>Set Appointment</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <form class="contact-form" method="POST" action="" enctype="multipart/form-data">
               {!!csrf_field()!!}
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="input-style-1 type-2 color-2">
                               <label class="label-p">Name <span class="red">*</span></label>
                                <input type="text" name="name" value="{{old('name')}}" required="">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="input-style-1 type-2 color-2">
                               <label class="label-p">Email Address <span class="red">*</span></label>
                                <input type="text" name="email" value="{{old('email')}}" required="">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="input-style-1 type-2 color-2">
                               <label class="label-p">Contact Number <span class="red">*</span></label>
                                <input type="number" name="contact_number" value="{{old('contact_number')}}" required="">
                            </div>
                        </div>  
                      
                   

                           <div class="col-xs-12 col-sm-6">
                            <div class="input-style-1 type-2 color-2">
                              <label class="label-p">Appointment Date (M-F) <span class="red">*</span></label>
                                <input type="date" name="appointment_date" required="" value="{{old('appointment_date')}}" placeholder="">
                            </div>
                        </div>  
                       
                         <div class="col-md-12"> 
                          <label class="label-p">Agent <span class="red">*</span></label>
                       <select required="" name="agent" class="form-control" style="padding: 0px 20px;height: 60px;border-radius: 20px; background-color: transparent;">
                          <option disabled="" selected="">-- Select Agent --</option>
                          @foreach($agent_list as $agent)
                            <option value="{{$agent->id}}">{{$agent->first_name}} {{$agent->last_name}}</option>
                          @endforeach
                        </select>

                      </div>
                      
                        
                        <div class="col-xs-12" style="margin-top: 40px">
                            <textarea class="area-style-1 color-1" name="message" value="{{old('message')}}" placeholder="Enter your message"></textarea>
                            <button type="submit" class="c-button bg-blue-2 hv-blue-2-o"><span>submit</span></button>
                        </div>
                    </div>                  
                </form>
            </div>
            
        </div>
    </div>
</div>

<!-- <div class="map-block">
    <div id="map-canvas" class="style-2" data-lat="33.664467" data-lng="-117.601586" data-zoom="10" data-style="2"></div>
    <div class="addresses-block">
        <a data-lat="33.664467" data-lng="-117.601586" data-string="Santa Monica Hotel"></a>
    </div>
</div>
 -->              

<!-- FOOTER -->
  @include('frontend._components.footer')

@stop

