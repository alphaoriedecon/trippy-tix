@extends('frontend._layouts.main') 
@section('content')
 
 
  
     
<div class="loading blue-2">
    <div class="loading-center">
        <div class="loading-center-absolute">
            <div class="object object_four"></div>
            <div class="object object_three"></div>
            <div class="object object_two"></div>
            <div class="object object_one"></div>
        </div>
    </div>
</div>
  
  @include('frontend._components.header')



<div class="main-wraper  padd-90" id="contact" style="background-color: #f5f6fa">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="second-title">
                    <h4 class="subtitle color-blue-2 underline">Inquiry</h4>
                    <h2>Airline Ticket</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12">
              <form class="contact-form" method="POST" action="" enctype="multipart/form-data">
               {!!csrf_field()!!}
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 {{$errors->first('name') ? 'has-error' : NULL}}">
                            <div class="input-style-1 type-2 color-2">
                               <label class="label-p">Full Name <span class="red">*</span></label>
                                <input type="text" name="name" required="" value="{{old('name')}}">
                                @if($errors->first('name'))
                                  <span class="help-block">{{$errors->first('name')}}</span>
                                  @endif
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 {{$errors->first('email') ? 'has-error' : NULL}}">
                            <div class="input-style-1 type-2 color-2">
                               <label class="label-p">Email Address <span class="red">*</span></label>
                                <input type="text" name="email" required="" value="{{old('email')}}">
                                @if($errors->first('email'))
                                  <span class="help-block">{{$errors->first('email')}}</span>
                                  @endif
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 {{$errors->first('contact_number') ? 'has-error' : NULL}}">
                            <div class="input-style-1 type-2 color-2">
                               <label class="label-p">Contact Number <span class="red">*</span></label>
                                <input type="number" name="contact_number" required="" value="{{old('contact_number')}}">
                                @if($errors->first('contact_number'))
                                  <span class="help-block">{{$errors->first('contact_number')}}</span>
                                  @endif
                            </div>
                        </div>  
                         <div class="col-xs-12 col-sm-6 {{$errors->first('nationality') ? 'has-error' : NULL}}">
                            <div class="input-style-1 type-2 color-2">
                               <label class="label-p">Nationality <span class="red">*</span></label>
                                <input type="text" name="nationality" required="" value="{{old('nationality')}}">
                                @if($errors->first('nationality'))
                                  <span class="help-block">{{$errors->first('nationality')}}</span>
                                  @endif
                            </div>
                        </div>  
                        <div class="col-xs-12 col-sm-6 {{$errors->first('passenger') ? 'has-error' : NULL}}">
                            <div class="input-style-1 type-2 color-2">
                               <label class="label-p">Total Number of Passenger <span class="red">*</span></label>
                                <input type="number" name="passenger" required="" value="{{old('passenger')}}">
                                @if($errors->first('passenger'))
                                  <span class="help-block">{{$errors->first('passenger')}}</span>
                                  @endif
                            </div>
                        </div>  
                         <div class="col-xs-12 col-sm-6 {{$errors->first('infant') ? 'has-error' : NULL}}">
                            <div class="input-style-1 type-2 color-2">
                               <label class="label-p">Number of Infant (0-1 &11mos.) <span class="red">*</span></label>
                                <input type="number" name="infant" required="" value="{{old('infant')}}">
                                @if($errors->first('infant'))
                                  <span class="help-block">{{$errors->first('infant')}}</span>
                                  @endif
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 {{$errors->first('departure_location') ? 'has-error' : NULL}}">
                            <div class="input-style-1 type-2 color-2">
                               <label class="label-p">Airport Origin <span class="red">*</span></label>
                                <input type="text" name="departure_location" required="" value="{{old('departure_location')}}">
                                @if($errors->first('departure_location'))
                                  <span class="help-block">{{$errors->first('departure_location')}}</span>
                                  @endif
                            </div>
                        </div>  
                         <div class="col-xs-12 col-sm-6 {{$errors->first('arrival_location') ? 'has-error' : NULL}}">
                            <div class="input-style-1 type-2 color-2">
                               <label class="label-p">Destination <span class="red">*</span></label>
                                <input type="text" name="arrival_location" value="{{old('arrival_location')}}" required="">
                                @if($errors->first('arrival_location'))
                                  <span class="help-block">{{$errors->first('arrival_location')}}</span>
                                  @endif
                            </div>
                        </div>  
                           <div class="col-xs-12 col-sm-6 {{$errors->first('departure_date') ? 'has-error' : NULL}}">
                            <div class="input-style-1 type-2 color-2">
                              <label class="label-p">Departure Date <span class="red">*</span></label>
                                <input type="date" name="departure_date" required="" placeholder="" value="{{old('departure_date')}}">
                                @if($errors->first('departure_date'))
                                  <span class="help-block">{{$errors->first('departure_date')}}</span>
                                  @endif
                            </div>
                        </div>  
                         
                         <div class="col-xs-12 col-sm-6 {{$errors->first('arrival_date') ? 'has-error' : NULL}}">
                            <div class="input-style-1 type-2 color-2">
                               <label class="label-p">Arrival Date <span class="red">*</span></label>
                                <input type="date" name="arrival_date" required="" value="{{old('arrival_date')}}" placeholder="">
                                @if($errors->first('arrival_date'))
                                  <span class="help-block">{{$errors->first('arrival_date')}}</span>
                                  @endif
                            </div>
                        </div> 
                        
                          <div class="col-md-12 {{$errors->first('agent') ? 'has-error' : NULL}}"> 
                          <label class="label-p">Agent <span class="red">*</span></label>
                        <select required="" name="agent" class="form-control" style="padding: 0px 20px;height: 60px;border-radius: 20px; background-color: transparent;">
                          <option disabled="" selected="">-- Select Agent --</option>
                          @foreach($agent_list as $agent)
                            <option value="{{$agent->id}}">{{$agent->first_name}} {{$agent->last_name}}</option>
                          @endforeach
                        </select>
                        @if($errors->first('agent'))
                        <span class="help-block">{{$errors->first('agent')}}</span>
                        @endif
                      </div>
                      
                        
                        <div class="col-xs-12" style="margin-top: 40px">
                            <textarea class="area-style-1 color-1" name="message"  value="{{old('message')}}" placeholder="Enter your message"></textarea>
                            <button type="submit" class="c-button bg-blue-2 hv-blue-2-o"><span>submit</span></button>
                        </div>
                    </div>                  
                </form>
            </div>
         
        </div>
    </div>
</div>

<!-- <div class="map-block">
    <div id="map-canvas" class="style-2" data-lat="33.664467" data-lng="-117.601586" data-zoom="10" data-style="2"></div>
    <div class="addresses-block">
        <a data-lat="33.664467" data-lng="-117.601586" data-string="Santa Monica Hotel"></a>
    </div>
</div>
 -->              

<!-- FOOTER -->
  @include('frontend._components.footer')

@stop

