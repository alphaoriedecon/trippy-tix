@extends('frontend._layouts.main') 
@section('content')

    <div class="page-loading">
        <div class="thecube">
            <div class="cube c1"></div>
            <div class="cube c2"></div>
            <div class="cube c4"></div>
            <div class="cube c3"></div>
        </div>
    </div><!--page-loading end-->

    <div class="wrapper">
            
        @include('frontend._components.header')
        <section class="pager-section">
            <div class="container">
                <div class="pager-info">
                    <ul class="breadcrumb">
                        <li><a href="#" title="">Home</a></li>
                        <li><span>Services</span></li>
                    </ul><!--breadcrumb end-->
                    <h2>Services</h2>
                    <span>Please review our services</span>
                </div>
                <div class="pger-imgs style2">
                    <div class="abt-imgz">
                        <img src="assets/images/resources/about3.jpg" alt="">
                    </div>
                </div><!--pger-imgs end-->
                <div class="clearfix"></div>
            </div>
        </section><!--pager-section end-->

        

        <section class="block" style="padding-top: 100px;padding-bottom: 0px !important">
            <div class="container">
                <div class="section-title style2 align-items-center">
                    <h3 class="sub-title mw-45">SMART GLASS CALIBRATIONS SERVICES
</h3>
                    <p class="mw-45" style="font-size: 17px">Smart Glass Calibration ensures.nothing but exemplary services to every customer. Our top-tier calibration procedures will ensure that your vehicle’s ADAS features work according to their designated functions. We guarantee this through the expertise and excellent performance of our dedicated team of licensed calibration technicians.<br><br>

There are two types of ADAS calibration. What you need to get will be based on the technologies  used by your manufacturer.
</p>
                    <div class="clearfix"></div>
                </div>
                <div class="">
                </div>
                <div class="row">
                    <div class="col-12">
                        <!-- <div class="svs-img">
                            <img src="assets/images/resources/svs-img1.jpg" alt="" class="w-100">
                        </div> -->
                        <div class="container">
                            <h3 style="padding: 0px 25px 10px 25px;font-weight: bold;font-size: 24px"><a href="#" title="">Dynamic Calibration
</a></h3>
                            <p style="font-size: 18px;
    padding: 0px 25px 25px 25px;">Also known as mobile calibration. Our licensed technicians will drive your vehicle based on a prescribed guideline and calibration parameters provided by your manufacturer. To ensure that the process will be error-free, our team will be using a specialized handheld device that is to be plugged directly into your car.<br><br>

With this procedure successfully carried out, you can guarantee that each ADAS feature gets accustomed to specific road features and driving conditions — allowing you to use your ADAS at its fullest potential!
</p>
                        <!--    <a href="#" title="" class="lnk-default2">View more <i class="la la-arrow-right"></i></a> -->
                        </div>
                    </div><!--svs-item end-->
                        <div class="col-12">
                        <!-- <div class="svs-img">
                            <img src="assets/images/resources/svs-img1.jpg" alt="" class="w-100">
                        </div> -->
                        <div class="container mt-5 ">
                            <h3 style="padding: 0px 25px 10px 25px;font-weight: bold;font-size: 24px"><a href="#" title="">Static Calibration

</a></h3>
                            <p style="font-size: 18px;
    padding: 0px 25px 25px 25px;">Also known as fixed calibration. This procedure is carried out in our specifically tailored calibration facility near your area. Our calibration technicians will use a set of sensitive and specialized equipment to secure a successful calibration. <br><br>

With our team’s expertise and top-grade equipment, we will be able to gather precise measurements — allowing your ADAS to re-calibrate accurately.  With that, you can use your vehicle’s driving assistance and safety features the way they are supposed to!

</p>
                        <!--    <a href="#" title="" class="lnk-default2">View more <i class="la la-arrow-right"></i></a> -->
                        </div>
                    </div><!--svs-item end-->

                </div><!--svs-section end-->
        </section>

        

        <section class="block">
            <div class="container">
                <div class="consult-text style2 mt-0">
                    <h3 class="sub-title">Schedule a Calibration With Us Today!
</h3><p style="    line-height: 25px;
    /* padding: 31px; */
    padding-bottom: 30px;
    padding-left: 30px;
    padding-right: 30px;
    font-size: 17px;">Whenever you are in need of exemplary calibration services, reach out to Smart Glass Calibrations! Contact us today to schedule an appointment, and have your vehicle’s ADAS systems ready to provide you the assistance and safety they are specifically designed for!
</p>
                    <form class="subsc-form">
                        <a href="contact.html" type="submit" class="lnk-default mt-2" style="margin-left: 30px !important">Schedule an Appointment <i class="la la-arrow-right"></i> <span></span></a>
                    </form>
                </div><!--consult-text end-->
                
            </div>
        </section>

         @include('frontend._components.footer')

    </div><!--wrapper end-->

@stop