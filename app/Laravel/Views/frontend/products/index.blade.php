@extends('frontend._layouts.main') 
@section('content')
 <div class="page-wrapper">
        @include('frontend._components.header') 
        <main class="main">
            <div class="banner banner-cat" style="background-image: url('frontend/images/banners/banner-top.jpg');">
                <div class="banner-content container">
                    <h2 class="banner-subtitle">check out over <span>200+</span></h2>
                    <h1 class="banner-title">
                        INCREDIBLE deals
                    </h1>
                    <a href="#" class="btn btn-dark">Shop Now</a>
                </div><!-- End .banner-content -->
            </div><!-- End .banner -->

            <nav aria-label="breadcrumb" class="breadcrumb-nav">
                <div class="container">
                    <ol class="breadcrumb mt-0 oswold-300">
                        <li class="breadcrumb-item"><a href="{{route('frontend.home.index')}}"><i class="icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="category.html">Items</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Default</li>
                    </ol>
                </div><!-- End .container -->
            </nav>

            <div class="container">
                <nav class="toolbox horizontal-filter">
                    <div class="toolbox-left">
                        <div class="filter-toggle">
                            <span class="oswold-300 text-uppercase">Filters:</span>
                            <a href=#>&nbsp;</a>
                        </div>
                    </div><!-- End .toolbox-left -->

                    <div class="toolbox-item toolbox-sort">
                   

                  
                    </div><!-- End .toolbox-item -->

                    <div class="toolbox-item">
                        <div class="toolbox-item toolbox-show">
                                <label>Showing 1–9 of 60 results</label>
                            </div><!-- End .toolbox-item -->

                        <div class="layout-modes">
                            <a href="#" class="layout-btn btn-grid active" title="Grid">
                                <i class="icon-mode-grid"></i>
                            </a>
                          
                        </div><!-- End .layout-modes -->
                    </div>
                </nav>

                <div class="row products-body">
                    <div class="col-lg-9 main-content">
                        <div class="row row-sm">


                            {{-- Product List --}}
                            @forelse ($products as $product)
                           
                                {{-- expr --}}
                                @forelse ($product->product_key as $prod)
                                     <div class="col-6 col-md-4 text-center">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="d-flex flex-row">
                                                <img src="{{ $product->directory }}/{{ $product->filename }}" alt="product" class="avatar-items">
                                                <div class="d-flex flex-column text-justify">
                                                <span class="ml-3 text-uppercase f-12-size oswold-400">Dhen mark</span>
                                                <span class="ml-3 text-uppercase f-10-size oswold-300">18 minutes ago</span>
                                               
                                            </div>
                                            </div>
                                                <figure>
                                                    <a href="{{route('frontend.products.show')}}">
                                                        <img src="{{ asset('frontend/images/house.jfif') }}" alt="product">
                                                    </a>
                                                </figure>
                                                <div class="product-details start-flex">               
                                                    <h2 class="product-title font-semibold text-uppercase text-limit--title">
                                                        <a class="text-size--14 no-decoration oswold-400" href="{{route('frontend.products.show')}}">{{ $product->description }}</a>
                                                    </h2>
                                                    <div class="price-box">
                                                        <span class="product-price text-blue mr-1 text-size--18 oswold-400">PHP {{$product->price}}</span>
                {{--                                          <span class="product-price text-line-through text-gray text-size--18">$19.00</span>
                 --}}                                    </div><!-- End .price-box -->
                                                   {{--  <div class="product-action">         
                                                        <button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-bag"></i>ADD TO CART</button>
                                                      
                                                    </div> --}}
                                                </div><!-- End .product-details -->
                                            </div>
                                        </div>
                                    </div>
                                   
                                    @empty
                                    <div class="col-6 col-md-4 text-center card">
                                        <div class="card-body">
                                            <div class="d-flex flex-row">
                                            <img src="{{ asset('frontend/images/avatar.png') }}" alt="product" class="avatar-items">
                                            <div class="d-flex flex-column text-justify">
                                            <span class="ml-3 text-uppercase f-12-size oswold-400">Dhen mark</span>
                                            <span class="ml-3 text-uppercase f-10-size oswold-300">18 minutes ago</span>
                                           
                                        </div>
                                        </div>
                                            <figure>
                                                <a href="{{route('frontend.products.show')}}">
                                                    <img src="{{ asset('frontend/images/house.jfif') }}" alt="product">
                                                </a>
                                            </figure>
                                            <div class="product-details start-flex">               
                                                <h2 class="product-title font-semibold text-uppercase text-limit--title">
                                                    <a class="text-size--14 no-decoration oswold-400" href="{{route('frontend.products.show')}}">Lorem ipsum dummy text aasdsa sadsadsa</a>
                                                </h2>
                                                <div class="price-box" style="margin-bottom:2px !important;">
                                                    <span class="product-price text-blue mr-1 text-size--18 oswold-400">PHP 2,229.00</span>
                                                      {{--<span class="product-price text-line-through text-gray text-size--18">$19.00</span> --}}     
                                                    </div><!-- End .price-box -->
                                                 <span class="text-uppercase f-15-size oswold-400">NEW</span>
                                               {{--  <div class="product-action">         
                                                    <button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-bag"></i>ADD TO CART</button>
                                                  
                                                </div> --}}
                                            </div><!-- End .product-details -->
                                        </div>
                                    </div>
                                @endforelse
                           
                           
                            @empty
                             <div class="col-6 col-md-4 text-left">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="d-flex flex-row">
                                        <img src="{{ asset('frontend/images/avatar.png') }}" alt="product" class="avatar-items">
                                        <div class="d-flex flex-column text-justify">
                                        <span class="ml-3 text-uppercase f-12-size oswold-400">Dhen mark</span>
                                        <span class="ml-3 text-uppercase f-10-size oswold-300">18 minutes ago</span>
                                       
                                    </div>
                                    </div>
                                        <figure>
                                            <a href="{{route('frontend.products.show')}}">
                                                <img src="{{ asset('frontend/images/house.jfif') }}" alt="product">
                                            </a>
                                        </figure>
                                        <div class="product-details start-flex">               
                                        <h2 class="product-title font-semibold text-uppercase text-limit--title">
                                            <a class="text-size--14 no-decoration oswold-400" href="{{route('frontend.products.show')}}">Lorem ipsum dummy text aasdsa sadsadsa</a>
                                        </h2>
                                        <div class="price-box text-left" style="margin-bottom:2px !important;">
                                            <span class="product-price text-blue mr-1 text-size--18 oswold-400">PHP 2,229.00</span>
                                              {{--<span class="product-price text-line-through text-gray text-size--18">$19.00</span> --}}     
                                            </div><!-- End .price-box -->
                                         <span class="text-uppercase text-left f-15-size oswold-400">NEW</span>
                                       {{--  <div class="product-action">         
                                            <button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-bag"></i>ADD TO CART</button>
                                          
                                        </div> --}}
                                    </div><!-- End .product-details -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-md-4 text-left">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="d-flex flex-row">
                                        <img src="{{ asset('frontend/images/avatar.png') }}" alt="product" class="avatar-items">
                                        <div class="d-flex flex-column text-justify">
                                        <span class="ml-3 text-uppercase f-12-size oswold-400">Dhen mark</span>
                                        <span class="ml-3 text-uppercase f-10-size oswold-300">18 minutes ago</span>
                                       
                                    </div>
                                    </div>
                                        <figure>
                                            <a href="{{route('frontend.products.show')}}">
                                                <img src="{{ asset('frontend/images/house.jfif') }}" alt="product">
                                            </a>
                                        </figure>
                                        <div class="product-details start-flex">               
                                        <h2 class="product-title font-semibold text-uppercase text-limit--title">
                                            <a class="text-size--14 no-decoration oswold-400" href="{{route('frontend.products.show')}}">Lorem ipsum dummy text aasdsa sadsadsa</a>
                                        </h2>
                                        <div class="price-box text-left" style="margin-bottom:2px !important;">
                                            <span class="product-price text-blue mr-1 text-size--18 oswold-400">PHP 2,229.00</span>
                                              {{--<span class="product-price text-line-through text-gray text-size--18">$19.00</span> --}}     
                                            </div><!-- End .price-box -->
                                         <span class="text-uppercase text-left f-15-size oswold-400">NEW</span>
                                       {{--  <div class="product-action">         
                                            <button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-bag"></i>ADD TO CART</button>
                                          
                                        </div> --}}
                                    </div><!-- End .product-details -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-md-4 text-left">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="d-flex flex-row">
                                        <img src="{{ asset('frontend/images/avatar.png') }}" alt="product" class="avatar-items">
                                        <div class="d-flex flex-column text-justify">
                                        <span class="ml-3 text-uppercase f-12-size oswold-400">Dhen mark</span>
                                        <span class="ml-3 text-uppercase f-10-size oswold-300">18 minutes ago</span>
                                       
                                    </div>
                                    </div>
                                        <figure>
                                            <a href="{{route('frontend.products.show')}}">
                                                <img src="{{ asset('frontend/images/house.jfif') }}" alt="product">
                                            </a>
                                        </figure>
                                        <div class="product-details start-flex">               
                                        <h2 class="product-title font-semibold text-uppercase text-limit--title">
                                            <a class="text-size--14 no-decoration oswold-400" href="{{route('frontend.products.show')}}">Lorem ipsum dummy text aasdsa sadsadsa</a>
                                        </h2>
                                        <div class="price-box text-left" style="margin-bottom:2px !important;">
                                            <span class="product-price text-blue mr-1 text-size--18 oswold-400">PHP 2,229.00</span>
                                              {{--<span class="product-price text-line-through text-gray text-size--18">$19.00</span> --}}     
                                            </div><!-- End .price-box -->
                                         <span class="text-uppercase text-left f-15-size oswold-400">NEW</span>
                                       {{--  <div class="product-action">         
                                            <button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-bag"></i>ADD TO CART</button>
                                          
                                        </div> --}}
                                    </div><!-- End .product-details -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-md-4 text-left">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="d-flex flex-row">
                                        <img src="{{ asset('frontend/images/avatar.png') }}" alt="product" class="avatar-items">
                                        <div class="d-flex flex-column text-justify">
                                        <span class="ml-3 text-uppercase f-12-size oswold-400">Dhen mark</span>
                                        <span class="ml-3 text-uppercase f-10-size oswold-300">18 minutes ago</span>
                                       
                                    </div>
                                    </div>
                                        <figure>
                                            <a href="{{route('frontend.products.show')}}">
                                                <img src="{{ asset('frontend/images/house.jfif') }}" alt="product">
                                            </a>
                                        </figure>
                                        <div class="product-details start-flex">               
                                        <h2 class="product-title font-semibold text-uppercase text-limit--title">
                                            <a class="text-size--14 no-decoration oswold-400" href="{{route('frontend.products.show')}}">Lorem ipsum dummy text aasdsa sadsadsa</a>
                                        </h2>
                                        <div class="price-box text-left" style="margin-bottom:2px !important;">
                                            <span class="product-price text-blue mr-1 text-size--18 oswold-400">PHP 2,229.00</span>
                                              {{--<span class="product-price text-line-through text-gray text-size--18">$19.00</span> --}}     
                                            </div><!-- End .price-box -->
                                         <span class="text-uppercase text-left f-15-size oswold-400">NEW</span>
                                       {{--  <div class="product-action">         
                                            <button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-bag"></i>ADD TO CART</button>
                                          
                                        </div> --}}
                                    </div><!-- End .product-details -->
                                    </div>
                                </div>
                            </div>


                            @endforelse

                            {{-- End Of ProductList --}}
                            
                        </div>

                        <nav class="toolbox toolbox-pagination">
                            <div class="toolbox-item toolbox-show">
                                <label>Showing 1–9 of 60 results</label>
                            </div><!-- End .toolbox-item -->

                           {{--  <ul class="pagination">
                               
                                <li class="page-item active">
                                    <a class="page-link" href="#"><span class="sr-only">(current)</span></a>
                                </li>
                              
                               
                            </ul> --}}
                            {{ $products->links() }}
                        </nav>
                    </div><!-- End .col-lg-9 -->

                    <div class="sidebar-overlay"></div>
                    <aside class="sidebar-shop col-lg-3 order-lg-first mobile-sidebar">
                        <div class="sidebar-wrapper">
                            <div class="widget">
                                <h3 class="widget-title oswold-400">
                                    <a data-toggle="collapse" href="#widget-body-1" role="button" aria-expanded="true" aria-controls="widget-body-1">Cars & Property</a>
                                </h3>

                                <div class="collapse" id="widget-body-1">
                                    <div class="widget-body">
                                        <ul class="cat-list oswold-300">
                                            <li><a href="#">Lorem ipsum <span>18</span></a></li>
                                            <li><a href="#">Lorem ipsum <span>22</span></a></li>
                                            <li><a href="#">Lorem ipsum <span>05</span></a></li>
                                            <li><a href="#">Lorem ipsum <span>68</span></a></li>
                                            <li><a href="#">Lorem ipsum <span>03</span></a></li>
                                        </ul>
                                    </div><!-- End .widget-body -->
                                </div><!-- End .collapse -->
                            </div><!-- End .widget -->

                            <div class="widget">
                                <h3 class="widget-title oswold-400">
                                    <a data-toggle="collapse" href="#widget-body-2" role="button" aria-expanded="true" aria-controls="widget-body-2">Fashion</a>
                                </h3>

                                <div class="collapse" id="widget-body-2">
                                    <div class="widget-body">
                                        <ul class="cat-list oswold-300">
                                            <li><a href="#">Lorem ipsum <span>18</span></a></li>
                                            <li><a href="#">Lorem ipsum <span>22</span></a></li>
                                            <li><a href="#">Lorem ipsum <span>05</span></a></li>
                                            <li><a href="#">Lorem ipsum <span>68</span></a></li>
                                            <li><a href="#">Lorem ipsum <span>03</span></a></li>
                                        </ul>
                                    </div><!-- End .widget-body -->
                                </div><!-- End .collapse -->
                            </div><!-- End .widget -->

                           

                            
                    </aside><!-- End .col-lg-3 -->
                </div><!-- End .row -->
            </div><!-- End .container -->

            <div class="mb-5"></div><!-- margin -->
        </main><!-- End .main -->
        @include('frontend._components.footer') 
       
    </div><!-- End .page-wrapper -->
    @include('frontend._components.mobile_header') 
   
    <a id="scroll-top" href="#top" role="button"><i class="icon-angle-up"></i></a>
@stop

@section('page-scripts')

<script type="text/javascript">
    $(document).ready(function(){
        

    })
     
</script>
@stop