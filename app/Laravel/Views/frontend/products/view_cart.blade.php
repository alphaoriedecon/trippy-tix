@extends('frontend._layouts.main') 
@section('content')
 <div class="page-wrapper">
        @include('frontend._components.header') 
       <main class="main">
            <nav aria-label="breadcrumb" class="breadcrumb-nav">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('frontend.home.index')}}"><i class="icon-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Shopping Cart</li>
                    </ol>
                </div><!-- End .container -->
            </nav>

            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="cart-table-container">
                            <table class="table table-cart">
                                <thead>
                                    <tr>
                                        <th class="product-col">Product</th>
                                        <th class="price-col">Price</th>
                                        <th class="qty-col">Qty</th>
                                        <th>Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                   
                                   {{--  --}}
                                    @foreach ($items as $item) 
                                    @foreach ($item->details as $detail)

                                        {{-- expr --}}
                                   
                                         <tr class="product-row">
                                        <td class="product-col">
                                            <figure class="product-image-container">
                                                <a href="{{route('frontend.products.show')}}" class="product-image">
                                                    {{-- <img src="https://msofficeworks.com/wp-content/uploads/2017/01/microsoft-office-plus-2016-500-500.png" alt="product"> --}}
                                                     <img src="{{ $detail->product->products->directory}}/{{$detail->product->products->filename}}">
                                                </a>
                                            </figure>
                                            <h2 class="product-title">
                                                <a href="#">{{ $detail->product_name}}</a>
                                            </h2>
                                        </td>
                                        <td>{{ number_format($detail->item_price ,2)}}</td>
                                        <td>
                                            <input class="vertical-quantity form-control" type="text" value="{{$detail->qty}}">
                                        </td>
                                        <td>{{ number_format($detail->sub_total,2) }} </td>
                                    </tr>
                                     @endforeach
                                    @endforeach
                                   
                                    <tr class="product-action-row">
                                        <td colspan="4" class="clearfix">
                                            

                                            
                                            <div class="float-right">
                                                <a href="#" title="Edit product" class="btn-edit"><span class="sr-only">Edit</span><i class="icon-pencil"></i></a>
                                                <a href="#" title="Remove product" class="btn-remove"><span class="sr-only">Remove</span></a>
                                            </div><!-- End .float-right -->
                                        </td>
                                    </tr>
                                </tbody>

                                <tfoot>
                                    <tr>
                                        <td colspan="4" class="clearfix">
                                            <div class="float-left">
                                                <a href="{{route('frontend.products.index')}}" class="btn btn-outline-secondary">Continue Shopping</a>
                                            </div><!-- End .float-left -->

                                            <div class="float-right">
                                                <a href="#" class="btn btn-outline-secondary btn-clear-cart">Clear Shopping Cart</a>
                                                <a href="#" class="btn btn-outline-secondary btn-update-cart">Update Shopping Cart</a>
                                            </div><!-- End .float-right -->
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- End .cart-table-container -->

                        
                    </div><!-- End .col-lg-8 -->

                    <div class="col-lg-4">
                        <div class="cart-summary">
                            <h3>Summary</h3>
                            <table class="table table-totals">
                                <tbody>
                                    <tr>
                                        <td>Subtotal</td>
                             <td>{{count(auth()->user()->transaction) > 0 ? number_format(auth()->user()->transaction->total_price,2) : "" }}</td>
                                    </tr>

                                    <tr>
                                        <td>Tax</td>
                                        <td>$0.00</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td>Order Total</td>
                                      <td>{{count(auth()->user()->transaction) > 0 ? number_format(auth()->user()->transaction->total_price,2) : "" }}</td>

                                    </tr>
                                </tfoot>
                            </table>

                            <div class="checkout-methods">
                                <a href="{{route('frontend.products.checkout')}}" class="btn btn-block btn-sm btn-primary">Go to Checkout</a>
                              
                            </div><!-- End .checkout-methods -->
                        </div><!-- End .cart-summary -->
                    </div><!-- End .col-lg-4 -->
                </div><!-- End .row -->
            </div><!-- End .container -->

            <div class="mb-6"></div><!-- margin -->
        </main><!-- End .main -->
        @include('frontend._components.footer') 
       
    </div><!-- End .page-wrapper -->
    @include('frontend._components.mobile_header') 
   


    <a id="scroll-top" href="#top" role="button"><i class="icon-angle-up"></i></a>

@stop