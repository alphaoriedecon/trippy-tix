 <!-- Modal -->
<div class="modal fade" id="addressModal" tabindex="-1" role="dialog" aria-labelledby="addressModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="#">
                <div class="modal-header">
                    <h3 class="modal-title" id="addressModalLabel">Shipping Address</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div><!-- End .modal-header -->

                <div class="modal-body">
                        <div class="form-group required-field">
                            <label>First Name </label>
                            <input type="text" class="form-control form-control-sm" required>
                        </div><!-- End .form-group -->

                        <div class="form-group required-field">
                            <label>Last Name </label>
                            <input type="text" class="form-control form-control-sm" required>
                        </div><!-- End .form-group -->

                        <div class="form-group">
                            <label>Company </label>
                            <input type="text" class="form-control form-control-sm">
                        </div><!-- End .form-group -->

                        <div class="form-group required-field">
                            <label>Street Address </label>
                            <input type="text" class="form-control form-control-sm" required>
                            <input type="text" class="form-control form-control-sm" required>
                        </div><!-- End .form-group -->

                        <div class="form-group required-field">
                            <label>City  </label>
                            <input type="text" class="form-control form-control-sm" required>
                        </div><!-- End .form-group -->

                        <div class="form-group">
                            <label>State/Province</label>
                            <div class="select-custom">
                                <select class="form-control form-control-sm">
                                    <option value="CA">California</option>
                                    <option value="TX">Texas</option>
                                </select>
                            </div><!-- End .select-custom -->
                        </div><!-- End .form-group -->

                        <div class="form-group required-field">
                            <label>Zip/Postal Code </label>
                            <input type="text" class="form-control form-control-sm" required>
                        </div><!-- End .form-group -->

                        <div class="form-group">
                            <label>Country</label>
                            <div class="select-custom">
                                <select class="form-control form-control-sm">
                                    <option value="USA">United States</option>
                                    <option value="Turkey">Turkey</option>
                                    <option value="China">China</option>
                                    <option value="Germany">Germany</option>
                                </select>
                            </div><!-- End .select-custom -->
                        </div><!-- End .form-group -->

                        <div class="form-group required-field">
                            <label>Phone Number </label>
                            <div class="form-control-tooltip">
                                <input type="tel" class="form-control form-control-sm" required>
                                <span class="input-tooltip" data-toggle="tooltip" title="For delivery questions." data-placement="right"><i class="icon-question-circle"></i></span>
                            </div><!-- End .form-control-tooltip -->
                        </div><!-- End .form-group -->
                        
                        <div class="form-group-custom-control">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="address-save">
                                <label class="custom-control-label" for="address-save">Save in Address book</label>
                            </div><!-- End .custom-checkbox -->
                        </div><!-- End .form-group -->
                </div><!-- End .modal-body -->

                <div class="modal-footer">
                    <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
                </div><!-- End .modal-footer -->
            </form>
        </div><!-- End .modal-content -->
    </div><!-- End .modal-dialog -->
</div><!-- End .modal -->