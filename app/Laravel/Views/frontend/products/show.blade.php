@extends('frontend._layouts.main') 
@section('content')
 <div class="page-wrapper">
        @include('frontend._components.header') 
        <main class="main">
            <nav aria-label="breadcrumb" class="breadcrumb-nav">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('frontend.home.index')}}"><i class="icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#">Product List</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Details</li>
                    </ol>
                </div><!-- End .container -->
            </nav>

            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="product-single-container product-single-default">
                            <div class="row">
                                <div class="col-md-12 product-single-gallery">

                                    <div class="product-slider-container product-item">
                                    	<div class="price-box mt-1">
                                            <!--<span class="old-price">$81.00</span>-->
                                            <span class="product-price oswold-400" style="font-size:29px">PHP 101.00</span>
                                        </div><!-- End .price-box -->
                                    	 <h2 style="margin-bottom:2px !important" class="product-title oswold-300">Microsoft Office</h2>
                                    	 <span class="oswold-300">Duis aute irure dolor in reprehenderit in voluptate</span>
                                      
                                        
                                        <div class="product-single-carousel mt-2 owl-carousel owl-theme">
                                            <div class="product-item">
                                                <img class="product-single-image" src="{{ asset('frontend/images/house.jfif') }}" />
                                            </div>
                                            <div class="product-item">
                                                <img class="product-single-image" src="{{ asset('frontend/images/house.jfif') }}" />
                                            </div>
                                            <div class="product-item">
                                                <img class="product-single-image" src="{{ asset('frontend/images/house.jfif') }}" />
                                            </div>
                                            <div class="product-item">
                                                <img class="product-single-image" src="{{ asset('frontend/images/house.jfif') }}" />
                                            </div>
                                        </div>
                                        <!-- End .product-single-carousel -->
                                        
                                    </div>
                                    <div class="prod-thumbnail row owl-dots" id='carousel-custom-dots'>
                                        <div class="col-3 owl-dot">
                                            <img src="{{ asset('frontend/images/house.jfif') }}"/>
                                        </div>
                                        <div class="col-3 owl-dot">
                                            <img src="{{ asset('frontend/images/house.jfif') }}"/>
                                        </div>
                                        <div class="col-3 owl-dot">
                                            <img src="{{ asset('frontend/images/house.jfif') }}"/>
                                        </div>
                                        <div class="col-3 owl-dot">
                                            <img src="{{ asset('frontend/images/house.jfif') }}"/>
                                        </div>
                                    </div>
                                </div><!-- End .col-lg-7 -->

                              
                            </div><!-- End .row -->
                        </div><!-- End .product-single-container -->

                        <div class="product-single-tabs">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="product-tab-desc" data-toggle="tab" href="#product-desc-content" role="tab" aria-controls="product-desc-content" aria-selected="true">Description</a>
                                </li>
                                
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="product-desc-content" role="tabpanel" aria-labelledby="product-tab-desc">
                                    <div class="product-desc-content oswold-300">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat.</p>
                                        <ul>
                                            <li><i class="icon-ok"></i>Any Product types that You want - Simple, Configurable</li>
                                            <li><i class="icon-ok"></i>Downloadable/Digital Products, Virtual Products</li>
                                            <li><i class="icon-ok"></i>Inventory Management with Backordered items</li>
                                        </ul>
                                        <p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, <br>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>

                                        <div class="row">
                                        	<div class="col-md-6">
                                        		<p>Post ID: <span class="oswold-400">312321312312</span></p>
                                        	</div>
                                        	<div class="col-md-6 text-md-right">
                                        		<p>Posted By: <span class="oswold-400">admin / 02-28-2020</span> </p>
                                        	</div>
                                        	<div class="col-md-12">
                                        		<button class="btn bg-red rounded text-white"><i class="mr-2 fas fa-exclamation-circle"></i>Report</button>
                                        	</div>
                                        </div>
                                    </div><!-- End .product-desc-content -->
                                </div><!-- End .tab-pane -->

                                

                              
                            </div><!-- End .tab-content -->
                        </div><!-- End .product-single-tabs -->
                    </div><!-- End .col-lg-9 -->
                    <div class="col-lg-6">
                    	<div class="row">
                    		<div class="col-md-12 d-flex justify-content-center">
                    			<img src="{{ asset('frontend/images/avatar.png') }}" alt="product" class="avatar-items-120">
                    		</div>
                    		<div class="col-md-12 text-center">
                    			<p class="oswold-400">Member Since December 2019</p>
                    			 <i class="text-green fas fa-star f-20-size mr-2"></i>
                    			 <i class="text-green fas fa-star f-20-size mr-2"></i>
                    			 <i class="text-green fas fa-star f-20-size mr-2"></i>
                    			 <i class="text-green fas fa-star f-20-size mr-2"></i>
                    			 <i class="text-green mr-2 far fa-star f-20-size"></i>
                    			 <p class="oswold-400">122 Seller Profile</p>
                    			</div>
                    			<div class="col-md-12 text-center">
                                    @if (Auth::check())
                                        <button class="btn bg-white" style="border:1px solid #333">Chat</button>
                                        <button class="btn bg-red text-white">Make a Offer</button>
                                        @else
                                        <a href="{{ route('frontend.auth.login') }}" class="btn bg-white" style="border:1px solid #333">Chat</a>
                                        <a href="{{ route('frontend.auth.login') }}" class="btn bg-red text-white">Make a Offer</a>
                                        @endif
                    			</div>
                    			<div class="col-md-12 text-left mt-3">
                    				<p class="oswold-400 ml-md-5 pl-md-4">Contact Seller</p>
                    				<div class="d-flex justify-content-center">
                    					<input type="text" name="fullname" class="form-control" placeholder="Name (Optional)">
                    				</div>
                    				<div class="d-flex justify-content-center">
                    					<input type="email" class="form-control" placeholder="Email (Optional)">
                    				</div>
                    				<div class="d-flex justify-content-center">
                    					<input type="number" class="form-control" placeholder="Phone Number">
                    				</div>
                    				<div class="d-flex ml-md-5 pl-md-4 mr-md-5 pr-md-4 justify-content-center">
                    					<textarea type="text" class="form-control" placeholder="Message"></textarea>
                    				</div>
                    				<div class="d-flex ml-md-5 pl-md-4 mr-md-5 pr-md-4 justify-content-end">
                                        @if (Auth::check())
                    					<button class="btn bg-green text-white">Send Message</button>
                                        @else
                                        <a href="{{ route('frontend.auth.login') }}" class="btn bg-green text-white">Send Message</a>
                                        @endif
                    				</div>
                    			</div>
                    		
                    	</div>
                    	
                    </div>
                </div>

                    

            <div class=" carousel-section">
                <div class="container">
                    <h2 class="oswold-400 title mb-2">Related Items</h2>

                    <div class="featured-products owl-carousel owl-theme">
                        <div class="product-default">
                           <div class="text-center card">
	                            <div class="card-body">
	                                <div class="d-flex flex-row">
	                                <img src="{{ asset('frontend/images/avatar.png') }}" alt="product" class="avatar-items">
	                                <div class="d-flex flex-column text-justify">
	                                <span class="ml-3 text-uppercase f-12-size oswold-400">Dhen mark</span>
	                                <span class="ml-3 text-uppercase f-10-size oswold-300">18 minutes ago</span>
	                               
	                            </div>
	                            </div>
	                                <figure>
	                                    <a href="{{route('frontend.products.show')}}">
	                                        <img src="{{ asset('frontend/images/house.jfif') }}" alt="product">
	                                    </a>
	                                </figure>
	                               <div class="product-details start-flex">               
	                                    <h2 class="product-title font-semibold text-uppercase text-limit--title">
	                                        <a class="text-size--14 no-decoration oswold-400" href="{{route('frontend.products.show')}}">Lorem ipsum dummy text aasdsa sadsadsa</a>
	                                    </h2>
	                                    <div class="price-box" style="margin-bottom:2px !important;">
	                                        <span class="product-price text-red mr-1 text-size--18 oswold-400">PHP 2,229.00</span>
	                                          {{--<span class="product-price text-line-through text-gray text-size--18">$19.00</span> --}}     
	                               			</div><!-- End .price-box -->
										 <span class="text-uppercase f-15-size oswold-400">NEW</span>
	                                   {{--  <div class="product-action">         
	                                        <button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-bag"></i>ADD TO CART</button>
	                                      
	                                    </div> --}}
	                                </div><!-- End .product-details -->
	                            </div>
	                        </div>
                        </div>
                        <div class="product-default">
                           <div class="text-center card">
	                            <div class="card-body">
	                                <div class="d-flex flex-row">
	                                <img src="{{ asset('frontend/images/avatar.png') }}" alt="product" class="avatar-items">
	                                <div class="d-flex flex-column text-justify">
	                                <span class="ml-3 text-uppercase f-12-size oswold-400">Dhen mark</span>
	                                <span class="ml-3 text-uppercase f-10-size oswold-300">18 minutes ago</span>
	                               
	                            </div>
	                            </div>
	                                <figure>
	                                    <a href="{{route('frontend.products.show')}}">
	                                        <img src="{{ asset('frontend/images/house.jfif') }}" alt="product">
	                                    </a>
	                                </figure>
	                                <div class="product-details start-flex">               
	                                    <h2 class="product-title font-semibold text-uppercase text-limit--title">
	                                        <a class="text-size--14 no-decoration oswold-400" href="{{route('frontend.products.show')}}">Lorem ipsum dummy text aasdsa sadsadsa</a>
	                                    </h2>
	                                    <div class="price-box" style="margin-bottom:2px !important;">
	                                        <span class="product-price text-red mr-1 text-size--18 oswold-400">PHP 2,229.00</span>
	                                          {{--<span class="product-price text-line-through text-gray text-size--18">$19.00</span> --}}     
	                               			</div><!-- End .price-box -->
										 <span class="text-uppercase f-15-size oswold-400">NEW</span>
	                                   {{--  <div class="product-action">         
	                                        <button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-bag"></i>ADD TO CART</button>
	                                      
	                                    </div> --}}
	                                </div><!-- End .product-details -->
	                            </div>
	                        </div>
                        </div>
                        <div class="product-default">
                           <div class="text-center card">
	                            <div class="card-body">
	                                <div class="d-flex flex-row">
	                                <img src="{{ asset('frontend/images/avatar.png') }}" alt="product" class="avatar-items">
	                                <div class="d-flex flex-column text-justify">
	                                <span class="ml-3 text-uppercase f-12-size oswold-400">Dhen mark</span>
	                                <span class="ml-3 text-uppercase f-10-size oswold-300">18 minutes ago</span>
	                               
	                            </div>
	                            </div>
	                                <figure>
	                                    <a href="{{route('frontend.products.show')}}">
	                                        <img src="{{ asset('frontend/images/house.jfif') }}" alt="product">
	                                    </a>
	                                </figure>
	                               <div class="product-details start-flex">               
	                                    <h2 class="product-title font-semibold text-uppercase text-limit--title">
	                                        <a class="text-size--14 no-decoration oswold-400" href="{{route('frontend.products.show')}}">Lorem ipsum dummy text aasdsa sadsadsa</a>
	                                    </h2>
	                                    <div class="price-box" style="margin-bottom:2px !important;">
	                                        <span class="product-price text-red mr-1 text-size--18 oswold-400">PHP 2,229.00</span>
	                                          {{--<span class="product-price text-line-through text-gray text-size--18">$19.00</span> --}}     
	                               			</div><!-- End .price-box -->
										 <span class="text-uppercase f-15-size oswold-400">NEW</span>
	                                   {{--  <div class="product-action">         
	                                        <button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-bag"></i>ADD TO CART</button>
	                                      
	                                    </div> --}}
	                                </div><!-- End .product-details -->
	                            </div>
	                        </div>
                        </div>
                        <div class="product-default">
                           <div class="text-center card">
	                            <div class="card-body">
	                                <div class="d-flex flex-row">
	                                <img src="{{ asset('frontend/images/avatar.png') }}" alt="product" class="avatar-items">
	                                <div class="d-flex flex-column text-justify">
	                                <span class="ml-3 text-uppercase f-12-size oswold-400">Dhen mark</span>
	                                <span class="ml-3 text-uppercase f-10-size oswold-300">18 minutes ago</span>
	                               
	                            </div>
	                            </div>
	                                <figure>
	                                    <a href="{{route('frontend.products.show')}}">
	                                        <img src="{{ asset('frontend/images/house.jfif') }}" alt="product">
	                                    </a>
	                                </figure>
	                               <div class="product-details start-flex">               
	                                    <h2 class="product-title font-semibold text-uppercase text-limit--title">
	                                        <a class="text-size--14 no-decoration oswold-400" href="{{route('frontend.products.show')}}">Lorem ipsum dummy text aasdsa sadsadsa</a>
	                                    </h2>
	                                    <div class="price-box" style="margin-bottom:2px !important;">
	                                        <span class="product-price text-red mr-1 text-size--18 oswold-400">PHP 2,229.00</span>
	                                          {{--<span class="product-price text-line-through text-gray text-size--18">$19.00</span> --}}     
	                               			</div><!-- End .price-box -->
										 <span class="text-uppercase f-15-size oswold-400">NEW</span>
	                                   {{--  <div class="product-action">         
	                                        <button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-bag"></i>ADD TO CART</button>
	                                      
	                                    </div> --}}
	                                </div><!-- End .product-details -->
	                            </div>
	                        </div>
                        </div>
                    </div><!-- End .featured-proucts -->
                </div><!-- End .container -->
            </div><!-- End .featured-proucts-section -->
        </main><!-- End .main -->
        @include('frontend._components.footer') 
       
    </div><!-- End .page-wrapper -->
    @include('frontend._components.mobile_header') 
   


    <a id="scroll-top" href="#top" role="button"><i class="icon-angle-up"></i></a>

@stop