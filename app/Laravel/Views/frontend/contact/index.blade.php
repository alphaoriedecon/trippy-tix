@extends('frontend._layouts.main') 
@section('content')

    <div class="page-loading">
        <div class="thecube">
            <div class="cube c1"></div>
            <div class="cube c2"></div>
            <div class="cube c4"></div>
            <div class="cube c3"></div>
        </div>
    </div><!--page-loading end-->

    <div class="wrapper">
            
       @include('frontend._components.header')

        <section class="pager-section">
            <div class="container">
                <div class="pager-info">
                    <ul class="breadcrumb">
                        <li><a href="#" title="">Home</a></li>
                        <li><span>Contacts</span></li>
                    </ul><!--breadcrumb end-->
                    <h2>Contacts</h2>
                    <span>How to contact us</span>
                </div>
                <div class="pger-imgs style2">
                    <div class="abt-imgz">
                        <img style="height: 430px" src="{{asset('frontend/images/smart_glass/13.jpg')}}" alt="">
                    </div>
                </div><!--pger-imgs end-->
                <div class="clearfix"></div>
            </div>
        </section><!--pager-section end-->

        <section class="page-content">
            <div class="container">
                <div class="contact-page">
                    <div class="contact-head">
                        <h3 style="font-weight: bold;
    font-size: 25px;">Schedule a Calibration With Us Today!
</h3>
                        <p class="mt-3">Whenever you are in need of exemplary calibration services, reach out to <br> Smart Glass Calibrations! Contact us today to schedule an appointment,<br> and have your vehicle’s ADAS systems ready to provide you the assistance <br> and safety they are specifically designed for!
</p>
                    </div><!--contact-head end-->
                    <div class="contact-main">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="contact_info">
                                    <h3 class="sub-title white">Contacts</h3>
                                    <ul class="cl-list">
                                        <li>
                                            <span class="ci-icon">
                                                <img src="{{asset('frontend/images/ci1.png')}}" alt="">
                                            </span>
                                            <p>Sample Address</p>
                                        </li>
                                        <li>
                                            <span class="ci-icon">
                                                <img src="{{asset('frontend/images/ci2.png')}}" alt="">
                                            </span>
                                            <p><span>Mon-Sat:</span> 9 am til 6 pm <span>Sunday:</span> Closed</p>
                                        </li>
                                        <li>
                                            <span class="ci-icon">
                                                <img src="{{asset('frontend/images/ci3.png')}}" alt="">
                                            </span>
                                            <p>Sample email address</p>
                                        </li>
                                        <li>
                                            <span class="ci-icon">
                                                <img src="{{asset('frontend/images/ci4.png')}}" alt="">
                                            </span>
                                            <p>Sample Contact Number</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="contact-main-form">
                                    <form method="post" action="#" id="contact-form">
                                        <div class="response"></div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Name</label>
                                                    <input type="text" name="name" class="form-control name" placeholder="">
                                                </div><!--form-group end-->
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>What city are you from?</label>
                                                    <input type="email" name="text" class="form-control email" placeholder="">
                                                </div><!--form-group end-->
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>E-mail</label>
                                                    <input type="email" name="email" class="form-control email" placeholder="Example@gmail.com">
                                                </div><!--form-group end-->
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Phone Number</label>
                                                    <input type="text" name="name" class="form-control" placeholder="">
                                                </div><!--form-group end-->
                                            </div>
                                            <div class="col-sm-10">
                                                <div class="form-group">
                                                    <label>Message</label>
                                                    <textarea class="form-control" rows="10"></textarea>
                                                </div><!--form-group end-->
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-submit">
                                                    <button type="button" id="submit"><img src="{{asset('frontend/images/submit.png')}}" alt=""></button>
                                                </div><!--form-submit end-->
                                            </div>
                                        </div>
                                    </form>
                                </div><!--contact-main-form end-->
                            </div>
                        </div>
                    </div><!--contact-main end-->
                    <div class="contact-social">
                        <span>Subscribe to our social networks :</span>
                        <ul class="social-links without-bg">
                            <li><a href="#" title=""><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#" title=""><i class="fab fa-instagram"></i></a></li>
                            <li><a href="#" title=""><i class="fab fa-facebook-f"></i></a></li>
                        </ul>
                    </div><!--contact-social end-->
                </div><!--contact-page end-->
            </div>
        </section><!--page-content end-->

         @include('frontend._components.footer')

    </div><!--wrapper end-->

@stop