@extends('system._layouts.main')

@section('content')
<div class="content content-components section-wrapper mb-5">
  <div class="pl-2 pr-2">
      <div class="tx-13 mg-b-25">
        <div class="row">
          <div class="col-lg-8">
            <div class="d-flex flex-row">
            <a href="{{ route('system.sub_category.index') }}"><i data-feather="skip-back" class="mt-1 mr-2"></i></a>
            <h3>Create Product Category</h3>
          </div>
            <p class="tx-14 mg-b-30">Fill out the form below to create your product keys.</p>
               <form action="" method="POST" enctype="multipart/form-data">
              <section class="mt-4">     
                  <div class="row row-sm">

                 
                      {{ csrf_field() }}
                    <div class="form-group col-md-12">
                      <label class="text-uppercase font-weight-bold">Main Category</label>
                      <select class="custom-select" name="main_category">
                        <option disabled="" selected="">-- Select Main Category --</option>
                         @foreach ($main_category as $category)
                            <option value="{{ $category->name }}">{{Str::title($category->name) }}</option>
                        @endforeach
                      </select>
                      @if($errors->has("main_category"))
                      <span class="text text-danger">{{ $errors->first("main_category") }}</span>
                      @endif
                    </div>
                    <div class="form-group col-md-12">
                      <label class="text-uppercase font-weight-bold">Sub Category Name</label>  

                      <input type="text" value="{{old('name')}}" class="form-control" name="name" autocomplete="off">
                      @if($errors->has("name"))
                      <span class="text text-danger">{{ $errors->first("name") }}</span>
                      @endif
                    </div>   
                    <div class="form-group col-md-8">
                      <img id="blah" src="#" class="image-preview" alt="your image" />
                    </div>
                    <div class="form-group col-md-12 {{$errors->first('file') ? 'has-error' : NULL}}">
                      <input type="file" class="form-control" name="file" id="imgInp" />
                      @if($errors->first('file'))
                        <span class="help-block">{{$errors->first('file')}}</span>
                      @endif
                    </div> 
                     <div class="form-group col-md-12">
                      <label class="text-uppercase font-weight-bold">Remarks</label>  

                      <input type="text" value="{{old('remarks')}}" class="form-control" name="remarks" autocomplete="off">
                      @if($errors->has("remarks"))
                      <span class="text text-danger">{{ $errors->first("remarks") }}</span>
                      @endif
                    </div>  

                    {{-- <div class="form-group col-md-12">
                      <label class="text-uppercase font-weight-bold">Product Expiration Date</label>                  
                      <input type="date" class="form-control" name="title">
                    </div>  --}}     
                    <div class="col-md-12 text-right">
                      <button class="btn btn-primary" type="submit"><i data-feather="check-square" class="mr-2"></i>Create Sub Category</button>
                    </div>  
                  
                  </div><!-- row -->
               </section>
                 </form>      
             </div>
           </div>
      </div>
  </div><!-- container -->
</div><!-- content -->
@stop