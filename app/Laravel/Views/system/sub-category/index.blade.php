@extends('system._layouts.main')

@section('content')
<div class="content content-components section-wrapper mb-5">
      <div>
        <div class="d-flex justify-content-md-between mb-2">
          <h4 id="section1" class="mb-3 mb-lg-0 mb-md-0 mb-xl-0">List of Sub Categories</h4>
          <a href="{{ route('system.sub_category.create')}}" class="btn btn-primary mg-b-10 font-small">
          	<i data-feather="check-square" class="mr-2"></i>Create Sub Category
          </a>
        </div>
        
        <div class="row">
        	
          <div class="col-lg-12 mt-3">
          	@include('system._components.notifications')
             <div class="df-example demo-table">
	            <div class="table-responsive">
	              <table class="table table-hover mg-b-0">
	                <thead>
	                  <tr>
                        <th scope="col">Images</th>
                        <th scope="col">Main Category</th>
	                    <th scope="col">Sub Category</th>
	                    <th scope="col">Remarks</th>
	                    <th scope="col">Action</th>
	                  </tr>
	                </thead>
	                <tbody>

	                	@forelse ($categories as $category)
                        <tr>
                            <td><img src="{{ "{$category->directory}/resized/{$category->filename}" }}" class="image-size-auto--90">
                            </td>
                            <td>{{Str::title($category->main_category) }} </td> 
	                    	<td>{{Str::title($category->name) }} </td>  
                            <td>{{Str::title($category->remarks) }} </td>           
	                    	<td>
    	                    	<a href="{{ route('system.sub_category.edit',[$category->id]) }}" title="Update Product Category" class="btn btn-sm btn-light text-dark bg-warning">
    	                         <i data-feather="edit"></i>
    	                       </a>
    	                       <a data-toggle="modal" id="delete" data-role="delete" data-url="{{ route('system.sub_category.delete',[$category->id]) }}" data-target="#deactivate" title="Deactivate Product Category" class="btn btn-sm btn-light text-white bg-danger">
    	                         <i data-feather="x"></i>
    	                       </a>        
	                       </td>
	                    </tr>
	                	@empty
			              <td colspan="5" class="text-center"><i>No record found yet.</i> <a href="{{route('system.sub_category.create')}}"><strong>Click here</strong></a> to create one.</td>
	                	@endforelse
	                
	                
	                </tbody>
	              </table>
	            </div><!-- table-responsive -->
          </div><!-- df-example -->
         </div>      
       </div>
      </div><!-- container -->
    </div><!-- content -->

{{-- Modals --}}
@include('system.product-category.modals.activate')
@include('system.product-category.modals.deactivate')
@stop

@section('page-scripts')

<script type="text/javascript">
	$(document).ready(function(){


		$(document).on('click','a[data-role=delete]',function(){
			
			var url = $(this).data("url")
			$('#confirm_delete').attr({"href": url});

		})
		

	})
</script>

<script type="text/javascript">
	 
</script>
@stop