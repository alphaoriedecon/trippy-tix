<!DOCTYPE html>
<html>
<head>
    @include('system._components.metas')
    @include('system._components.styles')

    <style type="text/css">
          .oswold-300 {
        font-family: 'Oswald', sans-serif !important;
        font-weight: 300 !important;
    }
     .oswold-400 {
        font-family: 'Oswald', sans-serif !important;
        font-weight: 400 !important;
    }
     .oswold-500 {
        font-family: 'Oswald', sans-serif !important;
        font-weight: 500 !important;
    }
     .oswold-600 {
        font-family: 'Oswald', sans-serif !important;
        font-weight: 600 !important;
    }
     .oswold-700 {
        font-family: 'Oswald', sans-serif !important;
        font-weight: 700 !important;
    }
    .sans-300 {
        font-family: 'Open Sans', sans-serif !important;
        font-weight: 300 !important;
    }
    .sans-300-i {
        font-family: 'Open Sans', sans-serif !important;
        font-weight: 300 !important;
        font-style: italic;
    }
    .sans-400 {
        font-family: 'Open Sans', sans-serif !important;
        font-weight: 400 !important;
    }
    .sans-600 {
        font-family: 'Open Sans', sans-serif !important;
        font-weight: 600 !important;
    }
    .sans-700 {
        font-family: 'Open Sans', sans-serif !important;
        font-weight: 700 !important;
    }
    
    .bg-green {
        background-color:#16887C !important;
    }
    .text-green {
        color:#16887C !important;
    }
    .bg-red {
        background-color:#FD233B !important;
    }
    .text-red {
        color:#FD233B !important;
    }
    .card {
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
    }
    </style>
    @yield('page-styles')

</head>

<body style="background-color: #ecf0f1">

        @yield('content')

        @include('system._components.scripts')
        @yield('page-scripts')
</body>
</html>