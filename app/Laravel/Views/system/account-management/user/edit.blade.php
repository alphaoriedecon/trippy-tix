@extends('system._layouts.main')

@section('content')
<div class="content content-components section-wrapper mb-5">
  <div class="pl-2 pr-2">
      <div class="tx-13 mg-b-25">
        <div class="row">
          <div class="col-lg-8">
            <h3>Update Account</h3>
            <p class="tx-14 mg-b-30">Fill out the form below to update your information.</p>
             <form action="" method="POST" enctype="multipart/form-data"> 
              {{ csrf_field() }}
              <section class="mt-4">     
                  <div class="row row-sm">
                    <div class="form-group col-md-8">
                      <img id="blah" src="{{ "{$users->directory}/resized/{$users->filename}" }}" class="image-preview" alt="your image" />
                    </div>
                    <div class="form-group col-md-12 {{$errors->first('file') ? 'has-error' : NULL}}">
                      <input type="file" class="form-control" name="file" id="imgInp" />
                      @if($errors->first('file'))
                        <span class="help-block">{{$errors->first('file')}}</span>
                      @endif
                    </div>
                        <div class="col-md-12">
                           <div class="form-group {{$errors->first('url') ? 'has-error' : NULL}}">
                              <label>Messenger URL</label>
                              <input type="text" class="form-control" name="url" value="{{old('url',$users->url)}}">
                              @if($errors->first('url'))
                              <span class="help-block">{{$errors->first('url')}}</span>
                              @endif
                          </div>
                      </div>
                     <div class="col-md-6">
                           <div class="form-group {{$errors->first('first_name') ? 'has-error' : NULL}}">
                              <label>First Name</label>
                              <input type="text" class="form-control" name="first_name" value="{{old('first_name',$users->first_name)}}">
                              @if($errors->first('first_name'))
                              <span class="help-block">{{$errors->first('first_name')}}</span>
                              @endif
                          </div>
                      </div>
                     
                      <div class="col-md-6">
                           <div class="form-group {{$errors->first('last_name') ? 'has-error' : NULL}}">
                              <label>Last Name</label>
                              <input type="text" class="form-control" name="last_name" value="{{old('last_name',$users->last_name)}}">
                              @if($errors->first('last_name'))
                              <span class="help-block">{{$errors->first('last_name')}}</span>
                              @endif
                          </div>
                      </div>
                      <div class="col-md-6">
                           <div class="form-group {{$errors->first('mobile_number') ? 'has-error' : NULL}}">
                              <label>Mobile Number</label>
                              <input type="number" class="form-control" name="mobile_number" value="{{old('mobile_number',$users->mobile_number)}}">
                              @if($errors->first('mobile_number'))
                              <span class="help-block">{{$errors->first('mobile_number')}}</span>
                              @endif
                          </div>
                      </div>
                       <div class="col-md-6">
                           <div class="form-group {{$errors->first('gender') ? 'has-error' : NULL}}">
                              <label>Gender</label>
                              <select class="form-control" name="gender">
                                <option disabled="" selected="">--Select Gender--</option>
                                  @if($users->gender ==='male')
                                  <option value="male" selected="">Male</option>
                                  <option value="female">Female</option>  
                                  @else
                                  <option value="male" >Male</option>
                                  <option value="female" selected="">Female</option>  
                                  @endif
                              </select>
                              @if($errors->first('gender'))
                              <span class="help-block">{{$errors->first('gender')}}</span>
                              @endif
                          </div>
                      </div>
                      <div class="col-md-6">
                           <div class="form-group {{$errors->first('email') ? 'has-error' : NULL}}">
                              <label>Email Address</label>
                              <input type="text" class="form-control" name="email" value="{{old('email',$users->email)}}">
                              @if($errors->first('email'))
                              <span class="help-block">{{$errors->first('email')}}</span>
                              @endif
                          </div>
                      </div>              
                      <div class="col-md-6">
                          <div class="{{$errors->first('username') ? 'has-error' : NULL}}">
                              <label>Username</label>
                              <input type="text" class="form-control" name="username" value="{{old('username',$users->username)}}">
                              @if($errors->first('username'))
                              <span class="help-block">{{$errors->first('username')}}</span>
                              @endif
                          </div>
                      </div>
                       <div class="col-md-6">
                          <div class="{{$errors->first('password') ? 'has-error' : NULL}}">
                              <label>New Password</label>
                              <input type="password" class="form-control" name="password" value="{{old('password')}}">
                              @if($errors->first('password'))
                              <span class="help-block">{{$errors->first('password')}}</span>
                              @endif
                          </div>
                      </div>
                       <div class="col-md-6">
                          <div class="{{$errors->first('password_confirmation') ? 'has-error' : NULL}}">
                              <label>Confirm Password</label>
                              <input type="password" class="form-control" name="password_confirmation" value="{{old('password_confirmation')}}">
                              @if($errors->first('password_confirmation'))
                              <span class="help-block">{{$errors->first('password_confirmation')}}</span>
                              @endif
                          </div>
                      </div>
                    <div class="col-md-12 text-right mt-3">
                      <button  type="submit" class="btn btn-primary"><i data-feather="user-plus" class="mr-2"></i>Update Account</button>
                    </div>        
                  </div><!-- row -->
               </section>
             </form>
             </div>
           </div>
        </div>
    </div><!-- container -->
</div><!-- content -->
@stop