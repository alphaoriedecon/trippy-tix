@extends('system._layouts.main')

@section('content')
<div class="content content-components section-wrapper mb-5">
      <div>
        <div class="d-flex justify-content-between mb-2">
          <h4 id="section1" class="mb-3 mb-lg-0 mb-md-0 mb-xl-0 oswold-400">List of User</h4>
          <a href="{{ route('system.account_management.user.create')}}" class="btn btn-primary mg-b-10 font-small">
          	<i data-feather="user-check" class="mr-2"></i>Create User
          </a>
        </div>
        <div class="row">
      
          <div class="col-lg-12 mt-3">
             <div class="df-example demo-table">
                <div class="table-responsive">
                  <table class="table table-hover mg-b-0">
	                <thead>
	                  <tr>
	                    <th scope="col" class="oswold-400"></th>
	                    <th scope="col" class="oswold-400">Fullname</th>
	                    <th scope="col" class="oswold-400">Username</th>      
	                    <th scope="col" class="oswold-400">Status</th>
	                    <th scope="col" class="oswold-400">Action</th>
	                  </tr>
	                </thead>
	                <tbody>
	                	 @foreach($user_list as  $user)
	                  <tr>
	                  	<th class="pt-3 pb-3 sans-300"> <img style="height: 80px;width: auto;border-radius: 10px;" src="{{ "{$user->directory}/resized/{$user->filename}" }}"></th>
	                    <th class="pt-3 pb-3 sans-300">{{$user->first_name}} {{$user->last_name}}</th>
	                    <td class="pt-3 pb-3 sans-300">{{$user->username}}</td>
	                    <td class="pt-3 pb-3 sans-300">
	                    	@if($user->status == 'active')
	                    	<span class="bg-success p-2 rounded text-white">Active</span>
	                    	@else
	                    	<span class="bg-danger p-2 rounded text-white">Deactivated</span>   
	                    	@endif
	                    </td>       
   
	                    <td>
	                    	 	@if($user->status == 'active')
	                     <a href="{{route('system.account_management.user.edit',[$user->id])}}" title="Edit Account" class="btn btn-sm btn-light text-white bg-success">
	                         <i data-feather="edit"></i>
	                     </a>
	                    
	                      <a data-toggle="modal" data-target="#deactivate" title="Deactivate" class="btn btn-sm btn-light text-white bg-danger action-delete" data-url="{{route('system.account_management.user.inactive',[$user->id])}}">
	                         <i data-feather="user-x"></i>                   
	                     	</a>  
	                     	@else 
													<a data-toggle="modal" data-target="#activate" title="activate" class="btn btn-sm btn-light text-white bg-success action-delete" data-url="{{route('system.account_management.user.active',[$user->id])}}">
	                         <i data-feather="user-check"></i>                   
	                     	</a>  
	                     	@endif     
	                    </td>
	                  </tr>
	                  @endforeach
	                 


	                </tbody>
	              </table>
	            </div><!-- table-responsive -->
          </div><!-- df-example -->
         </div>      
       </div>
      </div><!-- container -->
    </div><!-- content -->

{{-- Modals --}}
@include('system.account-management.user.modals.activate')
@include('system.account-management.user.modals.deactivate')
@stop

@section('page-scripts')

<script type="text/javascript">
	 
</script>
@stop