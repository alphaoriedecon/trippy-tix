@extends('system._layouts.auth')

@section('content')
<a class="btn oswold-300 text-uppercase text-white btn--back" style="background-color: #FFCC00 !important" href="{{route('frontend.home.index')}}">Back to Homepage</a>
	<div class="content mt-5 content-fixed content-auth">
    <div class="container">
      <div class="media align-items-stretch justify-content-center ht-100p pos-relative">
        {{-- <div class="media-body align-items-center d-none d-lg-flex">
          <div class="mx-wd-600">
            <img src="{{asset('assets/img/img15.png')}}" class="img-fluid" alt="">
          </div>
        </div><!-- media-body --> --}}

        <div class="sign-wrapper mg-lg-l-50 mg-xl-l-60 card p-4">

          <div class="" >
            <h3 class="tx-color-01 mg-b-5 oswold-400">Sign In</h3>
            <p class="tx-color-03 tx-16 mg-b-40 oswold-300">Welcome back! Please signin to continue.</p>
            @include('system._components.notifications')
            <form action="" method="POST">
              {!!csrf_field()!!}
              <div class="form-group">
                <label class="oswold-300">Email address</label>
                <input type="text" class="form-control" name="username" id="username" placeholder="yourname@yourmail.com" value="{{old('username')}}">
              </div>
              <div class="form-group">
                <div class="d-flex justify-content-between mg-b-5">
                  <label class="mg-b-0-f oswold-300">Password</label>
                </div>
                <input type="password" class="form-control" name="password" id="password" placeholder="Enter your password">
              </div>
              <button type="submit" id="login" class="oswold-300 text-uppercase btn text-white btn-block" style="background-color: #FFCC00 !important">Sign in</button>
            </form>
          </div>
        </div><!-- sign-wrapper -->
      </div><!-- media -->
    </div><!-- container -->
  </div><!-- content -->
@stop
@section('page-scripts')

<script type="text/javascript">
    $(document).ready(function(){
        
// login
     $('#login').on('click',function(event){
      
      var username = $('#username').val();
        var password = $('#password').val();
            $.ajax({

            method:"POST",
            data:{ username: username,password: password },
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
             url:"{{ route("api.auth.login",['json']) }}",
            success:function(response){

              console.log(response.token)
              localStorage.setItem("token",response.token);

              setToken(response.token)
              console.log(response.data)
              getUserProfie(JSON.response.data)
              // location.reload();
              
              
            },
            error:function(response){

             console.log(response.responseJSON.msg)
             $('#error').text(response.responseJSON.msg);
             $('#error').removeAttr("hidden")
           }

        })
        })//end of login
    })
     
</script>
@stop