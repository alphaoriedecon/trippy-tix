@extends('system._layouts.main')

@section('content')
<div class="content content-components section-wrapper mb-5">
  <div class="pl-2 pr-2">
      <div class="tx-13 mg-b-25">
        <div class="row">
          <div class="col-lg-8">
            <div class="d-flex flex-row">
            <i data-feather="skip-back" class="mt-1 mr-2"></i>
            <h3>Update Product Keys</h3>
          </div>
            <p class="tx-14 mg-b-30">Fill out the form below to update your product keys.</p>
            <form method="POST">
              {{ csrf_field() }}
              <section class="mt-4">     
                  <div class="row row-sm">
                    <div class="form-group col-md-12">
                      <label class="text-uppercase font-weight-bold">Product</label>
                      <select class="custom-select" name="product_id" >
                        <option value="{{ old("product_id",$key->products->id)}}">{{ old('product_id',$key->products->title) }}</option>
                        @foreach ($products as $product)
                          <option value="{{ old('product_id',$product->id) }}">{{ old('product_id',$product->title) }}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group col-md-12">
                      <label class="text-uppercase font-weight-bold">Product Keys</label>                  
                      <input type="text" class="form-control" name="product_key" value="{{ old('product_key',$key->product_key) }}">
                    </div>   
                    <div class="form-group col-md-12">
                      <label class="text-uppercase font-weight-bold">Product Expiration Date</label>                  
                      <input type="date" class="form-control" name="expiration" value="{{ old('expiration',$key->expiration) }}">
                    </div>         
                    <div class="col-md-12 text-right">
                      <button class="btn btn-primary" type="submit"><i data-feather="edit" class="mr-2"></i>Update Keys</button>
                    </div>        
                  </div><!-- row -->
               </section>
               </form>
             </div>
           </div>
      </div>
  </div><!-- container -->
</div><!-- content -->
@stop