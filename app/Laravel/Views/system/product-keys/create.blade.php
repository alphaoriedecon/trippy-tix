@extends('system._layouts.main')

@section('content')
<div class="content content-components section-wrapper mb-5">
  <div class="pl-2 pr-2">
      <div class="tx-13 mg-b-25">
        <div class="row">
          <div class="col-lg-8">
            <div class="d-flex flex-row">
            <i data-feather="skip-back" class="mt-1 mr-2"></i>
            <h3>Create Product Keys</h3>
          </div>
            <p class="tx-14 mg-b-30">Fill out the form below to create your product keys.</p>
            <form method="POST">

              {{csrf_field()}}
              <section class="mt-4">      
                  <div class="row row-sm">
                    <div class="form-group col-md-12">
                      <label class="text-uppercase font-weight-bold">Product</label>
                      <select class="custom-select" name="product_id">
                        <option value="">--Choose Product</option>
                        @foreach ($products as $product)
                             <option value="{{old('product_id',$product->id) }}">{{ $product->title }} {{ $product->description}}</option>
                        @endforeach
                     
                      </select>

                      @if ($errors->has("product_id"))
                        <span class="text text-danger">{{ $errors->first("product_id") }}</span>
                      @endif
                    </div>
                    <div class="form-group col-md-12">
                      <label class="text-uppercase font-weight-bold">Product Keys</label>                  
                      <input type="text" class="form-control" name="product_key" value="{{ old('product_key') }}">
                       @if ($errors->has("product_key"))
                        <span class="text text-danger">{{ $errors->first("product_key") }}</span>
                      @endif
                    </div>  

                     <div class="form-group col-md-12">
                      <label class="text-uppercase font-weight-bold">Number of User</label>                  
                      <input type="number"  min="1" value="1" class="form-control" name="qty">
                      @if ($errors->has("qty"))
                        <span class="text text-danger">{{ $errors->first("qty") }}</span>
                      @endif
                    </div> 
                    <div class="form-group col-md-12">
                      <label class="text-uppercase font-weight-bold">Product Expiration Date</label>                  
                      <input type="date" class="form-control" name="expiration" value="{{ old('expiration') }}">
                       @if ($errors->has("expiration"))
                        <span class="text text-danger">{{ $errors->first("expiration") }}</span>
                      @endif
                    </div>
                    <div class="form-group col-md-12">
                      <label class="text-uppercase font-weight-bold">Price</label>                  
                      <input type="number"  min="0" value="{{old("price")}}"  class="form-control" name="price">
                      @if ($errors->has("price"))
                        <span class="text text-danger">{{ $errors->first("price") }}</span>
                      @endif
                    </div>    
                    <div class="col-md-12 text-right">
                      <button class="btn btn-primary" type="submit"><i data-feather="check-square" class="mr-2"></i>Create Product Keys</button>
                    </div>        
                  </div><!-- row -->
               </section>
               </form>
             </div>
           </div>
      </div>
  </div><!-- container -->
</div><!-- content -->
@stop