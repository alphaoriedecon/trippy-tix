@extends('system._layouts.main')

@section('content')
<div class="content content-components section-wrapper mb-5">
      <div>
        <div class="d-flex justify-content-md-between mb-2">
          <h4 id="section1" class="mb-3 mb-lg-0 mb-md-0 mb-xl-0">List of Product Keys</h4>
          <a href="{{ route('system.product_keys.create')}}" class="btn btn-primary mg-b-10 font-small">
          	<i data-feather="check-square" class="mr-2"></i>Create Product Keys
          </a>
        </div>
        <form method="get">
        		<div class="row">   			
		        	<div class="col-lg-6">
				          <div class="row">
				            <div class="col-12">
				              <input type="text" class="form-control" name="title" value="{{ old("title") }}" placeholder="Search">
				            </div><!-- col -->
				          </div><!-- row -->
		        	</div>
		        	<div class="col-lg-6">
				          <div class="d-flex flex-row">
				             <select class="form-control" name="status">
		                    <option disabled="" selected="">Choose a status</option>
		                   	<option value="active">Active</option>
		                   	<option value="inactive">In-active</option>
		                 </select>    
				          <button class="btn btn-primary ml-2"><i data-feather="search"></i></button>
				           <a class="btn btn-primary ml-2" href="{{ route("system.products.index")}}">Clear</a>
				          </div><!-- row -->
		        	</div>     		
            </div>
        	</form>
        <div class="row">       	
          <div class="col-lg-12 mt-3">
          	@include("system._components.notifications")
             <div class="df-example demo-table">
	            <div class="table-responsive">
	              <table class="table table-hover mg-b-0">
	                <thead>


	                  <tr>
	                    
	                    <th scope="col">Product Name</th>
	                    <th scope="col">Product Keys</th>	
	                    <th scope="col">Number of User</th>	
	                    <th scope="col">Expiration Date</th>	 
	                    <th scope="col">Status</th>
	                    <th scope="col">Action</th>
	                  </tr>
	                </thead>
	                <tbody>
	                	@forelse ($product_keys as $product_key)
	                		 <tr>
			                  	 <td class="pt-3 pb-3">{{ strtoupper($product_key->products->title) }}</td>
			                    <td class="pt-3 pb-3">{{ $product_key->product_key }}</td>
			                    <td class="pt-3 pb-3">{{ $product_key->qty }}</td>
			                    <td class="pt-3 pb-3">{{ date('M d, Y', strtotime($product_key->expiration)) }}</td>
			     			    <td class="pt-3 pb-3"><span class="bg-success p-2 rounded text-white">{{ $product_key->products->status }}</span></td>            
	                    		<td>
	                    	<a href="{{route('system.product_keys.edit',[$product_key->id])}}" title="Update Product Category" class="btn btn-sm btn-light text-dark bg-warning">
	                         <i data-feather="edit"></i>
	                     </a>
	                     <a data-toggle="modal" id="delete" data-role="delete" 
	                      		data-url="{{ route('system.product_keys.delete',[$product_key->id]) }}" data-target="#deactivate" title="Deactivate Product Category" class="btn btn-sm btn-light text-white bg-danger">
	                         <i data-feather="x"></i>
	                     </a>      
	                    </td>
	                  </tr>
	                  @empty
			              <td colspan="5" class="text-center"><i>No record found yet.</i> <a href="{{route('system.product_keys.create')}}"><strong>Click here</strong></a> to create one.</td>
	                	@endforelse
	                 
	                
	                </tbody>
	              </table>
	            </div><!-- table-responsive -->
          </div><!-- df-example -->
         </div>      
       </div>
      </div><!-- container -->
    </div><!-- content -->

{{-- Modals --}}
@include('system.product-keys.modals.activate')
@include('system.product-keys.modals.deactivate')
@stop

@section('page-scripts')

<script type="text/javascript">
	$(document).ready(function(){

		
		$(document).on('click','a[data-role=delete]',function(){
			var url = $(this).data("url")
			$('#confirm_delete').attr({"href": url});

		})
		

	})
</script>

<script type="text/javascript">
	 
</script>
@stop