@extends('system._layouts.main')

@section('content')
<div class="content content-components section-wrapper mb-5">
  <div class="pl-2 pr-2">
    <div class="row">
      <div class="col-md-7">
        <div class="tx-13 mg-b-25">
          <h3>Account Settings</h3>
          <p class="tx-14 mg-b-30">Fill out the form below to change your password.</p>
          <form method="POST">
            <section class="mt-4">     
                <div class="row row-sm">
                  
                   <div class="col-md-12">
                          <div class="{{$errors->first('password') ? 'has-error' : NULL}}">
                              <label>New Password</label>
                              <input type="password" class="form-control" name="password" value="{{old('password')}}">
                              @if($errors->first('password'))
                              <span class="help-block">{{$errors->first('password')}}</span>
                              @endif
                          </div>
                      </div>
                       <div class="col-md-12 mt-3">
                          <div class="{{$errors->first('password_confirmation') ? 'has-error' : NULL}}">
                              <label>Confirm Password</label>
                              <input type="password" class="form-control" name="password_confirmation" value="{{old('password_confirmation')}}">
                              @if($errors->first('password_confirmation'))
                              <span class="help-block">{{$errors->first('password_confirmation')}}</span>
                              @endif
                          </div>
                      </div>
                  <div class="col-md-12 text-right mt-3">
                    <button type="submit" class="btn btn-primary"><i data-feather="edit" class="mr-2"></i>Change Password</button>
                  </div>        
                </div><!-- row -->
            </section>
            </form>
        </div>
      </div>
    </div>
  </div><!-- container -->
</div><!-- content -->
@stop