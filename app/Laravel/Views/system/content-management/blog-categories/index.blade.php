@extends('system._layouts.main')

@section('content')
<div class="content content-components section-wrapper mb-5">
      <div>
        <div class="d-flex justify-content-md-between mb-2">
          <h4 id="section1" class="mb-3 mb-lg-0 mb-md-0 mb-xl-0">List of Blog Category</h4>
          <a href="{{ route('system.content_management.blog_categories.create')}}" class="btn btn-primary mg-b-10 font-small">
          	<i data-feather="check-square" class="mr-2"></i>Create Category
          </a>
        </div> 
        <div class="row">
        	<div class="col-lg-12">
		          <div class="row">
		            <div class="col-12 d-flex flex-row">
		              <input type="text" class="form-control" placeholder="Search">
		              <button class="btn btn-primary ml-2"><i data-feather="search"></i></button>
		            </div><!-- col -->
		          </div><!-- row -->
        	</div>
          <div class="col-lg-12 mt-3">
             <div class="df-example demo-table">
	            <div class="table-responsive">
	              <table class="table table-hover mg-b-0">
	                <thead>
	                  <tr>
	                    <th scope="col">ID</th>
	                    <th scope="col">Category Name</th>
	                    <th scope="col">Action</th>
	                  </tr>
	                </thead>
	                <tbody>
	                	@forelse($blog_categories as $blog_category)
	                  <tr>
	                    <th class="pt-3 pb-3">{{$blog_category->id}}</th>
	                    <td class="pt-3 pb-3">{{Str::title($blog_category->category_name)}}</td>
	                    <td>
	                    	<a href="{{route('system.content_management.blog_categories.edit',[$blog_category->id])}}" title="Update Blog Category Item" class="btn btn-sm btn-light text-dark bg-warning">
	                         <i data-feather="edit"></i>
	                     </a>
	                      <a data-toggle="modal" data-target="#confirm-delete" title="Remove Banner Item" class="btn btn-sm btn-light text-white bg-danger action-delete" data-url="{{route('system.content_management.blog_categories.destroy',[$blog_category->id])}}">
	                         <i data-feather="x"></i>
	                         
	                     </a> 
	                    </td>
	                  </tr>
	                  @empty
			              <td colspan="3" class="text-center"><i>No record found yet.</i> <a href="{{route('system.content_management.blog_categories.create')}}"><strong>Click here</strong></a> to create one.</td>
			              @endforelse
	                
	                </tbody>
	              </table>
	            </div><!-- table-responsive -->
          </div><!-- df-example -->
         </div>      
       </div>
      </div><!-- container -->
    </div><!-- content -->

{{-- Modals --}}
@include('system.content-management.modals.activate')
@include('system.content-management.modals.deactivate')
@stop

@section('page-scripts')

<script type="text/javascript">
	 
</script>
@stop