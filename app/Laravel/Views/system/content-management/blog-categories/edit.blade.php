@extends('system._layouts.main')

@section('content')
<div class="content content-components section-wrapper mb-5">
  <div class="pl-2 pr-2">
      <div class="tx-13 mg-b-25">
        <div class="row">
          <div class="col-lg-8">
            <div class="d-flex flex-row">
            <i data-feather="skip-back" class="mt-1 mr-2"></i>
            <h3>Update Category</h3>
          </div>
            <p class="tx-14 mg-b-30">Fill out the form below to update your category.</p>
              <form method="POST" action="" enctype="multipart/form-data">
               {!!csrf_field()!!}
              <section class="mt-4">     
                  <div class="row row-sm">
                    <div class="form-group col-md-12 {{$errors->first('category_name') ? 'has-error' : NULL}}">
                      <label class="text-uppercase font-weight-bold">Category Name</label>                  
                      <input type="text" class="form-control" name="category_name" value="{{old('category_name',$blog_categories->category_name)}}">
                      @if($errors->first('category_name')) 
                        <span class="help-block">{{$errors->first('category_name')}}</span>
                      @endif
                    </div>
                    <div class="col-md-12 text-right">
                      <button class="btn btn-primary" type="submit"><i data-feather="check-square" class="mr-2"></i>Update Category</button>
                    </div>        
                  </div><!-- row -->
               </section>
             </form>
             </div>
           </div>
      </div>
  </div><!-- container -->
</div><!-- content -->
@stop