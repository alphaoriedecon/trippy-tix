@extends('system._layouts.main')

@section('content')
<div class="content content-components section-wrapper mb-5">
  <div class="pl-2 pr-2">
      <div class="tx-13 mg-b-25">
        <div class="row">
          <div class="col-lg-8">
            <div class="d-flex flex-row">
            <i data-feather="skip-back" class="mt-1 mr-2"></i>
            <h3>Create Partner</h3>
          </div>
            <p class="tx-14 mg-b-30">Fill out the form below to create your partner.</p>
              <section class="mt-4">     
                  <div class="row row-sm">
                    <div class="form-group col-md-12">
                      <label class="text-uppercase font-weight-bold">Title</label>                  
                      <input type="text" class="form-control" name="title">
                    </div>
                   <div class="form-group col-md-8">
                      <img id="blah" src="#" class="image-preview" alt="your image" />
                    </div>
                    <div class="form-group col-md-12">
                      <input type="file" class="form-control" id="imgInp" />
                    </div>
                    <div class="form-group col-md-12">
                      <label class="text-uppercase font-weight-bold">Description</label>
                      <textarea class="form-control" rows="7" name="description"></textarea>
                    </div>        
                    <div class="col-md-12 text-right">
                      <button class="btn btn-primary" type="submit"><i data-feather="check-square" class="mr-2"></i>Create Partner</button>
                    </div>        
                  </div><!-- row -->
               </section>
             </div>
           </div>
      </div>
  </div><!-- container -->
</div><!-- content -->
@stop