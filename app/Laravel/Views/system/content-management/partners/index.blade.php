@extends('system._layouts.main')

@section('content')
<div class="content content-components section-wrapper mb-5">
      <div>
        <div class="d-flex justify-content-md-between mb-2">
          <h4 id="section1" class="mb-3 mb-lg-0 mb-md-0 mb-xl-0">List of Partners</h4>
          <a href="{{ route('system.content_management.partners.create')}}" class="btn btn-primary mg-b-10 font-small">
          	<i data-feather="check-square" class="mr-2"></i>Create Partner
          </a>
        </div>
        <div class="row">
        	<div class="col-lg-6">
		          <div class="row">
		            <div class="col-12">
		              <input type="text" class="form-control" placeholder="Search">
		            </div><!-- col -->
		          </div><!-- row -->
        	</div>
        	<div class="col-lg-6">
		          <div class="row">
		            <div class="col-12">
		              <select class="custom-select" name="status">
                    <option disabled="" selected="">Choose a status</option>
                  </select>
		            </div><!-- col -->
		          </div><!-- row -->
        	</div>
        	
          <div class="col-lg-12 mt-3">
             <div class="df-example demo-table">
	            <div class="table-responsive">
	              <table class="table table-hover mg-b-0">
	                <thead>
	                  <tr>
	                    <th scope="col">ID</th>
	                    <th scope="col">Title</th>
	                    <th scope="col">Description</th>
	                    <th scope="col">Date Created</th>		                    
	                    <th scope="col">Status</th>
	                    <th scope="col">Action</th>
	                  </tr>
	                </thead>
	                <tbody>
	                  <tr>
	                    <th class="pt-3 pb-3">1</th>
	                    <td class="pt-3 pb-3">Lorem ipsum</td>
	                    <td class="pt-3 pb-3">Lorem Ipsum Dummy Text</td>
	                    <td class="pt-3 pb-3">02-18-2020</td>
	                    <td class="pt-3 pb-3"><span class="bg-success p-2 rounded text-white">Active</span></td>            
	                    <td>
	                    	<a href="{{route('system.content_management.partners.update')}}" title="Update Partner Item" class="btn btn-sm btn-light text-dark bg-warning">
	                         <i data-feather="edit"></i>
	                     </a>
	                     <a data-toggle="modal" data-target="#deactivate" title="Remove Partner Item" class="btn btn-sm btn-light text-white bg-danger">
	                         <i data-feather="x"></i>
	                     </a>        
	                    </td>
	                  </tr>
	                
	                </tbody>
	              </table>
	            </div><!-- table-responsive -->
          </div><!-- df-example -->
         </div>      
       </div>
      </div><!-- container -->
    </div><!-- content -->

{{-- Modals --}}
@include('system.content-management.modals.activate')
@include('system.content-management.modals.deactivate')
@stop

@section('page-scripts')

<script type="text/javascript">
	 
</script>
@stop