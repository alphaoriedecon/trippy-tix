@extends('system._layouts.main')

@section('content')
<div class="content content-components section-wrapper mb-5">
  <div class="pl-2 pr-2">
      <div class="tx-13 mg-b-25">
        <div class="row">
          <div class="col-lg-8">
            <div class="d-flex flex-row">
            <i data-feather="skip-back" class="mt-1 mr-2"></i>
            <h3>Update Package</h3>
          </div>
            <p class="tx-14 mg-b-30">Fill out the form below to update your package.</p>
              <form method="POST" action="" enctype="multipart/form-data">
               {!!csrf_field()!!}
              <section class="mt-4">     
                  <div class="row row-sm">
                    <div class="form-group col-md-12 {{$errors->first('title') ? 'has-error' : NULL}}">
                      <label class="text-uppercase font-weight-bold">Title</label>                  
                      <input type="text" class="form-control" name="title" value="{{old('title',$packages->title)}}">
                      @if($errors->first('title'))
                        <span class="help-block">{{$errors->first('title')}}</span>
                      @endif
                    </div>    
               
                    <div class="form-group col-md-8">
                      <img id="blah" src="{{ "{$packages->directory}/resized/{$packages->filename}" }}" class="image-preview" alt="your image" />
                    </div>
                    <div class="form-group col-md-12 {{$errors->first('file') ? 'has-error' : NULL}}">
                      <input type="file" class="form-control" name="file" id="imgInp"  value="{{old('file')}}"/>
                      @if($errors->first('file'))
                        <span class="help-block">{{$errors->first('file')}}</span>
                      @endif
                    </div>
                    
                    <div class="form-group col-md-12 {{$errors->first('content') ? 'has-error' : NULL}}">
                      <label class="text-uppercase font-weight-bold">Description</label>
                      <textarea class="form-control" rows="7" name="content">{{old('content',$packages->content)}}</textarea>
                      @if($errors->first('content'))
                        <span class="help-block">{{$errors->first('content')}}</span>
                      @endif
                    </div>        
                    <div class="col-md-12 text-right">
                      <button type="submit" class="btn btn-primary"><i data-feather="check-square" class="mr-2"></i>Update Package</button>
                    </div>        
                  </div><!-- row -->
               </section>
             </form>
             </div>
           </div>
      </div>
  </div><!-- container -->
</div><!-- content -->
@stop

@section('page-scripts')
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
<script type="text/javascript">

// Initialize CKEditor

CKEDITOR.replace('content',{

  width: "auto",
  height: "200px"

}); 


</script>

@stop