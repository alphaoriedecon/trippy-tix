@extends('system._layouts.main')

@section('content')
<div class="content content-components section-wrapper mb-5">
      <div>
        <div class="d-flex justify-content-between mb-2">
          <h4 id="section1" class="mb-3 mb-lg-0 mb-md-0 mb-xl-0 oswold-400">List of Blogs</h4>
          <a href="{{ route('system.content_management.blogs.create')}}" class="btn btn-primary mg-b-10 font-small">
          	<i data-feather="check-square" class="mr-2"></i>Create Blogs
          </a>
        </div>
        <div class="row">
    
          <div class="col-lg-12 mt-3">
              <div  class="df-example demo-table">
                <div class="table-responsive">
                  <table class="table table-hover mg-b-0">

	                <thead>
	                  <tr class="oswold-400">
	                    <th scope="col">ID</th>
	                    <th scope="col">Title</th>
	                    <th scope="col">Image</th>	
{{-- 	                    <th scope="col">Author</th>		
 --}}	                    <th scope="col">Date Created</th>
	                    <th scope="col">Action</th>
	                  </tr>
	                </thead>
	                <tbody>
	                		 @forelse($blogs as  $blog)
	                  <tr class="sans-300">
	                    <th class="pt-3 pb-3">{{$blog->id}}</th>
	                    <td class="pt-3 pb-3">{{Str::title($blog->title)}}</td>
	                    <td class="pt-3 pb-3">
	                    	<img src="{{ "{$blog->directory}/resized/{$blog->filename}" }}" class="image-size-auto--90">
	                    </td>
{{-- 	                    <td class="pt-3 pb-3">{{Str::title($blog->author?$blog->author->name:NULL)}}</td>        
 --}}	                    <td class="pt-3 pb-3">{{ date('M d, Y',strtotime($blog->created_at)) }}</td>                
	                    <td>
	                    	<a href="{{route('system.content_management.blogs.edit',[$blog->id])}}" title="Update Blog Item" class="btn btn-sm btn-light text-dark bg-warning">
	                         <i data-feather="edit"></i>
	                     </a>
	                      <a data-toggle="modal" data-target="#confirm-delete" title="Remove Blog Item" class="btn btn-sm btn-light text-white bg-danger action-delete" data-url="{{route('system.content_management.blogs.destroy',[$blog->id])}}">
	                         <i data-feather="x"></i>
	                         
	                     </a>        
	                    </td>
	                  </tr>
	                  @empty
			              <td colspan="7" class="text-center"><i>No record found yet.</i> <a href="{{route('system.content_management.blogs.create')}}"><strong>Click here</strong></a> to create one.</td>
			              @endforelse
	                
	                </tbody>
	              </table>
	            </div><!-- table-responsive -->
          </div><!-- df-example -->
         </div>      
       </div>
      </div><!-- container -->
    </div><!-- content -->

{{-- Modals --}}
@include('system.content-management.modals.activate')
@include('system.content-management.modals.deactivate')
@stop

@section('page-scripts')

<script type="text/javascript">
	 
</script>
@stop