<link href="{{asset('assets/lib/fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/lib/typicons.font/typicons.css')}}" rel="stylesheet">
<link href="{{asset('assets/lib/prismjs/themes/prism-vs.css')}}" rel="stylesheet">
<link href="{{asset('assets/lib/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat|Quicksand:400,500,600&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<link href="{{asset('assets/lib/ion-rangeslider/css/ion.rangeSlider.min.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/css/dashforge.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/dashforge.demo.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css" rel="stylesheet" />
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;1,300&family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">


    
<style type="text/css">
    .navbar-menu .nav-item.active .nav-link {
        color: white !important;
    }
    .navbar-menu .nav-item.active .nav-link svg {
        color: white !important;
    }
	 .image-preview {
	 	height: 180px;
	 	width: auto; 
	 }
	 .bg-violet {
			background-color: #3C1F69 !important;
		}
		.text-violet {
			color: #3C1F69 !important;
		}
		.text-gray {
			color: #212121 !important;
		}

		.mb-07 {
			margin-bottom: 7px !important;
		}
		.text-white {
			color: white !important;
		}
	.text-white {
		color: white !important;
	}
	.btn--login {
		padding: 10px;
	}
	.btn--back {
		position: absolute;
		top: 3%;
		left: 3%;
	}
	.ui-datepicker-current {
    	display: none !important; 
    }
    .ui-datepicker-close {
    	text-align: center !important;
    }
    .ui-datepicker-buttonpane {
    	justify-content: center !important;
    }

    .padding-right--custom {
		padding-right: 5px !important;
	}
	.help-block {
		color: red;
		font-weight: bold;
	}
	.image-size-auto--90 {
		width: auto; 
		height: 90px;
	}
	input {
		text-transform: uppercase;
	}
	text-area{
		text-transform: uppercase;
	}
	  .oswold-300 {
        font-family: 'Oswald', sans-serif !important;
        font-weight: 300 !important;
    }
     .oswold-400 {
        font-family: 'Oswald', sans-serif !important;
        font-weight: 400 !important;
    }
     .oswold-500 {
        font-family: 'Oswald', sans-serif !important;
        font-weight: 500 !important;
    }
     .oswold-600 {
        font-family: 'Oswald', sans-serif !important;
        font-weight: 600 !important;
    }
     .oswold-700 {
        font-family: 'Oswald', sans-serif !important;
        font-weight: 700 !important;
    }
    .sans-300 {
        font-family: 'Open Sans', sans-serif !important;
        font-weight: 300 !important;
    }
    .sans-300-i {
        font-family: 'Open Sans', sans-serif !important;
        font-weight: 300 !important;
        font-style: italic;
    }
    .sans-400 {
        font-family: 'Open Sans', sans-serif !important;
        font-weight: 400 !important;
    }
    .sans-600 {
        font-family: 'Open Sans', sans-serif !important;
        font-weight: 600 !important;
    }
    .sans-700 {
        font-family: 'Open Sans', sans-serif !important;
        font-weight: 700 !important;
    }
    
    .bg-green {
        background-color:#16887C !important;
    }
    .text-green {
        color:#16887C !important;
    }
    .bg-red {
        background-color:#FD233B !important;
    }
    .text-red {
        color:#FD233B !important;
    }
    .select2-selection__rendered {
        display: none;
    }
    .no_data {
        display: none !important;
    }
    .active {
        background-color: #0168FA;
        border-radius: 5px;
        color: white !important;
    }
</style>


<script src="{{asset('assets/lib/jquery/jquery.min.js')}}"></script>
<script src="{{asset('assets/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
   