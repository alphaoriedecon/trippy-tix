<div id="sidebarMenu" class="sidebar sidebar-fixed sidebar-components">
      <div class="sidebar-header">
        <a href="#" id="mainMenuOpen"><i data-feather="menu"></i></a>
        <h5>Components</h5>
        <a href="#" id="sidebarMenuClose"><i data-feather="x"></i></a>
      </div><!-- sidebar-header -->
      <div class="sidebar-body">
        <ul class="sidebar-nav">
          <li class="nav-label mg-b-15 mt-3">Overview </li>
      <li class="nav-item mt-2 navbar-hover {{ (request()->is('admin')) ? 'active' : '' }}">
            <a href="{{ route('system.dashboard')}}" class="nav-link font-small  navbar-hover--text p-2 rounded ">
              <i class="icon-white--hover" data-feather="bar-chart-2"></i> Dashboard </a>
          </li>
          <li class="nav-item mt-2 navbar-hover {{ (request()->is('admin/inquiry/request-travel')) ? 'active' : '' }}">
            <a href="{{ route('system.inquiry.request_travel_index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="bar-chart-2"></i> Request All in Inquiry </a>
          </li>
          <li class="nav-item mt-2 navbar-hover {{ (request()->is('admin/inquiry/request-travel-land')) ? 'active' : '' }}">
            <a href="{{ route('system.inquiry.request_travel_land_index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="bar-chart-2"></i> Request Land Tour Itinerary </a>
          </li>
           <li class="nav-item mt-2 navbar-hover {{ (request()->is('admin/inquiry/tourist-visa')) ? 'active' : '' }}">
            <a href="{{ route('system.inquiry.tourist_visa_index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="bar-chart-2"></i> Tourist Visa Consultation </a>
          </li>
           <li class="nav-item mt-2 navbar-hover {{ (request()->is('admin/inquiry/insured')) ? 'active' : '' }}">
            <a href="{{ route('system.inquiry.insured_index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="bar-chart-2"></i> Be insured to be sure </a>
          </li>
           <li class="nav-item mt-2 navbar-hover {{ (request()->is('admin/inquiry/permanent-visa')) ? 'active' : '' }}">
            <a href="{{ route('system.inquiry.permanent_visa_index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="bar-chart-2"></i> Permanent Resident Visa</a>
          </li>
           <li class="nav-item mt-2 navbar-hover {{ (request()->is('admin/inquiry/appointment')) ? 'active' : '' }}">
            <a href="{{ route('system.inquiry.appointment_index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="bar-chart-2"></i> Set Appointment </a>
          </li>
           <li class="nav-item mt-2 navbar-hover {{ (request()->is('admin/inquiry/airline')) ? 'active' : '' }}">
            <a href="{{ route('system.inquiry.airline_index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="bar-chart-2"></i> Airline Ticket </a>
          </li>


          @if(\Auth::user()->type == 'super_user')
          <li class="nav-label mg-b-15 mt-3">Account Management</li>
          <li class="nav-item mt-2 navbar-hover {{ (request()->is('admin/account-management/user')) ? 'active' : '' }}">
            <a href="{{ route('system.account_management.user.index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="user"></i> User</a>
          </li>
          @endif

           <li class="nav-label mg-b-15 mt-3">Content Management</li>
          
          <li class="nav-item mt-2 navbar-hover {{ (request()->is('admin/content-management/blogs')) ? 'active' : '' }}">
            <a href="{{ route('system.content_management.blogs.index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="user"></i> Blogs</a>
          </li>
          <li class="nav-item mt-2 navbar-hover {{ (request()->is('admin/content-management/banners')) ? 'active' : '' }}">
            <a href="{{ route('system.content_management.banners.index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="user"></i> Banners</a>
          </li>
          <li class="nav-item mt-2 navbar-hover {{ (request()->is('admin/content-management/packages')) ? 'active' : '' }}">
            <a href="{{ route('system.content_management.packages.index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="user"></i> Packages</a>
          </li>

        </ul>
      </div><!-- sidebar-body -->
    </div><!-- sidebar -->