 <header class="navbar navbar-header navbar-header-fixed bg-gray">
      <a href="#" id="mainMenuOpen" class="burger-menu"><i data-feather="menu"></i></a>
      <div class="navbar-brand">
        <a href="{{ route('system.dashboard')}}" class="df-logo">
          <img src="{{asset('frontend/img/logo/logo.PNG')}}" class="logo-size" alt="logo">
        </a>
      </div><!-- navbar-brand -->
      <div id="navbarMenu" class="navbar-menu-wrapper">
        <div class="navbar-menu-header">
          <a href="{{ route('system.dashboard')}}" class="df-logo">
            <img src="{{asset('frontend/img/logo/logo.PNG')}}" class="logo-size mt-2 mb-2" alt="logo">
          </a>
          <a id="mainMenuClose" href="#"><i data-feather="x"></i></a>
        </div><!-- navbar-menu-header -->
        <ul class="nav navbar-menu d-block d-md-none ml-3" style="overflow-y: scroll;">
            <li class="nav-label mg-b-15 mt-3">Overview </li>
      <li class="nav-item mt-2 navbar-hover {{ (request()->is('admin')) ? 'active' : '' }}">
            <a href="{{ route('system.dashboard')}}" class="nav-link font-small  navbar-hover--text p-2 rounded ">
              <i class="icon-white--hover" data-feather="bar-chart-2"></i> Dashboard </a>
          </li>
          <li class="nav-item mt-2 navbar-hover {{ (request()->is('admin/inquiry/request-travel')) ? 'active' : '' }}">
            <a href="{{ route('system.inquiry.request_travel_index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="bar-chart-2"></i> Request All in Inquiry </a>
          </li>
           <li class="nav-item mt-2 navbar-hover {{ (request()->is('admin/inquiry/request-travel-land')) ? 'active' : '' }}">
            <a href="{{ route('system.inquiry.request_travel_land_index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="bar-chart-2"></i> Request Land Tour Itinerary </a>
          </li>
           <li class="nav-item mt-2 navbar-hover {{ (request()->is('admin/inquiry/tourist-visa')) ? 'active' : '' }}">
            <a href="{{ route('system.inquiry.tourist_visa_index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="bar-chart-2"></i> Tourist Visa Consultation </a>
          </li>
           <li class="nav-item mt-2 navbar-hover {{ (request()->is('admin/inquiry/insured')) ? 'active' : '' }}">
            <a href="{{ route('system.inquiry.insured_index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="bar-chart-2"></i> Be insured to be sure </a>
          </li>
           <li class="nav-item mt-2 navbar-hover {{ (request()->is('admin/inquiry/permanent-visa')) ? 'active' : '' }}">
            <a href="{{ route('system.inquiry.permanent_visa_index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="bar-chart-2"></i> Permanent Resident Visa</a>
          </li>
           <li class="nav-item mt-2 navbar-hover {{ (request()->is('admin/inquiry/appointment')) ? 'active' : '' }}">
            <a href="{{ route('system.inquiry.appointment_index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="bar-chart-2"></i> Set Appointment </a>
          </li>
           <li class="nav-item mt-2 navbar-hover {{ (request()->is('admin/inquiry/airline')) ? 'active' : '' }}">
            <a href="{{ route('system.inquiry.airline_index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="bar-chart-2"></i> Airline Ticket </a>
          </li>







          @if(\Auth::user()->type == 'super_user')
          <li class="nav-label mg-b-15 mt-3">Account Management</li>
          <li class="nav-item mt-2 navbar-hover {{ (request()->is('admin/account-management/user')) ? 'active' : '' }}">
            <a href="{{ route('system.account_management.user.index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="user"></i> User</a>
          </li>
          @endif

           <li class="nav-label mg-b-15 mt-3">Content Management</li>
          
          <li class="nav-item mt-2 navbar-hover {{ (request()->is('admin/content-management/blogs')) ? 'active' : '' }}">
            <a href="{{ route('system.content_management.blogs.index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="user"></i> Blogs</a>
          </li>
          <li class="nav-item mt-2 navbar-hover {{ (request()->is('admin/content-management/banners')) ? 'active' : '' }}">
            <a href="{{ route('system.content_management.banners.index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="user"></i> Banners</a>
          </li>
          <li class="nav-item mt-2 navbar-hover {{ (request()->is('admin/content-management/packages')) ? 'active' : '' }}">
            <a href="{{ route('system.content_management.packages.index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="user"></i> Packages</a>
          </li>
         {{--  <li class="nav-item mt-2 navbar-hover">
            <a href="{{ route('system.content_management.partners.index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="user"></i> Partners</a>
          </li> --}}
       {{--    <li class="nav-item mt-2 navbar-hover">
            <a href="{{ route('system.account_management.system_user.index')}}" class="nav-link font-small  navbar-hover--text p-2 rounded">
              <i class="icon-white--hover" data-feather="user"></i> Web Information</a>
          </li> --}}
        </ul>
      </div><!-- navbar-menu-wrapper -->
      <div class="navbar-right">
        <div class="dropdown dropdown-profile">

          <a href="#" class="dropdown-link" data-toggle="dropdown" data-display="static">
           <div class="avatar avatar-sm"><img src="{{\Auth::user()->avatar}}" class="rounded-circle" alt=""></div>
          </a><!-- dropdown-link -->
          <div class="dropdown-menu dropdown-menu-right tx-13">
            <div class="avatar avatar-lg mg-b-15"><img src="{{\Auth::user()->avatar}}" class="rounded-circle" alt=""></div>
            <h6 class="tx-semibold mg-b-5">{{\Auth::user()->first_name}} {{\Auth::user()->last_name}}</h6>
 
            <div class="dropdown-divider"></div>
{{--             <a href="{{ route('system.settings.index',[\Auth::user()->id])}}" class="dropdown-item"><i data-feather="settings"></i>Account Settings</a>
 --}}            <a href="{{ route('system.logout')}}" class="dropdown-item"><i data-feather="log-out"></i>Sign Out</a>
          </div><!-- dropdown-menu -->
        </div><!-- dropdown -->
      </div><!-- navbar-right -->
    </header><!-- navbar -->

