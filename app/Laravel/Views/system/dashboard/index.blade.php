@extends('system._layouts.main')

@section('content')
    <div class="content content-components mb-5">
      <div class="row">
        <div class="col-lg-12 col-xl-12 mg-t-10">
            <div class="mg-b-10">
              <div class="card-header pd-t-20 d-sm-flex align-items-start justify-content-between bd-b-0 pd-b-0">
                <div>
                  <h4 class="mg-b-5">Dashboard</h4>
                </div>
              </div><!-- card-header -->
              <div class="card-body pd-y-30">
                <div class="row">
                  <div class="media col-lg-4">
                    <div class="wd-40 wd-md-50 ht-40 ht-md-50 bg-primary tx-white mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded op-6">
                      <i data-feather="bar-chart-2"></i>
                    </div>
                    <div class="media-body">
                      <h6 class="tx-sans tx-uppercase tx-10 tx-spacing-1 tx-color-03 tx-semibold tx-nowrap mg-b-5 mg-md-b-8">All Inquiry</h6>
                      <h4 class="tx-20 tx-sm-18 tx-md-24 tx-normal tx-rubik mg-b-0">{{$count_all_status}}</h4>
                    </div>
                  </div>
                  <div class="media col-lg-4">
                    <div class="wd-40 wd-md-50 ht-40 ht-md-50 bg-warning tx-white mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded op-5">
                      <i data-feather="bar-chart-2"></i>
                    </div>
                    <div class="media-body">
                      <h6 class="tx-sans tx-uppercase tx-10 tx-spacing-1 tx-color-03 tx-semibold mg-b-5 mg-md-b-8">Pending Inquiry</h6>
                      <h4 class="tx-20 tx-sm-18 tx-md-24 tx-normal tx-rubik mg-b-0">{{$count_pending}}</h4>
                    </div>
                  </div>
                  <div class="media col-lg-4">
                    <div class="wd-40 wd-md-50 ht-40 ht-md-50 bg-success tx-white mg-r-10 mg-md-r-10 d-flex align-items-center justify-content-center rounded op-4">
                      <i data-feather="bar-chart-2"></i>
                    </div>
                    <div class="media-body">
                      <h6 class="tx-sans tx-uppercase tx-10 tx-spacing-1 tx-color-03 tx-semibold mg-b-5 mg-md-b-8">Completed Inquiry</h6>
                      <h4 class="tx-20 tx-sm-18 tx-md-24 tx-normal tx-rubik mg-b-0">{{$count_completed}}</small></h4>
                    </div>
                  </div>
                </div>
              </div><!-- card-body -->
              
            </div><!-- card -->            
          </div><!-- col -->       
            <div class="col-lg-12 mt-3">
               <div data-label="Recent Inquiry" class="df-example demo-table">
                <div class="table-responsive">
                  <table class="table table-hover mg-b-0">
                   <thead>
                   <tr>
                      <th class="text-uppercase" scope="col"><strong>ID</strong></th>
                      <th class="text-uppercase" scope="col"><strong>Fullname</strong></th>
                      <th class="text-uppercase" scope="col"><strong>Contact Number</strong></th>      
                      <th class="text-uppercase" scope="col"><strong>Email Address</strong></th>
                      @if(\Auth::user()->type == 'super_user')
                          <th class="text-uppercase" scope="col"><strong>Agent</strong></th>
                      @endif
                      <th class="text-uppercase" scope="col"><strong>Status</strong></th>
                      <th class="text-uppercase" scope="col"><strong>Action</strong></th>
                    </tr>
                  </thead>
                  <tbody>
                 @foreach($inquiry_list as  $inquiry)
                    <tr>
                      <th class="pt-3 pb-3">{{$inquiry->id}}</th>
                      <td class="pt-3 pb-3">{{$inquiry->name}}</td>
                      <td class="pt-3 pb-3">{{$inquiry->contact_number}}</td>
                      <td class="pt-3 pb-3">{{$inquiry->email}}</td>
                      @if(\Auth::user()->type == 'super_user')
                        <td class="pt-3 pb-3">{{$inquiry->author->last_name}}, {{$inquiry->author->first_name}}</td>
                      @endif
                      <td class="pt-3 pb-3">
                         {{Str::title($inquiry->status)}}
                      </td>            
                      <td>
                       <a href="{{route('system.inquiry.details',[$inquiry->id])}}" title="View Transaction" class="btn btn-sm btn-light text-white bg-success">
                           <i data-feather="eye"></i>
                       </a>
                            
                      </td>
                    </tr>
                     @endforeach
                  </tbody>
                  </table>
                </div><!-- table-responsive -->
              </div>
            </div>

      </div><!-- container -->
    </div><!-- content -->
@stop