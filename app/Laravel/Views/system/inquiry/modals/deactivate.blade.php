<div id="confirm-delete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">   
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div class="row mt-5">
          <div class="col-lg-12 text-center">
            <i class="far fa-check-circle fa-7x text-success mb-3"></i>
            <p>Are you sure you want to Complete this inquiry ?</p>
          </div>
          <div class="col-lg-12 text-center mb-3">
             <a id="btn-confirm-delete" class="btn btn-success text-white w-25">Yes</a>
            <button type="button" class="btn btn-secondary w-25" data-dismiss="modal">No</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

