@extends('system._layouts.main')

@section('content')
<div class="content content-components section-wrapper mb-5">
    <div>
        <div class="row">
        <div class="col-lg-12">
            <h4 id="section1" class="mg-b-10 oswold-400">Inquiry Information</h4>
        </div>
            <div class="col-lg-4 mt-2 {{$details->name == null ? 'no_data' : NULL}}">
                <span>Full Name</span>
                <h5>{{$details->name}}</h5>
            </div>
            <div class="col-lg-4 mt-2 {{$details->email == null ? 'no_data' : NULL}}">
                <span>Email Address</span>
                <h5>{{$details->email}}</h5>
            </div>
            <div class="col-lg-4 mt-2 {{$details->contact_number == null ? 'no_data' : NULL}}">
                <span>Contact Number</span>
                <h5>{{$details->contact_number}}</h5>
            </div>
            <div class="col-lg-4 mt-2 {{$details->nationality == null ? 'no_data' : NULL}}">
                <span>Nationality</span>
                <h5>{{$details->nationality}}</h5>
            </div>
             <div class="col-lg-4 mt-2 {{$details->marital_status == null ? 'no_data' : NULL}}">
                <span>Marital Status</span>
                <h5>{{$details->marital_status}}</h5>
            </div>
             <div class="col-lg-4 mt-2 {{$details->address == null ? 'no_data' : NULL}}">
                <span>Address</span>
                <h5>{{$details->address}}</h5>
            </div>
             <div class="col-lg-4 mt-2 {{$details->country == null ? 'no_data' : NULL}}">
                <span>Country</span>
                <h5>{{$details->country}}</h5>
            </div>
             <div class="col-lg-4 mt-2 {{$details->departure_date == null ? 'no_data' : NULL}}">
                <span>Departure Date</span>
                <h5>{{$details->departure_date}}</h5>
            </div>

             <div class="col-lg-4 mt-2 {{$details->arrival_date == null ? 'no_data' : NULL}}">
                <span>Arrival Date</span>
                <h5>{{$details->arrival_date}}</h5>
            </div>

             <div class="col-lg-4 mt-2 {{$details->passenger == null ? 'no_data' : NULL}}">
                <span>Total Number of Passenger</span>
                <h5>{{$details->passenger}}</h5>
            </div>
             <div class="col-lg-4 mt-2 {{$details->child == null ? 'no_data' : NULL}}">
                <span>Total Number of Child (2-6y/0)</span>
                <h5>{{$details->child}}</h5>
            </div>
            <div class="col-lg-4 mt-2 {{$details->infant == null ? 'no_data' : NULL}}">
                <span>Total Number of Infant (0-1 & 11mos.)</span>
                <h5>{{$details->infant}}</h5>
            </div>
           <div class="col-lg-4 mt-2 {{$details->departure_location == null ? 'no_data' : NULL}}">
                <span>Airport Origin</span>
                <h5>{{$details->departure_location}}</h5>
            </div>



             <div class="col-lg-4 mt-2 {{$details->arrival_location == null ? 'no_data' : NULL}}">
                <span>Destionation</span>
                <h5>{{$details->arrival_location}}</h5>
            </div>
            <div class="col-lg-4 mt-2 {{$details->days == null ? 'no_data' : NULL}}">
                <span>Number of Days</span>
                <h5>{{$details->days}}</h5>
            </div>

            <div class="col-lg-4 mt-2 {{$details->tourist_visa == null ? 'no_data' : NULL}}">
                <span>Tourist Visa</span>
                <h5>{{$details->tourist_visa}}</h5>
            </div>
            <div class="col-lg-4 mt-2 {{$details->appointment_date == null ? 'no_data' : NULL}}">
                <span>Appointment Date</span>
                <h5>{{$details->appointment_date }}</h5>
            </div>
          {{--   <div class="col-lg-4 mt-2">
                <span>Status</span>
                @if($details->status == 'active')
                <h5 class="mt-1"><span class="bg-success text-white p-1 rounded">Active</span></h5>
                @else
                <h5 class="mt-1"><span class="bg-warning p-1 rounded" style="color: #333">Pending</span></h5>
                @endif
            </div> --}}
             <div class="col-lg-12 mt-2">
                <span>Message</span>
                <h5>{{$details->message == null ? 'No messages' : $details->message}}</h5>
            </div>
            <div class="col-lg-12 text-right">
              <hr>
                <a data-toggle="modal" data-target="#confirm-delete" title="Complete Inquiry" class="btn btn-sm btn-light text-white bg-primary action-delete" data-url="{{route('system.inquiry.complete',[$details->id])}}">
                           Complete Inquiry    
                       </a>   
            </div>
        </div>
       
        

    </div>
    <!-- container -->
</div>
<!-- content -->
@include('system.inquiry.modals.activate')
@include('system.inquiry.modals.deactivate')
@stop