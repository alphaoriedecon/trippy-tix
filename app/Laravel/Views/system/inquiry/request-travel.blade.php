@extends('system._layouts.main')

@section('content')
<div class="content content-components section-wrapper mb-5">
      <div>
        <div class="d-flex justify-content-md-between mb-2">
          <h4 id="section1" class="mb-3 mb-lg-0 mb-md-0 mb-xl-0 oswold-400">Request All in Inquiry</h4>
         
        </div>
        <div class="row">
      
          <div class="col-lg-12 mt-3">
              <div class="df-example demo-table">
                <div class="table-responsive">
                  <table class="table table-hover mg-b-0">
                  <thead>
                    <tr>
                      <th class="text-uppercase oswold-400" scope="col">ID</th>
                      <th class="text-uppercase oswold-400" scope="col">Fullname</th>
                      <th class="text-uppercase oswold-400" scope="col">Contact Number</th>      
                      <th class="text-uppercase oswold-400" scope="col">Email Address</th>
                      @if(\Auth::user()->type == 'super_user')
                          <th class="text-uppercase oswold-400" scope="col">Agent</th>
                      @endif
                      <th class="text-uppercase oswold-400" scope="col">Status</th>
                      <th class="text-uppercase oswold-400" scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($request_travel as  $inquiry)
                    <tr>
                      <th class="pt-3 pb-3 sans-300">{{$inquiry->id}}</th>
                      <td class="pt-3 pb-3 sans-300">{{$inquiry->name}}</td>
                      <td class="pt-3 pb-3 sans-300">{{$inquiry->contact_number}}</td>
                      <td class="pt-3 pb-3 sans-300">{{$inquiry->email}}</td>
                      @if(\Auth::user()->type == 'super_user')
                        <td class="pt-3 pb-3 sans-300">{{$inquiry->author->last_name}}, {{$inquiry->author->first_name}}</td>
                      @endif
                      <td class="pt-3 pb-3 sans-300">
                         {{Str::title($inquiry->status)}}
                      </td>            
                      <td>
                       <a href="{{route('system.inquiry.details',[$inquiry->id])}}" title="View Transaction" class="btn btn-sm btn-light text-white bg-success">
                           <i data-feather="eye"></i>
                       </a>
                            
                      </td>
                    </tr>
                     @endforeach
                    

                  </tbody>
                </table>
              </div><!-- table-responsive -->
          </div><!-- df-example -->
         </div>      
       </div>
      </div><!-- container -->
    </div><!-- content -->

{{-- Modals --}}
@include('system.account-management.user.modals.activate')
@include('system.account-management.user.modals.deactivate')
@stop

@section('page-scripts')

<script type="text/javascript">
   
</script>
@stop
