@extends('system._layouts.main')

@section('content')
<div class="content content-components section-wrapper mb-5">
  <div class="pl-2 pr-2">
      <div class="tx-13 mg-b-25">
        <div class="row">
          <div class="col-lg-8">
            <div class="d-flex flex-row">
            <i data-feather="skip-back" class="mt-1 mr-2"></i>
            <h3>Create Product</h3>
          </div>
            <p class="tx-14 mg-b-30">Fill out the form below to create your product.</p>
            <form method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}
              <section class="mt-4">     
                  <div class="row row-sm">
                    <div class="form-group col-md-12">
                      <label class="text-uppercase font-weight-bold">Title</label>                  
                      <input type="text" class="form-control" name="title" >
                      @if ($errors->has("title"))
                        <span class="text text-danger">{{ $errors->first("title") }}</span>
                      @endif
                    </div>
                    <div class="form-group col-md-12">
                      <label class="text-uppercase font-weight-bold">Type</label>
                      <select class="custom-select" name="category">
                        <option value="">--Choose Categories--</option>
                        @foreach ($categories as $category)
                          <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                        @endforeach
                      </select>
                      @if ($errors->has("category"))
                        <span class="text text-danger">{{ $errors->first("category") }}</span>
                      @endif
                    </div>
                    <div class="form-group col-md-8">
                      <img id="blah" src="#" class="image-preview" alt="your image" />
                    </div>
                    <div class="form-group col-md-12">
                      <input type="file" name="file" class="form-control" id="imgInp" />
                    </div>
                   {{--  <div class="form-group col-md-12">
                      <label class="text-uppercase font-weight-bold">Quantity</label>                  
                      <input type="number"  min="1" value="1" class="form-control" name="qty">
                      @if ($errors->has("qty"))
                        <span class="text text-danger">{{ $errors->first("qty") }}</span>
                      @endif
                    </div> --}}
                    <div class="form-group col-md-12">
                      <label class="text-uppercase font-weight-bold">Description</label>
                      <textarea class="form-control" rows="7" name="description"></textarea>
                      @if ($errors->has("description"))
                        <span class="text text-danger">{{ $errors->first("description") }}</span>
                      @endif
                    </div>        
                    <div class="col-md-12 text-right">
                      <button class="btn btn-primary" type="submit"><i data-feather="check-square" class="mr-2"></i>Create Product</button>
                    </div>        
                  </div><!-- row -->
               </section>
             </form>
             </div>
           </div>
      </div>
  </div><!-- container -->
</div><!-- content -->
@stop