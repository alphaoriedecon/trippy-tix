@extends('system._layouts.main')

@section('content')
<div class="content content-components section-wrapper mb-5">
  	<div class="mb-2">
      <h4 id="section1" class="mg-b-10">Transaction Details</h4>
      <hr>
        <div class="row">
          <div class="col-md-4">
              <span class="font-semibold">7855878</span>
              <p>Transaction ID</p>
              <span class="font-semibold">5</span>
              <p>Number of Avail Product</p>
          </div>
          <div class="col-md-4">
              <span class="font-semibold">12-12-2019</span>
              <p>Transaction Date</p>
              <span class="font-semibold">12-12-2019</span>
              <p>Payment Date</p>
          </div>
          <div class="col-md-4">
              
              <span class="font-semibold font-size--amount">PHP 10,000.00</span>
              <p>Total Amount</p>
              <span class="font-semibold bg-success text-white p-1 rounded">Completed</span>
              <p>Status</p>
          </div>
        </div>
    </div>
    <div class="row">
      <div class="col-lg-12 mt-2">
        <div class="df-example demo-table">
            <div class="table-responsive">
              <table class="table table-hover mg-b-0">
                <thead class="bg-primary">
                  <tr>
                    <th scope="col" class="text-white">Product ID</th>
                    <th scope="col" class="text-white">Product Title</th>
                    <th scope="col" class="text-white">Product Details</th>
                    <th scope="col" class="text-white">Amount</th>
                    <th scope="col" class="text-white">Quantity</th>
                    <th scope="col" class="text-white">Subtotal</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                     <th class="pt-3 pb-3">5878955</th>
                     <th class="pt-3 pb-3">Lorem</th>
                     <th class="pt-3 pb-3">Ipsum dummy text</th>
                      <th class="pt-3 pb-3">PHP 100.00</th>
                     <th class="pt-3 pb-3">2</th>
                     <th class="pt-3 pb-3">PHP 200.00</th>
                  </tr>
                </tbody>
              </table>
            </div><!-- table-responsive -->
          </div><!-- df-example -->
       </div>        
  </div><!-- container -->
</div><!-- content -->
@stop