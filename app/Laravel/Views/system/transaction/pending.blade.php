@extends('system._layouts.main')

@section('content')
<div class="content content-components section-wrapper mb-5">
      <div>
        <div class="d-flex justify-content-md-between mb-2">
          <h4 id="section1" class="mg-b-10">Pending Transaction</h4>

        </div>
        <form>
        <div class="row">
          <div class="col-lg-4">
            <div data-label="Search Value" class=" demo-forms">
              <div class="row">   
                <div class="col-12 padding-right--custom">
                  <input type="text" class="form-control" placeholder="Search">
                </div><!-- col -->
              </div><!-- row -->
            </div><!-- df-example -->
          </div>
          <div class="col-lg-8 mt-3 mt-md-3 mt-lg-0 mt-xl-0">
            <div data-label="Filter Date" class="demo-forms">
              <div class="row">
                <div class="col-4 padding-right--custom">
                  {{ Form::selectMonth('search_month_from', null, array('class' => 'form-control')) }}
                </div><!-- col -->
                <div class="col-4 padding-right--custom">
                  {{ Form::selectMonth('search_month_to',null, array('class' => 'form-control')) }}
                </div><!-- col -->
                <div class="col-3 padding-right--custom">
                  {!!Form::selectYear('search_year',Carbon::now()->subYears(6)->format("Y"),Carbon::now()->format("Y"),old('search_year',2019),['class' =>"form-control"])!!}
                </div><!-- col -->
                <div class="col-1 padding-right--custom">
                  <button class="btn btn-primary"><i data-feather="search"></i></button>
                </div><!-- col -->
              </div><!-- row -->
            </div><!-- df-example -->
          </div>
        </div>
      </form>
      <div class="row">
          <div class="col-lg-12 mt-3">
             <div class="df-example demo-table">
            <div class="table-responsive">
              <table class="table table-hover mg-b-0">
                <thead>
                  <tr>
                    <th scope="col">Transaction ID</th>
                    <th scope="col">Reference No</th>
                    <th scope="col">Name of User</th>
                    <th scope="col">Transaction Date</th>
                    <th scope="col">Payment Date</th>
                    <th scope="col">Quantity</th>            
                    <th scope="col">Amount</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th class="pt-3 pb-3">5878955</th>
                    <td class="pt-3 pb-3">31232131</td>
                    <td class="pt-3 pb-3">Dhen Mark</td>
                    <td class="pt-3 pb-3">02-18-2020</td>
                    <td class="pt-3 pb-3">02-18-2020</td>
                    <td class="pt-3 pb-3">02</td>
                    <td class="pt-3 pb-3">PHP 10,000.00</td>                    
                    <td>
                     <a href="{{route('system.transaction.show')}}" class="btn btn-sm btn-light text-white bg-success">
                         <i data-feather="eye"></i>
                     </a>
                 
                    </td>
                  </tr>
                </tbody>
              </table>
            </div><!-- table-responsive -->
          </div><!-- df-example -->
          </div>
          
        
        </div>

      </div><!-- container -->
    </div><!-- content -->
@stop
@section('page-styles')
<style type="text/css">
	.ui-datepicker-calendar {
    display: none;
    }
    .ui-datepicker {
    	width: 200px !important;
    }
    .ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year {
    	font-size: 15px !important;
    }
    span.ui-datepicker-year {
    	display: none;
    }

</style>
@stop
@section('page-scripts')
<script type="text/javascript">
	$(function() {
     $('.date-picker').datepicker(
                    {
                        dateFormat: "MM",
                        changeMonth: true,
                        changeYear: true,
                        showButtonPanel: true,
                        onClose: function(dateText, inst) {
                            function isDonePressed(){
                                return ($('#ui-datepicker-div').html().indexOf('ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all ui-state-hover') > -1);
                            }

                            if (isDonePressed()){
                                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                                $(this).datepicker('setDate', new Date(year, month, 1)).trigger('change');
                                
                                 $('.date-picker').focusout()//Added to remove focus from datepicker input box on selecting date
                            }
                        },
                        beforeShow : function(input, inst) {

                            inst.dpDiv.addClass('month_year_datepicker')

                            if ((datestr = $(this).val()).length > 0) {
                                year = datestr.substring(datestr.length-4, datestr.length);
                                month = datestr.substring(0, 2);
                                $(this).datepicker('option', 'defaultDate', new Date(year, month-1, 1));
                                $(this).datepicker('setDate', new Date(year, month-1, 1));
                                $(".ui-datepicker-calendar").hide();
                            }
                        }
                    })
});
</script>
@stop