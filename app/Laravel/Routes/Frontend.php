<?php

/*,'domain' => env("FRONTEND_URL", "wineapp.localhost.com")*/

Route::group(['as' => "frontend.",
		 'namespace' => "Frontend",
		 'middleware' => ["web"]
		],function() {

	Route::group(['prefix' => "/",'as' => "auth."],function(){
		Route::get('login/{redirect_uri?}',['as' => "login",'uses' => "AuthController@login"]);
        Route::post('login/{redirect_uri?}',['uses' => "AuthController@authenticate"]);
		Route::get('logout',['as'=>"logout",'uses' => "AuthController@destroy"]);
		Route::get('register/{redirect_uri?}',['as' => "register",'uses' => "AuthController@register"]);
        Route::post('register/{redirect_uri?}',['uses' => "AuthController@store"]);
	});		
    	

	Route::group(['prefix' => "/",'as' => "home."],function(){
		Route::get('/',['as' => "index",'uses' => "HomeController@index"]);
	});
	Route::group(['prefix' => "/my-account",'as' => "my_account."],function(){
		Route::get('/settings',['as' => "settings",'uses' => "SettingsController@index"]);
		Route::post('/settings',['uses' => "SettingsController@update"]);

		Route::get('/my-orders',['as' => "my_orders",'uses' => "MyOrdersController@index"]);

        Route::group(['prefix' => "/my-products",'as' => "my_products."],function(){
            Route::get('/',['as' => "index",'uses' => "MyProductsController@index"]);
            Route::get('/create',['as' => "create",'uses' => "MyProductsController@create"]);
            Route::get('edit/{id?}',['as' => "edit",'uses' => "MyProductsController@edit"]);
            Route::post('edit/{id?}',['uses' => "MyProductsController@update"]);
            Route::any('delete/{id?}',['as' => "destroy", 'uses' => "MyProductsController@destroy"]);

            Route::any('delete-image/{id?}',['as' => "destroy_image", 'uses' => "MyProductsController@destroy_image"]);

            // Route::get('/get-sub-list',['as' => "get-sub-list",'uses' => "MyProductsController@getSubCategoryList"]);

            Route::get('get-sub-list','MyProductsController@getSubCategoryList');

            Route::post('/create',['uses' => "MyProductsController@store"]);
        });
         Route::group(['prefix' => "/chat",'as' => "chat."],function(){
            Route::get('/',['as' => "index",'uses' => "ChatController@index"]);
        });
		Route::get('/my-transaction',['as' => "my_transaction",'uses' => "MyTransactionController@index"]);
		Route::get('/address-book',['as' => "address_book",'uses' => "AddressBookController@index"]);
	});

	Route::group(['prefix' => "/about",'as' => "about."],function(){
		Route::get('/',['as' => "index",'uses' => "AboutController@index"]);
	});

	Route::group(['prefix' => "/services",'as' => "services."],function(){
		Route::get('/',['as' => "index",'uses' => "ServicesController@index"]);
	});

	Route::group(['prefix' => "/blogs",'as' => "blogs."],function(){
		Route::get('/',['as' => "index",'uses' => "BlogsController@index"]);
		Route::get('details/{id?}',['as' => "details",'uses' => "BlogsController@details"]);
	});

	Route::group(['prefix' => "/packages",'as' => "packages."],function(){
		Route::get('/',['as' => "index",'uses' => "PackagesController@index"]);
		Route::get('details/{id?}',['as' => "details",'uses' => "PackagesController@details"]);
	});

	Route::group(['prefix' => "/inquiry",'as' => "inquiry."],function(){
		Route::get('/airline',['as' => "airline",'uses' => "InquiryController@airline"]);
		Route::post('airline',['uses' => "InquiryController@storeAirline"]);
		Route::get('/appointment',['as' => "appointment",'uses' => "InquiryController@appointment"]);
		Route::post('appointment',['uses' => "InquiryController@storeAppointment"]);
		Route::get('/insured',['as' => "insured",'uses' => "InquiryController@insured"]);
		Route::post('insured',['uses' => "InquiryController@storeInsured"]);
		Route::get('/permanent-visa',['as' => "permanent_visa",'uses' => "InquiryController@permanent_visa"]);
		Route::post('permanent-visa',['uses' => "InquiryController@storePermanent_visa"]);
		Route::get('/request-travel',['as' => "request_travel",'uses' => "InquiryController@request_travel"]);
		Route::post('request-travel',['uses' => "InquiryController@storeRequest_travel"]);
		Route::post('request-travel-land-tour',['uses' => "InquiryController@storeRequest_travel_land"]);
		Route::get('/request-travel-land-tour',['as' => "request_travel_land",'uses' => "InquiryController@request_travel_land"]);
		
		Route::get('/tourist-visa',['as' => "tourist_visa",'uses' => "InquiryController@tourist_visa"]);
		Route::post('tourist-visa',['uses' => "InquiryController@storeTourist_visa"]);
	});



	Route::group(['prefix' => "contact",'as' => "contact."],function(){
		Route::get('/',['as' => "index",'uses' => "ContactController@index"]);
	});


	Route::group(['prefix' => "products",'as' => "products."],function(){

		Route::get('/',['as' => "index",'uses' => "ProductsController@index"]);
		Route::get('/details',['as' => "show",'uses' => "ProductsController@show"]);
		Route::get('/cart',['as' => "cart",'uses' => "ProductsController@cart"]);
		Route::post('add-to-cart',['as' => "add_to_cart",'uses' => "ProductsController@addToCart"]);
		Route::post('remove-cart',['as' => "remove_cart",'uses' => "ProductsController@destroy"]);
		Route::get('/checkout',['as' => "checkout",'uses' => "ProductsController@checkout"]);
	});


	

	

});