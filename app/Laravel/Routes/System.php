<?php

/*,'domain' => env("FRONTEND_URL", "wineapp.localhost.com")*/
Route::group([
		'as' => "system.",
		'namespace' => "System",
		'prefix' => "admin",
		],function() {

	Route::group(['middleware'=>['system.guest']],function(){
       Route::get('login/{redirect_uri?}',['as' => "login",'uses' => "AuthController@login"]);
		Route::post('login/{redirect_uri?}',['uses' => "AuthController@authenticate"]);
    });



	Route::group(['middleware' => ["system.auth"]], function(){
		
		Route::get('/',['as' => "dashboard",'uses' => "DashboardController@index"]);
		Route::get('lock',['as' => "lock", 'uses' => "AuthController@lock"]);
		Route::post('lock',['uses' => "AuthController@unlock"]);
		Route::get('logout',['as' => "logout",'uses' => "AuthController@destroy"]);

		/*
		* Transaction Route
		*/ 
		Route::group(['prefix' => "/transaction",'as' => "transaction."],function(){
			Route::get('/new',['as' => "new",'uses' => "TransactionController@new"]);
			Route::post('store',['as' => "store",'uses' => "TransactionController@store"]);
			Route::get('/pending',['as' => "pending",'uses' => "TransactionController@pending"]);
			Route::get('/completed',['as' => "completed",'uses' => "TransactionController@completed"]);
			Route::get('/details',['as' => "show",'uses' => "TransactionController@show"]);
		});

		Route::group(['prefix' => "/account-management",'as' => "account_management."],function(){
			Route::group(['prefix' => "/user",'as' => "user."],function(){
				Route::get('/',['as' => "index",'uses' => "AccountManagementController@user"]);
				Route::get('/create',['as' => "create",'uses' => "AccountManagementController@create_user"]);
				Route::post('create',['uses' => "AccountManagementController@store"]);
				Route::get('edit/{id?}',['as' => "edit",'uses' => "AccountManagementController@edit"]);
				Route::post('edit/{id?}',['uses' => "AccountManagementController@update"]);
				Route::any('inactive/{id?}',['as' => "inactive",'uses' => "AccountManagementController@inactive"]);
				Route::any('active/{id?}',['as' => "active",'uses' => "AccountManagementController@active"]);
				Route::get('/list-of-transaction',['as' => "list_of_transaction",'uses' => "AccountManagementController@list_of_transaction"]);
			});
			
			Route::group(['prefix' => "system-user",'as' => "system_user."],function(){
				Route::get('/',['as' => "index",'uses' => "AccountManagementController@system_user"]);
				Route::get('/create',['as' => "create",'uses' => "AccountManagementController@create_system_user"]);
			});
		});

		Route::group(['prefix' => "/",'as' => "settings."],function(){
			Route::get('settings/{id?}',['as' => "index",'uses' => "SettingsController@index"]);
           Route::post('settings/{id?}',['uses' => "SettingsController@update"]);
		});


		/*
			Products Route
		*/
		Route::group(['prefix' => "/products",'as' => "products."],function(){
			Route::get('/',['as' => "index",'uses' => "ProductsController@index"]);
			Route::get('create',['as' => "create",'uses' => "ProductsController@create"]);
			Route::post('create',['uses' => "ProductsController@store"]);
			Route::get('edit/{id?}',['as' => "edit",'uses' => "ProductsController@edit"]);
			Route::post('edit/{id?}',['uses' => "ProductsController@update"]);
			Route::get('delete/{id?}',['as' => "delete",'uses' => "ProductsController@destroy"]);
		});


		/*
		 		Product Key Route
		*/
		Route::group(['prefix' => "product-keys",'as' => "product_keys."],function(){
			Route::get('/',['as' => "index",'uses' => "ProductKeysController@index"]);
			Route::get('create',['as' => "create",'uses' => "ProductKeysController@create"]);
			Route::post('create',['uses' => "ProductKeysController@store"]);
			Route::get('edit/{id?}',['as' => "edit",'uses' => "ProductKeysController@edit"]);
			Route::post('edit/{id?}',['as' => "update",'uses' => "ProductKeysController@update"]);
			Route::get('delete/{id?}',['as'=>"delete",'uses' => "ProductKeysController@destroy"]);
		});

		/*
			Product Category Route
		*/
		Route::group(['prefix' => "product-category",'as' => "product_category."],function(){
			Route::get('/',['as' => "index",'uses' => "ProductCategoryController@index"]);
			Route::get('create',['as' => "create",'uses' => "ProductCategoryController@create"]);
			Route::post('create',['uses' => "ProductCategoryController@store"]);
			Route::get('edit/{id?}',['as'=>"edit",'uses' => "ProductCategoryController@edit"]);
			Route::post('edit/{id}',['as' => "update",'uses' => "ProductCategoryController@update"]);
			Route::get('delete/{id?}',['as'=>"delete",'uses' => "ProductCategoryController@destroy"]);
		});
        /*
            Main Category Route
        */
        Route::group(['prefix' => "main-category",'as' => "main_category."],function(){
            Route::get('/',['as' => "index",'uses' => "MainCategoryController@index"]);
            Route::get('create',['as' => "create",'uses' => "MainCategoryController@create"]);
            Route::post('create',['uses' => "MainCategoryController@store"]);
            Route::get('edit/{id?}',['as'=>"edit",'uses' => "MainCategoryController@edit"]);
            Route::post('edit/{id}',['as' => "update",'uses' => "MainCategoryController@update"]);
            Route::get('delete/{id?}',['as'=>"delete",'uses' => "MainCategoryController@destroy"]);
        });

        /*
            Sub Category Route
        */
        Route::group(['prefix' => "sub-category",'as' => "sub_category."],function(){
            Route::get('/',['as' => "index",'uses' => "SubCategoryController@index"]);
            Route::get('create',['as' => "create",'uses' => "SubCategoryController@create"]);
            Route::post('create',['uses' => "SubCategoryController@store"]);
            Route::get('edit/{id?}',['as'=>"edit",'uses' => "SubCategoryController@edit"]);
            Route::post('edit/{id}',['as' => "update",'uses' => "SubCategoryController@update"]);
            Route::get('delete/{id?}',['as'=>"delete",'uses' => "SubCategoryController@destroy"]);
        });

          /*
         Category Route
        */
        Route::group(['prefix' => "category",'as' => "category."],function(){
            Route::get('/',['as' => "index",'uses' => "CategoryController@index"]);
            Route::get('create',['as' => "create",'uses' => "CategoryController@create"]);
            Route::post('create',['uses' => "CategoryController@store"]);
            Route::get('edit/{id?}',['as'=>"edit",'uses' => "CategoryController@edit"]);
            Route::post('edit/{id}',['as' => "update",'uses' => "CategoryController@update"]);
            Route::get('delete/{id?}',['as'=>"delete",'uses' => "CategoryController@destroy"]);
        });

           /*
         Inquiry Route
        */
        Route::group(['prefix' => "inquiry",'as' => "inquiry."],function(){
            Route::get('/request-travel',['as' => "request_travel_index",'uses' => "InquiryController@request_travel_index"]);
             Route::get('/request-travel-land',['as' => "request_travel_land_index",'uses' => "InquiryController@request_travel_land_index"]);
      			Route::get('/appointment',['as' => "appointment_index",'uses' => "InquiryController@appointment_index"]);
      			Route::get('/insured',['as' => "insured_index",'uses' => "InquiryController@insured_index"]);
      			Route::get('/permanent-visa',['as' => "permanent_visa_index",'uses' => "InquiryController@permanent_visa_index"]);
      			Route::get('/tourist-visa',['as' => "tourist_visa_index",'uses' => "InquiryController@tourist_visa_index"]);
      			Route::get('/airline',['as' => "airline_index",'uses' => "InquiryController@airline_index"]);
      			Route::get('details/{id?}',['as' => "details",'uses' => "InquiryController@details"]);
      			Route::any('complete/{id?}',['as' => "complete",'uses' => "InquiryController@complete"]);
        });



		Route::group(['prefix' => "content-management",'as' => "content_management."],function(){
				Route::group(['prefix' => "/blogs",'as' => "blogs."],function(){
					Route::get('/',['as' => "index",'uses' => "BlogsController@index"]);
					Route::get('/create',['as' => "create",'uses' => "BlogsController@create"]);
					Route::post('create',['uses' => "BlogsController@store"]);
					Route::get('edit/{id?}',['as' => "edit",'uses' => "BlogsController@edit"]);
					Route::post('edit/{id?}',['uses' => "BlogsController@update"]);
					Route::any('delete/{id?}',['as' => "destroy",'uses' => "BlogsController@destroy"]);
				});
				Route::group(['prefix' => "/banners",'as' => "banners."],function(){
					Route::get('/',['as' => "index",'uses' => "BannersController@index"]);
					Route::get('/create',['as' => "create",'uses' => "BannersController@create"]);
					Route::post('create',['uses' => "BannersController@store"]);
					Route::get('edit/{id?}',['as' => "edit",'uses' => "BannersController@edit"]);
					Route::post('edit/{id?}',['uses' => "BannersController@update"]);
					Route::any('delete/{id?}',['as' => "destroy",'uses' => "BannersController@destroy"]);
				});
				Route::group(['prefix' => "blog-categories",'as' => "blog_categories."],function(){
					Route::get('/',['as' => "index",'uses' => "BlogCategoriesController@index"]);
					Route::get('create',['as' => "create",'uses' => "BlogCategoriesController@create"]);
					Route::post('create',['uses' => "BlogCategoriesController@store"]);
					Route::get('edit/{id?}',['as' => "edit",'uses' => "BlogCategoriesController@edit"]);
					Route::post('edit/{id?}',['uses' => "BlogCategoriesController@update"]);
					Route::any('delete/{id?}',['as' => "destroy",'uses' => "BlogCategoriesController@destroy"]);
				});

				Route::group(['prefix' => "/packages",'as' => "packages."],function(){
					Route::get('/',['as' => "index",'uses' => "PackagesController@index"]);
					Route::get('/create',['as' => "create",'uses' => "PackagesController@create"]);
					Route::post('create',['uses' => "PackagesController@store"]);
					Route::get('edit/{id?}',['as' => "edit",'uses' => "PackagesController@edit"]);
					Route::post('edit/{id?}',['uses' => "PackagesController@update"]);
					Route::any('delete/{id?}',['as' => "destroy",'uses' => "PackagesController@destroy"]);
				});

			
				Route::group(['prefix' => "/partners",'as' => "partners."],function(){
					Route::get('/',['as' => "index",'uses' => "PartnersController@index"]);
					Route::get('create',['as' => "create",'uses' => "PartnersController@create"]);
					Route::post('create',['uses' => "PartnersController@store"]);
					Route::get('/update',['as' => "update",'uses' => "PartnersController@update"]);
				});

			});
		});
	
	});

	
	

