<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInquiryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inquiry', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('agent')->unsigned()->default("0");
            $table->string('name')->nullable();
            $table->string('email')->nullable(); 
            $table->string('contact_number')->nullable(); 
            $table->string('nationality')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('address')->nullable();
            $table->string('country')->nullable();
            $table->string('departure_date')->nullable();
            $table->string('arrival_date')->nullable();
            $table->string('departure_location')->nullable();
            $table->string('arrival_location')->nullable();
            $table->string('tourist_visa')->nullable();
            $table->string('appointment_date')->nullable();
            $table->string('passenger')->nullable();
            $table->string('infant')->nullable();
            $table->string('child')->nullable();
            $table->string('days')->nullable();
            $table->string('type')->nullable();
            $table->text('message')->nullable();
            $table->string('status')->nullable();
            $table->rememberToken(); 
            $table->timestamps();
            $table->softDeletes();

            
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inquiry');
    }
}
