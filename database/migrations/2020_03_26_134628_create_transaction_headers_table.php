<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *"user_id","transaction_id","qty","total_price","status"
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_header', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('trans_id')->nullable();
            $table->integer("user_id")->nullable();
            $table->integer("qty")->nullable();
            $table->decimal("total_price",25,2)->nullable();
            $table->string("status")->default("pending");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_header');
    }
}
