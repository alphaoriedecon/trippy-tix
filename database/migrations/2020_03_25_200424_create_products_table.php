<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     "title","category","description","path","directory","filename","status","qty"]
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('main_category')->nullable();
            $table->string('sub_category')->nullable();
            $table->string('category')->nullable();
            $table->string('type')->nullable();
            $table->integer('price')->nullable();
            $table->string('description')->nullable();
            $table->text('path')->nullable();
            $table->text('directory')->nullable();
            $table->text('filename')->nullable();
            $table->string('status')->default("active");
            $table->integer("qty")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
