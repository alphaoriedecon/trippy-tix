<?php

use Illuminate\Database\Seeder;
use App\Laravel\Models\ProductCategory;

class CatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cat = new ProductCategory;
        $cat->category_name = "Opertating System";
        $cat->save();

        $cat = new ProductCategory;
        $cat->category_name = "Office Application";
        $cat->save();

        $cat = new ProductCategory;
        $cat->category_name = "Anti Virus";
        $cat->save();
    }
}
