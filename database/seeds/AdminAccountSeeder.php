<?php

use App\Laravel\Models\User;
use Illuminate\Database\Seeder;

class AdminAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = new User;
        // Area I
        $users->first_name = "Super";
        $users->last_name = "User";
        $users->status = "active";
        $users->email = "admin@gmail.com";
        $users->username = "master_admin";
        $users->type = "super_user";
        $users->password = bcrypt("admin");
        $users->save();

        // $users = new User;
        // $users->first_name = "TrippyTix";
        // $users->last_name = "SuperAdmin";
        // $users->status = "active";
        // $users->email = "trippytixsuperadmin@gmail.com";
        // $users->username = "master_admin_trippy_tix";
        // $users->type = "super_user";
        // $users->password = bcrypt("trippytixsuperadmin");
        // $users->save();
    }
}
